#!/usr/bin/env bash

#Stop on failure
set -e

#Give the necessary permissions and clean up
chmod +x scripts/*
chmod +x run.sh
chmod +x stop.sh

echo "Stopping existing docker containers."
./stop.sh

echo "Building the Optimizer image."
docker-compose build optimizer optimizer_tests

echo "Running the elasticsearch, kibana and optimizer services."
docker-compose up -d elasticsearch kibana logstash optimizer

echo "Running tests."
docker-compose up optimizer_tests

echo "Tests completed. Tearing down cluster."
./stop.sh