# INFORE Optimizer

Service and resources for the optimizer component of the INFORE project.

Online [README and resources](https://drive.google.com/drive/folders/1KfGr7kNI82Ws7cEKHVqloHjY1kOjiD2Y?usp=sharing).

Offline [Readme as PDF](manual.pdf)

Offline [Resources](input)


# Modules

## [Optimizer](optimizer)
Statistics collection, parsing utilities and optimization rules of the project.
Contains methods for parsing input resource files regarding workflows, sites, dictionaryOperators and more.
Also, contains the web service responsible for submitting workflows and transmitting back optimized workflows. 

## [Uploader](uploader)
Module used for uploading and running workflows to different BigData platforms.


# Visualizing 

Visit [this](http://45.10.26.123:5601/app/kibana#/discover) link to view the Kibana dashboards.


# Persistent storage and content visualization

The ELK stack (Elasticsearch Logstash Kibana) is used for storing user files, aggregating platforms statistics
and visualizing results. A dockerized version of ELK stack is included in the docker-compose along with the optimizer
service. To view submitted files visit the following line (replace localhost with the deployed IP).

    http://45.10.26.123:5601/app/kibana#/discover


## Configure / Build / Deploy

Edit the application.yml if you would like to change networking options:

    nano optimizer/src/main/resources/application.yml


To simply run everything as a docker compose list of services (no more steps required):
    
    chmod +x run.sh
    ./run.sh

# License

The project's license can be found [locally](LICENSE.md) and [online](https://www.gnu.org/licenses/agpl-3.0.md).


### Credentials 

    Optimizer VM URL: 45.10.26.123:8080
    
    Optimizer Service (REST API):
    Username: infore_user
    Password: infore_pass
    
    ELK stack (Web/REST Interface)
    Username: elastic
    Password: elastic123