#!/bin/bash

. "/docker_home/sbin/spark-config.sh"
. "/docker_home/bin/load-spark-env.sh"

mkdir -p $SPARK_MASTER_LOG
ln -sf /dev/stdout $SPARK_MASTER_LOG/spark-master.out

cd /spark/bin && /spark/sbin/../bin/spark-class org.apache.spark.deploy.master.Master --properties-file /docker_home/conf/master/spark-defaults.conf --ip $SPARK_MASTER_HOST --port $SPARK_MASTER_PORT --webui-port $SPARK_MASTER_UI_PORT >>$SPARK_MASTER_LOG/spark-master.out
