#!/usr/bin/env bash

# Setup env
. /.env
timeout_msg='Timeout. Could be a Firewall, unreachable host or even a DNS resolution failure. Check your environment.'

run_tests() {
  echo "Launching tests in JAR [$1] on URL [$2] with content root [$3]."
  mvn -f /usr/local/infore -DtargetHost="$2" -DcontentRoot="$3" clean test --no-transfer-progress -P tests
}

# Run the JUnit tests
for elapsed in $(seq 10 10 180); do
  echo "Checking status of the Optimizer services."
  touch /logs/public.log
  echo "START" >> /logs/public.log
  echo "" >> /logs/public.log
  echo Running curl cmd: curl -s -w "%{http_code}" -u "$OPTIMIZER_USER:$OPTIMIZER_PASS" -o /logs/public.log "$OPTIMIZER_TEST_URL"
  services_ready=$(curl -s -w "%{http_code}" -u "$OPTIMIZER_USER:$OPTIMIZER_PASS" -o /logs/public.log "$OPTIMIZER_TEST_URL")
  if [ "$services_ready" -eq 200 ]; then
    echo "Optimizer service is online. Running all tests."
    run_tests "/opt/app/optimizer.jar" "$OPTIMIZER_TEST_URL" "/input"
    exit 0
    break
  elif [ "$services_ready" -eq 404 ]; then
    echo "Service unavailable. Retrying in 10s. Time elapsed: $elapsed."
  else
    echo "Optimizer tests:  $timeout_msg Code: [$services_ready]"
    echo "Environment"
    echo "OPTIMIZER_TEST_URL: $OPTIMIZER_TEST_URL"
    echo "Logs"
    cat /logs/public.log
  fi
  sleep 10
done

# If we have reached this block, then there was some sort of error.
exit 1
