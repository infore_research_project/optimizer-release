#!/usr/bin/env bash

# Setup env
. /.env
timeout_msg='Timeout. Could be a Firewall, unreachable host or even a DNS resolution failure. Check your environment.'
user="$ES_USER:$ES_PASS"

# The following message can be seen in the docker-compose boot sequence
echo "Configuring ELK indexes and index patterns."

# Adds an Elasticsearch index and Kibana index pattern if it doesn't exist
add_ES_index_pattern() {
  index_name=$1

  #Check if index exists and if it doesn't just create the index
  echo "Attempting to create ES index '$index_name' at host: $ES_URL."
  index_exists_response=$(curl -I -s -w "%{http_code}" -o /dev/null --user "$user" "$ES_URL/$index_name?pretty")
  if [ "$index_exists_response" -eq 200 ]; then
    echo "Index $index_name already exists."
  elif [ "$index_exists_response" -eq 404 ]; then
    index_response=$(curl --user "$user" -s -w "%{http_code}" -o /dev/null -X PUT "$ES_URL/$index_name?pretty")
    if [ "$index_response" -eq 000 ]; then
      echo "$timeout_msg"
      return 1
    elif [ "$index_response" -eq 200 ]; then
      echo "Successfully created ES index [$index_name]."
    else
      echo "Failed to create ES index [$index_name] (code $index_response)."
      return 2
    fi
  else
    echo
    echo "$timeout_msg [CODE $index_exists_response]"
    return 1
  fi

  echo "Attempting to create Kibana index pattern for [$index_name]."
  index_pattern='
  {
    "attributes": {
      "title": "'$index_name'"
    }
  }'
  index_pattern_response=$(curl \
    --user "$user" \
    -s \
    -w "%{http_code}" \
    -o /dev/null \
    -H "kbn-xsrf: true" \
    -H "Content-Type: application/json" \
    -d "$index_pattern" \
    -X POST "$KIBANA_URL/api/saved_objects/index-pattern/$index_name")
  if [ "$index_pattern_response" -eq 000 ]; then
    echo "$timeout_msg"
    return 1
  elif [ "$index_pattern_response" -eq 409 ] || [ "$index_pattern_response" -eq 404 ]; then
    echo "Index pattern [$index_name] already exists."
  elif [ "$index_pattern_response" -eq 200 ]; then
    echo "Successfully created index pattern [$index_name]."
  else
    echo "Failed to create ES index pattern [$index_name] (code $index_pattern_response)."
    return 3
  fi
}

add_kibana_dashboards() {
  for entry in "$1"/*.json; do
    echo "Adding dashboard [$entry]."
    response=$(
      curl \
        -X POST "$KIBANA_URL/api/kibana/dashboards/import?exclude=index-pattern" \
        --user "$user" \
        -s \
        -o /dev/null \
        -H "kbn-xsrf: true" \
        -H "Content-Type: application/json" \
        -d @"$entry"
    )
    echo "$response"
  done
}

entrypoint() {
  echo "Launching JAR: $1"
  java -jar "$1"
}

# Create Elasticsearch indexes and Kibana index patterns
# Repeat everything a max number of times, then give up
indexes=(infore_network infore_dictionary infore_workflow infore_request infore_result raw_stats)
indexPos=0
for elapsed in $(seq 10 10 120); do
  for idx_pos in $(seq $indexPos ${#indexes[@]}); do
    cur_index=${indexes[idx_pos]}
    add_ES_index_pattern "$cur_index"
    if [ $? -eq 0 ]; then
      echo "Index and index pattern for [$cur_index] created successfully!"
      ((indexPos += 1))
      if [ $indexPos -eq ${#indexes[@]} ]; then
        echo
        echo "All indexes are setup."
        echo
        echo "Setting up Kibana dashboards."
        echo
        add_kibana_dashboards "/dashboards"
        echo
        echo "Optimizer setup completed, boot sequence will start immediately."
        entrypoint "/opt/app/optimizer.jar"
        exit 0
      fi
    else
      echo "Sleeping for 10s. Waited $elapsed seconds so far."
      sleep 10
      break
    fi
  done
done

echo
echo "Failed to start the optimizer service. Try again after elasticsearch is setup."
echo
exit 1
