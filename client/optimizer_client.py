import requests

# Setup URL and credentials
OPTIMIZER_URL = "http://45.10.26.123:8080"
# OPTIMIZER_URL = "http://localhost:8080"
CREDENTIALS = ('infore_user', 'infore_pass')
HEADERS = {
    "Content-Type": "application/json",
    "Accept": "*/*"
}

# Resource paths
dictionary_path = '../input/own/dictionary.json'
network_path = '../input/own/network.json'
workflow_path = '../input/own/workflow.json'
request_path = '../input/own/request.json'


def get_file_contents(file):
    with open(file) as f:
        contents = f.read().replace('\n', '')
    return contents


if __name__ == '__main__':
    # Session open
    s = requests.Session()
    s.auth = CREDENTIALS
    s.headers.update(HEADERS)

    # Load JSON contents
    dictionary_data = get_file_contents(dictionary_path)
    network_data = get_file_contents(network_path)
    workflow_data = get_file_contents(workflow_path)
    request_data = get_file_contents(request_path)

    # PUT the resources in the optimizer
    dictionary_resp = s.put(OPTIMIZER_URL + '/resource/dictionary', dictionary_data)
    if dictionary_resp.status_code != 201:
        print('Dictionary error [{0}]'.format(dictionary_resp.status_code))
        print(dictionary_resp.content)
        exit(1)

    network_resp = s.put(OPTIMIZER_URL + '/resource/network', network_data)
    if network_resp.status_code != 201:
        print('Network error [{0}]'.format(network_resp.status_code))
        print(network_resp.content)
        exit(1)

    workflow_resp = s.put(OPTIMIZER_URL + '/resource/workflow', workflow_data)
    if workflow_resp.status_code != 201:
        print('Workflow error [{0}]'.format(workflow_resp.status_code))
        print(workflow_resp.content)
        exit(1)

    # Perform the request
    opt_response = s.post(OPTIMIZER_URL + '/request/optimize', request_data)
    if opt_response.status_code != 200:
        print('Response error [{0}]'.format(opt_response.status_code))
        print(opt_response.content)
        exit(1)

    print(opt_response.text)

    # Clean up
    s.close()
