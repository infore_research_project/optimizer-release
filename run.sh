#!/usr/bin/env bash

#Give the necessary permissions and clean up
chmod +x scripts/*
chmod +x stop.sh
./stop.sh

echo "Building the Optimizer image."
docker-compose build optimizer

echo "Running the elasticsearch, kibana and optimizer services."
docker-compose up elasticsearch kibana logstash optimizer
