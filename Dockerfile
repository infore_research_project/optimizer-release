FROM maven:3.6.3-jdk-8

COPY pom.xml /usr/local/infore/
COPY uploader /usr/local/infore/uploader
COPY optimizer /usr/local/infore/optimizer
RUN mkdir -p /opt/app && \
    mvn -f /usr/local/infore clean package --no-transfer-progress && \
    mv /usr/local/infore/optimizer/target/optimizer.jar /opt/app/optimizer.jar