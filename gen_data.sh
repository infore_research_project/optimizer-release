#!/usr/bin/env bash

echo "Data generation started."
#python3 client/logstash_client.py 45.10.26.123:9200 45.10.26.123:60001
python3 client/logstash_client.py localhost:9200 localhost:60001
echo "Data generation stopped."
