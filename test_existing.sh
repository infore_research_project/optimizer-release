#!/usr/bin/env bash

echo "Running tests. You should also run ./run.sh."
docker-compose build optimizer_tests
docker-compose up optimizer_tests
echo "Tests completed"