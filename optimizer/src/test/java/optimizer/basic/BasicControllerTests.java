package optimizer.basic;

import optimizer.OptimizerApplication;
import org.junit.jupiter.api.*;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

import static optimizer.OptimizerTestConstants.*;

public class BasicControllerTests {
    @BeforeAll
    static void setUpAll() {
        //Start the Spring app
        if (useMockApp) {
            new OptimizerApplication();
            OptimizerApplication.main(new String[]{

            });
        }
    }

    @Test
    @DisplayName("Echo test")
    void InfoControllerEchoTest() {
        String payload = String.valueOf(random.nextInt());
        String body = restTemplate.exchange(host + "/info/echo/" + payload, HttpMethod.GET, new HttpEntity<>(AuthHeaders), String.class).getBody();
        Assertions.assertEquals(payload, body);
    }

    @Test
    @Disabled //Log files can get quite large and for some reason the bandwidth is limited to ~10KBps
    @DisplayName("Retrieve logs test")
    void InfoControllerLogsTest() {
        String body = restTemplate.exchange(host + "/info/logs", HttpMethod.GET, new HttpEntity<>(AuthHeaders), String.class).getBody();
        Assertions.assertNotNull(body);
    }

    @Test
    @DisplayName("Time test")
    void LocalDateTimeTest() throws InterruptedException {
        for (int i = 0; i < 3; i++) {
            try {
                ResponseEntity<String> response = restTemplate.exchange(host + "/info/time", HttpMethod.GET, new HttpEntity<>(AuthHeaders), String.class);
                Assertions.assertEquals(response.getStatusCode(), HttpStatus.OK);
                Assertions.assertNotNull(response.getBody());
                ZonedDateTime serverZDT = ZonedDateTime.parse(response.getBody());
                ZonedDateTime curZDT = ZonedDateTime.ofInstant(Instant.now(), zoneID);
                Assertions.assertTrue(serverZDT.isBefore(curZDT));
                long diff = ChronoUnit.MINUTES.between(serverZDT, curZDT);
                Assertions.assertTrue(diff < 2);
                return;
            } catch (Exception e) {
                Thread.sleep(5000); //Retry after 1 sec
            }
        }
        Assertions.fail();
    }
}
