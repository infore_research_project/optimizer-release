package optimizer.process;

import optimizer.OptimizerApplication;
import optimizer.parser.dictionary.DictionaryOperator;
import optimizer.parser.dictionary.INFOREDictionary;
import optimizer.parser.network.INFORENetwork;
import optimizer.parser.network.Site;
import optimizer.parser.workflow.INFOREWorkflow;
import optimizer.utils.JSONSingleton;
import org.junit.jupiter.api.*;
import org.springframework.context.annotation.Description;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import static optimizer.OptimizerTestConstants.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class OptimizerResourcesControllerTests {

    @BeforeAll
    static void setUpAll() {
        //Start the Spring app
        if (useMockApp) {
            new OptimizerApplication();
            OptimizerApplication.main(new String[]{

            });
        }
    }

    @Test
    @Order(1)
    @DisplayName("Network CRUD operations test")
    void NetworkCRUDTest() throws IOException {
        //Resources
        String networkContentsLS = new String(Files.readAllBytes(Paths.get(lifeSciencePath + "/network.json")), StandardCharsets.UTF_8);
        INFORENetwork inforeNetworkLS = JSONSingleton.fromJson(networkContentsLS, INFORENetwork.class);
        Assertions.assertNotNull(inforeNetworkLS);

        String networkContentsMT = new String(Files.readAllBytes(Paths.get(maritimePath + "/network.json")), StandardCharsets.UTF_8);
        INFORENetwork inforeNetworkMT = JSONSingleton.fromJson(networkContentsMT, INFORENetwork.class);
        Assertions.assertNotNull(inforeNetworkMT);

        //Add networkLS then retrieve then check for equality
        ResponseEntity<String> res = restTemplate.exchange(host + "/resource/network", HttpMethod.PUT, new HttpEntity<>(networkContentsLS, AuthHeaders), String.class);
        Assertions.assertEquals(res.getStatusCode(), HttpStatus.CREATED);
        String LS_body = res.getBody();
        Assertions.assertNotNull(LS_body);
        boolean LS_created = LS_body.equals("Created");
        Assertions.assertTrue(LS_body.matches("^(Created|Updated).*"));

        String networkResLS = restTemplate.exchange(host + "/info/network/" + inforeNetworkLS.getNetwork(), HttpMethod.GET, new HttpEntity<>(AuthHeaders), String.class).getBody();
        INFORENetwork inforeNetworkResLS = JSONSingleton.fromJson(networkResLS, INFORENetwork.class);
        Assertions.assertEquals(inforeNetworkResLS, inforeNetworkLS);

        //Put/Replace networkLS with networkMT then retrieve then check for equality
        res = restTemplate.exchange(host + "/resource/network", HttpMethod.PUT, new HttpEntity<>(networkContentsMT, AuthHeaders), String.class);
        Assertions.assertEquals(res.getStatusCode(), HttpStatus.CREATED);
        String MT_body = res.getBody();
        Assertions.assertNotNull(MT_body);
        boolean MT_created = MT_body.equals("Created");
        Assertions.assertTrue(MT_body.matches("^(Created|Updated).*"));

        String networkResMT = restTemplate.exchange(host + "/info/network/" + inforeNetworkMT.getNetwork(), HttpMethod.GET, new HttpEntity<>(AuthHeaders), String.class).getBody();
        INFORENetwork inforeNetworkResMT = JSONSingleton.fromJson(networkResMT, INFORENetwork.class);
        Assertions.assertEquals(inforeNetworkResMT, inforeNetworkMT);

        //Delete networks
        if (LS_created) {
            ResponseEntity<String> delres3 = restTemplate.exchange(host + "/resource/network/" + inforeNetworkLS.getNetwork(), HttpMethod.DELETE,
                    new HttpEntity<>(AuthHeaders), String.class);
            Assertions.assertEquals(delres3.getStatusCode(), HttpStatus.NO_CONTENT);
        }

        if (MT_created) {
            ResponseEntity<String> delres4 = restTemplate.exchange(host + "/resource/network/" + inforeNetworkMT.getNetwork(), HttpMethod.DELETE,
                    new HttpEntity<>(AuthHeaders), String.class);
            Assertions.assertEquals(delres4.getStatusCode(), HttpStatus.NO_CONTENT);
        }
    }

    @Test
    @Order(2)
    @DisplayName("Dictionary CRUD operations test")
    void DictionaryCRUDTest() throws IOException {
        //Resources
        String dictionaryContentsLS = new String(Files.readAllBytes(Paths.get(lifeSciencePath + "/dictionary.json")), StandardCharsets.UTF_8);
        INFOREDictionary inforeDictionaryLS = JSONSingleton.fromJson(dictionaryContentsLS, INFOREDictionary.class);
        Assertions.assertNotNull(inforeDictionaryLS);

        String dictionaryContentsMT = new String(Files.readAllBytes(Paths.get(maritimePath + "/dictionary.json")), StandardCharsets.UTF_8);
        INFOREDictionary inforeDictionaryMT = JSONSingleton.fromJson(dictionaryContentsMT, INFOREDictionary.class);
        Assertions.assertNotNull(inforeDictionaryMT);

        //Add dictionaryLS then retrieve then check for equality
        ResponseEntity<String> res = restTemplate.exchange(host + "/resource/dictionary", HttpMethod.PUT, new HttpEntity<>(dictionaryContentsLS, AuthHeaders), String.class);
        Assertions.assertEquals(res.getStatusCode(), HttpStatus.CREATED);
        String LS_body = res.getBody();
        Assertions.assertNotNull(LS_body);
        boolean LS_created = LS_body.equals("Created");
        Assertions.assertTrue(LS_body.matches("^(Created|Updated).*"));

        String dictionaryResLS = restTemplate.exchange(host + "/info/dictionary/" + inforeDictionaryLS.getDictionaryName(), HttpMethod.GET, new HttpEntity<>(AuthHeaders), String.class).getBody();
        INFOREDictionary inforeDictionaryResLS = JSONSingleton.fromJson(dictionaryResLS, INFOREDictionary.class);
        Assertions.assertEquals(inforeDictionaryResLS, inforeDictionaryLS);

        //Put/Replace dictionaryLS with dictionaryMT then retrieve then check for equality
        res = restTemplate.exchange(host + "/resource/dictionary", HttpMethod.PUT, new HttpEntity<>(dictionaryContentsMT, AuthHeaders), String.class);
        Assertions.assertEquals(res.getStatusCode(), HttpStatus.CREATED);
        String MT_body = res.getBody();
        Assertions.assertNotNull(MT_body);
        boolean MT_created = MT_body.equals("Created");
        Assertions.assertTrue(MT_body.matches("^(Created|Updated).*"));

        String DictionaryResMT = restTemplate.exchange(host + "/info/dictionary/" + inforeDictionaryMT.getDictionaryName(), HttpMethod.GET, new HttpEntity<>(AuthHeaders), String.class).getBody();
        INFOREDictionary inforeDictionaryResMT = JSONSingleton.fromJson(DictionaryResMT, INFOREDictionary.class);
        Assertions.assertEquals(inforeDictionaryResMT, inforeDictionaryMT);

        //Delete networks
        if (LS_created) {
            ResponseEntity<String> delres3 = restTemplate.exchange(host + "/resource/dictionary/" + inforeDictionaryLS.getDictionaryName(), HttpMethod.DELETE,
                    new HttpEntity<>(AuthHeaders), String.class);
            Assertions.assertEquals(delres3.getStatusCode(), HttpStatus.NO_CONTENT);
        }

        if (MT_created) {
            ResponseEntity<String> delres4 = restTemplate.exchange(host + "/resource/dictionary/" + inforeDictionaryMT.getDictionaryName(), HttpMethod.DELETE,
                    new HttpEntity<>(AuthHeaders), String.class);
            Assertions.assertEquals(delres4.getStatusCode(), HttpStatus.NO_CONTENT);
        }
    }

    @Test
    @Order(3)
    @DisplayName("Workflow CRUD operations test")
    void WorkflowCRUDTest() throws IOException {
        //Resources
        String workflowContentsLS = new String(Files.readAllBytes(Paths.get(lifeSciencePath + "/workflow.json")), StandardCharsets.UTF_8);
        INFOREWorkflow inforeWorkflowLS = JSONSingleton.fromJson(workflowContentsLS, INFOREWorkflow.class);
        Assertions.assertNotNull(inforeWorkflowLS);

        String workflowContentsMT = new String(Files.readAllBytes(Paths.get(maritimePath + "/workflow.json")), StandardCharsets.UTF_8);
        INFOREWorkflow inforeWorkflowMT = JSONSingleton.fromJson(workflowContentsMT, INFOREWorkflow.class);
        Assertions.assertNotNull(inforeWorkflowMT);

        //Add workflowLS then retrieve then check for equality
        ResponseEntity<String> res = restTemplate.exchange(host + "/resource/workflow", HttpMethod.PUT, new HttpEntity<>(workflowContentsLS, AuthHeaders), String.class);
        Assertions.assertEquals(res.getStatusCode(), HttpStatus.CREATED);
        String LS_body = res.getBody();
        Assertions.assertNotNull(LS_body);
        boolean LS_created = LS_body.equals("Created");
        Assertions.assertTrue(LS_body.matches("^(Created|Updated).*"));

        String workflowResLS = restTemplate.exchange(host + "/info/workflow/" + inforeWorkflowLS.getWorkflowName(), HttpMethod.GET, new HttpEntity<>(AuthHeaders), String.class).getBody();
        INFOREWorkflow inforeWorkflowResLS = JSONSingleton.fromJson(workflowResLS, INFOREWorkflow.class);
        Assertions.assertEquals(inforeWorkflowResLS, inforeWorkflowLS);

        //Put/Replace workflowLS with workflowMT then retrieve then check for equality
        res = restTemplate.exchange(host + "/resource/workflow", HttpMethod.PUT, new HttpEntity<>(workflowContentsMT, AuthHeaders), String.class);
        Assertions.assertEquals(res.getStatusCode(), HttpStatus.CREATED);
        String MT_body = res.getBody();
        Assertions.assertNotNull(MT_body);
        boolean MT_created = MT_body.equals("Created");
        Assertions.assertTrue(MT_body.matches("^(Created|Updated).*"));

        String workflowResMT = restTemplate.exchange(host + "/info/workflow/" + inforeWorkflowMT.getWorkflowName(), HttpMethod.GET, new HttpEntity<>(AuthHeaders), String.class).getBody();
        INFOREWorkflow inforeWorkflowResMT = JSONSingleton.fromJson(workflowResMT, INFOREWorkflow.class);
        Assertions.assertEquals(inforeWorkflowResMT, inforeWorkflowMT);

        //TODO Add delete requests and checks

        //Delete networks
        if (LS_created) {
            ResponseEntity<String> delres3 = restTemplate.exchange(host + "/resource/workflow/" + inforeWorkflowLS.getWorkflowName(), HttpMethod.DELETE,
                    new HttpEntity<>(AuthHeaders), String.class);
            Assertions.assertEquals(delres3.getStatusCode(), HttpStatus.NO_CONTENT);
        }

        if (MT_created) {
            ResponseEntity<String> delres4 = restTemplate.exchange(host + "/resource/workflow/" + inforeWorkflowMT.getWorkflowName(), HttpMethod.DELETE,
                    new HttpEntity<>(AuthHeaders), String.class);
            Assertions.assertEquals(delres4.getStatusCode(), HttpStatus.NO_CONTENT);
        }
    }

    @Test
    @Order(4)
    @Description("Update network entries")
    void NetworkAppendSiteTest() throws IOException {
        //Remove a site from each network and append it later to check if the final result matches the original (whole) network.
        String networkContentsLS = new String(Files.readAllBytes(Paths.get(lifeSciencePath + "/network.json")), StandardCharsets.UTF_8);
        INFORENetwork inforeNetworkLSOriginal = JSONSingleton.fromJson(networkContentsLS, INFORENetwork.class);
        INFORENetwork inforeNetworkLS = JSONSingleton.fromJson(networkContentsLS, INFORENetwork.class);
        Assertions.assertNotNull(inforeNetworkLS);
        Site selSiteLS = inforeNetworkLS.getSites().iterator().next();
        String selSiteLSContents = JSONSingleton.toJson(selSiteLS);
        inforeNetworkLS.getSites().remove(selSiteLS);

        //Add networkLS then retrieve then check for equality
        ResponseEntity<String> res = restTemplate.exchange(host + "/resource/network", HttpMethod.PUT, new HttpEntity<>(networkContentsLS, AuthHeaders), String.class);
        Assertions.assertEquals(res.getStatusCode(), HttpStatus.CREATED);
        String LS_body = res.getBody();
        Assertions.assertNotNull(LS_body);
        boolean LS_created = LS_body.equals("Created");
        Assertions.assertTrue(LS_body.matches("^(Created|Updated).*"));

        //Append the new site, retrieve and compare with the original network
        ResponseEntity<String> networkUpdateLS = restTemplate.exchange(host + "/resource/network/" + inforeNetworkLS.getNetwork(), HttpMethod.PUT,
                new HttpEntity<>(selSiteLSContents, AuthHeaders), String.class);
        Assertions.assertEquals(networkUpdateLS.getStatusCode(), HttpStatus.OK);
        Assertions.assertNotNull(networkUpdateLS.getBody());
        Assertions.assertTrue(networkUpdateLS.getBody().matches("Added|Updated"));

        String workflowResLS = restTemplate.exchange(host + "/info/network/" + inforeNetworkLS.getNetwork(),
                HttpMethod.GET, new HttpEntity<>(AuthHeaders), String.class).getBody();
        INFORENetwork inforeWorkflowResLS = JSONSingleton.fromJson(workflowResLS, INFORENetwork.class);
        Assertions.assertEquals(inforeWorkflowResLS, inforeNetworkLSOriginal);
        inforeNetworkLS.addSite(selSiteLS);
        Assertions.assertEquals(inforeWorkflowResLS, inforeNetworkLS);

        //Repeat for the other use-case
        String networkContentsMT = new String(Files.readAllBytes(Paths.get(maritimePath + "/network.json")), StandardCharsets.UTF_8);
        INFORENetwork inforeNetworkMTOriginal = JSONSingleton.fromJson(networkContentsMT, INFORENetwork.class);
        INFORENetwork inforeNetworkMT = JSONSingleton.fromJson(networkContentsMT, INFORENetwork.class);
        Assertions.assertNotNull(inforeNetworkMT);
        Site selSiteMT = inforeNetworkMT.getSites().iterator().next();
        String selSiteMTContents = JSONSingleton.toJson(selSiteMT);
        inforeNetworkMT.getSites().remove(selSiteMT);

        res = restTemplate.exchange(host + "/resource/network", HttpMethod.PUT, new HttpEntity<>(JSONSingleton.toJson(inforeNetworkMT), AuthHeaders), String.class);
        Assertions.assertEquals(res.getStatusCode(), HttpStatus.CREATED);
        String MT_body = res.getBody();
        Assertions.assertNotNull(MT_body);
        boolean MT_created = MT_body.equals("Created");
        Assertions.assertTrue(MT_body.matches("^(Created|Updated).*"));

        ResponseEntity<String> networkUpdateMT = restTemplate.exchange(host + "/resource/network/" + inforeNetworkMT.getNetwork(), HttpMethod.PUT,
                new HttpEntity<>(selSiteMTContents, AuthHeaders), String.class);
        Assertions.assertEquals(networkUpdateMT.getStatusCode(), HttpStatus.OK);
        Assertions.assertNotNull(networkUpdateMT.getBody());
        Assertions.assertTrue(networkUpdateMT.getBody().matches("Added|Updated"));

        String workflowResMT = restTemplate.exchange(host + "/info/network/" + inforeNetworkMT.getNetwork(),
                HttpMethod.GET, new HttpEntity<>(AuthHeaders), String.class).getBody();
        INFORENetwork inforeWorkflowResMT = JSONSingleton.fromJson(workflowResMT, INFORENetwork.class);
        Assertions.assertEquals(inforeWorkflowResMT, inforeNetworkMTOriginal);
        inforeNetworkMT.addSite(selSiteMT);
        Assertions.assertEquals(inforeWorkflowResMT, inforeNetworkMT);

        //Delete networks
        if (LS_created) {
            ResponseEntity<String> delres3 = restTemplate.exchange(host + "/resource/network/" + inforeNetworkLS.getNetwork(), HttpMethod.DELETE,
                    new HttpEntity<>(AuthHeaders), String.class);
            Assertions.assertEquals(delres3.getStatusCode(), HttpStatus.NO_CONTENT);
        }

        if (MT_created) {
            ResponseEntity<String> delres4 = restTemplate.exchange(host + "/resource/network/" + inforeNetworkMT.getNetwork(), HttpMethod.DELETE,
                    new HttpEntity<>(AuthHeaders), String.class);
            Assertions.assertEquals(delres4.getStatusCode(), HttpStatus.NO_CONTENT);
        }
    }

    @Test
    @Order(5)
    @Description("Update dictionary entries")
    void DictionaryAppendSiteTest() throws IOException {
        //Resources
        //Remove a site from each network and append it later to check if the final result matches the original (whole) network.
        String dictionaryContentsLS = new String(Files.readAllBytes(Paths.get(lifeSciencePath + "/dictionary.json")), StandardCharsets.UTF_8);
        INFOREDictionary inforeDictionaryLSOriginal = JSONSingleton.fromJson(dictionaryContentsLS, INFOREDictionary.class);
        INFOREDictionary inforeDictionaryLS = JSONSingleton.fromJson(dictionaryContentsLS, INFOREDictionary.class);
        Assertions.assertNotNull(inforeDictionaryLS);
        DictionaryOperator selOperatorLS = inforeDictionaryLS.getOperators().iterator().next();
        String selOpLSContents = JSONSingleton.toJson(selOperatorLS);
        inforeDictionaryLS.getOperators().remove(selOperatorLS);

        //Add networkLS then retrieve then check for equality
        ResponseEntity<String> res = restTemplate.exchange(host + "/resource/dictionary", HttpMethod.PUT, new HttpEntity<>(JSONSingleton.toJson(inforeDictionaryLS), AuthHeaders), String.class);
        Assertions.assertEquals(res.getStatusCode(), HttpStatus.CREATED);
        String LS_body = res.getBody();
        Assertions.assertNotNull(LS_body);
        boolean LS_created = LS_body.equals("Created");
        Assertions.assertTrue(LS_body.matches("^(Created|Updated).*"));

        //Append the new site, retrieve and compare with the original network
        ResponseEntity<String> dictionaryUpdateLS = restTemplate.exchange(host + "/resource/dictionary/" + inforeDictionaryLS.getDictionaryName(), HttpMethod.PUT,
                new HttpEntity<>(selOpLSContents, AuthHeaders), String.class);
        Assertions.assertEquals(dictionaryUpdateLS.getStatusCode(), HttpStatus.OK);
        Assertions.assertNotNull(dictionaryUpdateLS.getBody());
        Assertions.assertTrue(dictionaryUpdateLS.getBody().matches("Added|Updated"));

        String workflowResLS = restTemplate.exchange(host + "/info/dictionary/" + inforeDictionaryLS.getDictionaryName(),
                HttpMethod.GET, new HttpEntity<>(AuthHeaders), String.class).getBody();
        INFOREDictionary inforeWorkflowResLS = JSONSingleton.fromJson(workflowResLS, INFOREDictionary.class);
        Assertions.assertEquals(inforeWorkflowResLS, inforeDictionaryLSOriginal);
        inforeDictionaryLS.addOperator(selOperatorLS);
        Assertions.assertEquals(inforeWorkflowResLS, inforeDictionaryLS);

        //Repeat for the other use-case
        String dictionaryContentsMT = new String(Files.readAllBytes(Paths.get(maritimePath + "/dictionary.json")), StandardCharsets.UTF_8);
        INFOREDictionary inforeDictionaryMTOriginal = JSONSingleton.fromJson(dictionaryContentsMT, INFOREDictionary.class);
        INFOREDictionary inforeDictionaryMT = JSONSingleton.fromJson(dictionaryContentsMT, INFOREDictionary.class);
        Assertions.assertNotNull(inforeDictionaryMT);
        DictionaryOperator selOperatorMT = inforeDictionaryMT.getOperators().iterator().next();
        String selSiteMTContents = JSONSingleton.toJson(selOperatorMT);
        inforeDictionaryMT.getOperators().remove(selOperatorMT);

        res = restTemplate.exchange(host + "/resource/dictionary", HttpMethod.PUT, new HttpEntity<>(JSONSingleton.toJson(inforeDictionaryMT), AuthHeaders), String.class);
        Assertions.assertEquals(res.getStatusCode(), HttpStatus.CREATED);
        String MT_body = res.getBody();
        Assertions.assertNotNull(MT_body);
        boolean MT_created = MT_body.equals("Created");
        Assertions.assertTrue(MT_body.matches("^(Created|Updated).*"));

        ResponseEntity<String> dictionaryUpdateMT = restTemplate.exchange(host + "/resource/dictionary/" + inforeDictionaryMT.getDictionaryName(), HttpMethod.PUT,
                new HttpEntity<>(selSiteMTContents, AuthHeaders), String.class);
        Assertions.assertEquals(dictionaryUpdateMT.getStatusCode(), HttpStatus.OK);
        Assertions.assertNotNull(dictionaryUpdateMT.getBody());
        Assertions.assertTrue(dictionaryUpdateMT.getBody().matches("Added|Updated"));

        String workflowResMT = restTemplate.exchange(host + "/info/dictionary/" + inforeDictionaryMT.getDictionaryName(),
                HttpMethod.GET, new HttpEntity<>(AuthHeaders), String.class).getBody();
        INFOREDictionary inforeWorkflowResMT = JSONSingleton.fromJson(workflowResMT, INFOREDictionary.class);
        Assertions.assertEquals(inforeWorkflowResMT, inforeDictionaryMTOriginal);
        inforeDictionaryMT.addOperator(selOperatorMT);
        Assertions.assertEquals(inforeWorkflowResMT, inforeDictionaryMT);

        //Delete networks
        if (LS_created) {
            ResponseEntity<String> delres3 = restTemplate.exchange(host + "/resource/dictionary/" + inforeDictionaryLS.getDictionaryName(), HttpMethod.DELETE,
                    new HttpEntity<>(AuthHeaders), String.class);
            Assertions.assertEquals(delres3.getStatusCode(), HttpStatus.NO_CONTENT);
        }

        if (MT_created) {
            ResponseEntity<String> delres4 = restTemplate.exchange(host + "/resource/dictionary/" + inforeDictionaryMT.getDictionaryName(), HttpMethod.DELETE,
                    new HttpEntity<>(AuthHeaders), String.class);
            Assertions.assertEquals(delres4.getStatusCode(), HttpStatus.NO_CONTENT);
        }
    }

}
