package optimizer.process;

import optimizer.OptimizerApplication;
import optimizer.parser.dictionary.INFOREDictionary;
import optimizer.parser.network.INFORENetwork;
import optimizer.parser.optimization.OptimizationRequest;
import optimizer.parser.workflow.INFOREWorkflow;
import optimizer.utils.JSONSingleton;
import org.junit.jupiter.api.*;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import static optimizer.OptimizerTestConstants.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class OptimizerRequestControllerTests {

    @BeforeAll
    static void setUpAll() {
        //Start the Spring app
        if (useMockApp) {
            new OptimizerApplication();
            OptimizerApplication.main(new String[]{

            });
        }
    }

    @Test
    @Order(1)
    @DisplayName("Life Science use-case optimization")
    void LifeScienceOptimizationTest() throws IOException {
        //Resources
        String networkContents = new String(Files.readAllBytes(Paths.get(lifeSciencePath + "/network.json")), StandardCharsets.UTF_8);
        INFORENetwork inforeNetwork = JSONSingleton.fromJson(networkContents, INFORENetwork.class);
        Assertions.assertNotNull(inforeNetwork);

        String dictionaryContents = new String(Files.readAllBytes(Paths.get(lifeSciencePath + "/dictionary.json")), StandardCharsets.UTF_8);
        INFOREDictionary inforeDictionary = JSONSingleton.fromJson(dictionaryContents, INFOREDictionary.class);
        Assertions.assertNotNull(inforeDictionary);

        String workflowContents = new String(Files.readAllBytes(Paths.get(lifeSciencePath + "/workflow.json")), StandardCharsets.UTF_8);
        INFOREWorkflow inforeWorkflow = JSONSingleton.fromJson(workflowContents, INFOREWorkflow.class);
        Assertions.assertNotNull(inforeWorkflow);

        String optreqContents = new String(Files.readAllBytes(Paths.get(lifeSciencePath + "/optreq.json")), StandardCharsets.UTF_8);
        OptimizationRequest optimizationRequest = JSONSingleton.fromJson(optreqContents, OptimizationRequest.class);
        Assertions.assertNotNull(optimizationRequest);

        //Submit the necessary resources to the optimizer
        ResponseEntity<String> res_network = restTemplate.exchange(host + "/resource/network", HttpMethod.PUT, new HttpEntity<>(networkContents, AuthHeaders), String.class);
        Assertions.assertEquals(res_network.getStatusCode(), HttpStatus.CREATED);
        String net_body = res_network.getBody();
        Assertions.assertNotNull(net_body);
        boolean network_created = net_body.equals("Created");
        Assertions.assertTrue(net_body.matches("^(Created|Updated).*"));

        ResponseEntity<String> res_dict = restTemplate.exchange(host + "/resource/dictionary", HttpMethod.PUT, new HttpEntity<>(dictionaryContents, AuthHeaders), String.class);
        Assertions.assertEquals(res_dict.getStatusCode(), HttpStatus.CREATED);
        String dict_body = res_dict.getBody();
        Assertions.assertNotNull(dict_body);
        boolean dictionary_created = dict_body.equals("Created");
        Assertions.assertTrue(dict_body.matches("^(Created|Updated).*"));

        ResponseEntity<String> res_wf = restTemplate.exchange(host + "/resource/workflow", HttpMethod.PUT, new HttpEntity<>(workflowContents, AuthHeaders), String.class);
        Assertions.assertEquals(res_wf.getStatusCode(), HttpStatus.CREATED);
        String wf_body = res_wf.getBody();
        Assertions.assertNotNull(wf_body);
        boolean workflow_created = wf_body.equals("Created");
        Assertions.assertTrue(wf_body.matches("^(Created|Updated).*"));

        //Submit the optimization request
        ResponseEntity<String> optimized_workflow = restTemplate.postForEntity(host + "/request/optimize", new HttpEntity<>(optreqContents, AuthHeaders), String.class);
        Assertions.assertEquals(optimized_workflow.getStatusCode(), HttpStatus.OK, optimized_workflow.getBody());
        String optimized_workflow_body = optimized_workflow.getBody();
        Assertions.assertNotNull(optimized_workflow_body);
        Assertions.assertFalse(optimized_workflow_body.isEmpty());

        System.out.println(optimized_workflow_body);

        //Cleanup
        if (network_created) {
            ResponseEntity<String> delres1 = restTemplate.exchange(host + "/resource/network/" + inforeNetwork.getNetwork(), HttpMethod.DELETE,
                    new HttpEntity<>(AuthHeaders), String.class);
            Assertions.assertEquals(delres1.getStatusCode(), HttpStatus.NO_CONTENT);
        }

        if (dictionary_created) {
            ResponseEntity<String> delres2 = restTemplate.exchange(host + "/resource/dictionary/" + inforeDictionary.getDictionaryName(), HttpMethod.DELETE,
                    new HttpEntity<>(AuthHeaders), String.class);
            Assertions.assertEquals(delres2.getStatusCode(), HttpStatus.NO_CONTENT);
        }

        if (workflow_created) {
            ResponseEntity<String> delres3 = restTemplate.exchange(host + "/resource/workflow/" + inforeWorkflow.getWorkflowName(), HttpMethod.DELETE,
                    new HttpEntity<>(AuthHeaders), String.class);
            Assertions.assertEquals(delres3.getStatusCode(), HttpStatus.NO_CONTENT);
        }
    }

    @Test
    @Order(2)
    @DisplayName("Maritime use-case optimization")
    void MaritimeOptimizationTest() throws IOException {
        //Resources
        String networkContents = new String(Files.readAllBytes(Paths.get(maritimePath + "/network.json")), StandardCharsets.UTF_8);
        INFORENetwork inforeNetwork = JSONSingleton.fromJson(networkContents, INFORENetwork.class);
        Assertions.assertNotNull(inforeNetwork);

        String dictionaryContents = new String(Files.readAllBytes(Paths.get(maritimePath + "/dictionary.json")), StandardCharsets.UTF_8);
        INFOREDictionary inforeDictionary = JSONSingleton.fromJson(dictionaryContents, INFOREDictionary.class);
        Assertions.assertNotNull(inforeDictionary);

        String workflowContents = new String(Files.readAllBytes(Paths.get(maritimePath + "/workflow.json")), StandardCharsets.UTF_8);
        INFOREWorkflow inforeWorkflow = JSONSingleton.fromJson(workflowContents, INFOREWorkflow.class);
        Assertions.assertNotNull(inforeWorkflow);

        String optreqContents = new String(Files.readAllBytes(Paths.get(maritimePath + "/optreq.json")), StandardCharsets.UTF_8);
        OptimizationRequest optimizationRequest = JSONSingleton.fromJson(optreqContents, OptimizationRequest.class);
        Assertions.assertNotNull(optimizationRequest);

        //Submit the necessary resources to the optimizer
        ResponseEntity<String> res_network = restTemplate.exchange(host + "/resource/network", HttpMethod.PUT, new HttpEntity<>(networkContents, AuthHeaders), String.class);
        Assertions.assertEquals(res_network.getStatusCode(), HttpStatus.CREATED);
        String net_body = res_network.getBody();
        Assertions.assertNotNull(net_body);
        boolean network_created = net_body.equals("Created");
        Assertions.assertTrue(net_body.matches("^(Created|Updated).*"));

        ResponseEntity<String> res_dict = restTemplate.exchange(host + "/resource/dictionary", HttpMethod.PUT, new HttpEntity<>(dictionaryContents, AuthHeaders), String.class);
        Assertions.assertEquals(res_dict.getStatusCode(), HttpStatus.CREATED);
        String dict_body = res_dict.getBody();
        Assertions.assertNotNull(dict_body);
        boolean dict_created = net_body.equals("Created");
        Assertions.assertTrue(dict_body.matches("^(Created|Updated).*"));

        ResponseEntity<String> res_wf = restTemplate.exchange(host + "/resource/workflow", HttpMethod.PUT, new HttpEntity<>(workflowContents, AuthHeaders), String.class);
        Assertions.assertEquals(res_wf.getStatusCode(), HttpStatus.CREATED);
        String wf_body = res_wf.getBody();
        Assertions.assertNotNull(wf_body);
        boolean workflow_created = net_body.equals("Created");
        Assertions.assertTrue(wf_body.matches("^(Created|Updated).*"));

        //Submit the optimization request
        ResponseEntity<String> optimized_workflow = restTemplate.postForEntity(host + "/request/optimize", new HttpEntity<>(optreqContents, AuthHeaders), String.class);
        String ow_body = optimized_workflow.getBody();
        Assertions.assertNotNull(ow_body);
        Assertions.assertEquals(optimized_workflow.getStatusCode(), HttpStatus.OK, ow_body);
        Assertions.assertFalse(ow_body.isEmpty());

        System.out.println(ow_body);

        //Cleanup
        if (network_created) {
            ResponseEntity<String> delres1 = restTemplate.exchange(host + "/resource/network/" + inforeNetwork.getNetwork(),
                    HttpMethod.DELETE, new HttpEntity<>(AuthHeaders), String.class);
            Assertions.assertEquals(delres1.getStatusCode(), HttpStatus.NO_CONTENT);
        }

        if (dict_created) {
            ResponseEntity<String> delres2 = restTemplate.exchange(host + "/resource/dictionary/" + inforeDictionary.getDictionaryName(),
                    HttpMethod.DELETE, new HttpEntity<>(AuthHeaders), String.class);
            Assertions.assertEquals(delres2.getStatusCode(), HttpStatus.NO_CONTENT);
        }

        if (workflow_created) {
            ResponseEntity<String> delres3 = restTemplate.exchange(host + "/resource/workflow/" + inforeWorkflow.getWorkflowName(),
                    HttpMethod.DELETE, new HttpEntity<>(AuthHeaders), String.class);
            Assertions.assertEquals(delres3.getStatusCode(), HttpStatus.NO_CONTENT);
        }
    }

    @Test
    @Disabled
    void OwnResourcesTest() throws IOException {
        //Resources
        String networkContents = new String(Files.readAllBytes(Paths.get(ownPath + "/network.json")), StandardCharsets.UTF_8);
        INFORENetwork inforeNetwork = JSONSingleton.fromJson(networkContents, INFORENetwork.class);
        Assertions.assertNotNull(inforeNetwork);

        String dictionaryContents = new String(Files.readAllBytes(Paths.get(ownPath + "/dictionary.json")), StandardCharsets.UTF_8);
        INFOREDictionary inforeDictionary = JSONSingleton.fromJson(dictionaryContents, INFOREDictionary.class);
        Assertions.assertNotNull(inforeDictionary);

        String workflowContents = new String(Files.readAllBytes(Paths.get(ownPath + "/workflow.json")), StandardCharsets.UTF_8);
        INFOREWorkflow inforeWorkflow = JSONSingleton.fromJson(workflowContents, INFOREWorkflow.class);
        Assertions.assertNotNull(inforeWorkflow);

        String optreqContents = new String(Files.readAllBytes(Paths.get(ownPath + "/request.json")), StandardCharsets.UTF_8);
        OptimizationRequest optimizationRequest = JSONSingleton.fromJson(optreqContents, OptimizationRequest.class);
        Assertions.assertNotNull(optimizationRequest);

        //Submit the necessary resources to the optimizer
        ResponseEntity<String> res_network = restTemplate.exchange(host + "/resource/network", HttpMethod.PUT, new HttpEntity<>(networkContents, AuthHeaders), String.class);
        Assertions.assertEquals(res_network.getStatusCode(), HttpStatus.CREATED);
        String net_body = res_network.getBody();
        Assertions.assertNotNull(net_body);
        boolean network_created = net_body.equals("Created");
        Assertions.assertTrue(net_body.matches("^(Created|Updated).*"));

        ResponseEntity<String> res_dict = restTemplate.exchange(host + "/resource/dictionary", HttpMethod.PUT, new HttpEntity<>(dictionaryContents, AuthHeaders), String.class);
        Assertions.assertEquals(res_dict.getStatusCode(), HttpStatus.CREATED);
        String dict_body = res_dict.getBody();
        Assertions.assertNotNull(dict_body);
        boolean dict_created = net_body.equals("Created");
        Assertions.assertTrue(dict_body.matches("^(Created|Updated).*"));

        ResponseEntity<String> res_wf = restTemplate.exchange(host + "/resource/workflow", HttpMethod.PUT, new HttpEntity<>(workflowContents, AuthHeaders), String.class);
        Assertions.assertEquals(res_wf.getStatusCode(), HttpStatus.CREATED);
        String wf_body = res_wf.getBody();
        Assertions.assertNotNull(wf_body);
        boolean workflow_created = net_body.equals("Created");
        Assertions.assertTrue(wf_body.matches("^(Created|Updated).*"));

        //Submit the optimization request
        ResponseEntity<String> optimized_workflow_response = restTemplate.postForEntity(host + "/request/optimize", new HttpEntity<>(optreqContents, AuthHeaders), String.class);
        String ow_body = optimized_workflow_response.getBody();
        Assertions.assertNotNull(ow_body);
        Assertions.assertEquals(optimized_workflow_response.getStatusCode(), HttpStatus.OK, ow_body);
        Assertions.assertFalse(ow_body.isEmpty());
        Files.write(Paths.get(ownPath + "/response.json"), ow_body.getBytes());

        //Cleanup
        if (network_created) {
            ResponseEntity<String> delres1 = restTemplate.exchange(host + "/resource/network/" + inforeNetwork.getNetwork(),
                    HttpMethod.DELETE, new HttpEntity<>(AuthHeaders), String.class);
            Assertions.assertEquals(delres1.getStatusCode(), HttpStatus.NO_CONTENT);
        }

        if (dict_created) {
            ResponseEntity<String> delres2 = restTemplate.exchange(host + "/resource/dictionary/" + inforeDictionary.getDictionaryName(),
                    HttpMethod.DELETE, new HttpEntity<>(AuthHeaders), String.class);
            Assertions.assertEquals(delres2.getStatusCode(), HttpStatus.NO_CONTENT);
        }

        if (workflow_created) {
            ResponseEntity<String> delres3 = restTemplate.exchange(host + "/resource/workflow/" + inforeWorkflow.getWorkflowName(),
                    HttpMethod.DELETE, new HttpEntity<>(AuthHeaders), String.class);
            Assertions.assertEquals(delres3.getStatusCode(), HttpStatus.NO_CONTENT);
        }
    }
}
