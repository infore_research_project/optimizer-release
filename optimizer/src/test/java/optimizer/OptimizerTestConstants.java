package optimizer;


import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.time.ZoneId;
import java.util.Random;

import static org.springframework.http.HttpStatus.Series.CLIENT_ERROR;
import static org.springframework.http.HttpStatus.Series.SERVER_ERROR;

/**
 * Helper class with resources used in optimizer tests.
 */
public class OptimizerTestConstants {
    //If true the tests will use a mock version of the OptimizerApplication
    //else the user is responsible for spinning up an app instance and setting the 'host' URL below.
    public static final boolean useMockApp = false;

    // Setup system properties
    public static final String host = System.getProperty("targetHost") == null
            ? "http://optimizer:8080"
            : System.getProperty("targetHost");

    public static final String contentRoot = System.getProperty("contentRoot") == null
            ? "/input"
            : System.getProperty("contentRoot");

    //When running from inside a container
    public static final String lifeSciencePath = contentRoot + "/life_science";
    public static final String maritimePath = contentRoot + "/maritime";
    public static final String ownPath = contentRoot + "/own";

    public static final ZoneId zoneID = ZoneId.of("Europe/Athens");
    public static final Random random = new Random(0);
    public static RestTemplate restTemplate = initRestTemplate();
    public static HttpHeaders AuthHeaders = initHeaders();

    private OptimizerTestConstants() {
        //Avoid instantiation
    }

    private static HttpHeaders initHeaders() {
        HttpHeaders AuthHeaders = new HttpHeaders();
        AuthHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        AuthHeaders.add(HttpHeaders.ACCEPT, MediaType.ALL_VALUE);
        AuthHeaders.setBasicAuth("infore_user", "infore_pass");
        return AuthHeaders;
    }

    private static RestTemplate initRestTemplate() {
        SimpleClientHttpRequestFactory clientHttpRequestFactory = new SimpleClientHttpRequestFactory();
        clientHttpRequestFactory.setConnectTimeout(300_000);
        clientHttpRequestFactory.setReadTimeout(300_000);


        RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory);
        restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {

            @Override
            public boolean hasError(ClientHttpResponse httpResponse) throws IOException {
                return (httpResponse.getStatusCode().series() == CLIENT_ERROR || httpResponse.getStatusCode().series() == SERVER_ERROR);
            }

            @Override
            public void handleError(ClientHttpResponse httpResponse) throws IOException {
                if (httpResponse.getStatusCode().series() == SERVER_ERROR) {
                    // handle SERVER_ERROR
                } else if (httpResponse.getStatusCode().series() == CLIENT_ERROR) {
                    // handle CLIENT_ERROR
                }
            }
        });
        return restTemplate;
    }
}
