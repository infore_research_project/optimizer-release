package optimizer.experimental.visitor;

import optimizer.experimental.context.OperatorContext;
import optimizer.experimental.graph.Vertex;

import java.util.Iterator;

public class PrintGraphVisitor implements GraphVisitor<OperatorContext> {

    @Override
    public void visit(Vertex<OperatorContext> node) {
        System.out.println("Visiting: " + node.toString());
    }

    @Override
    public Iterable<OperatorContext> result() {
        return () -> new Iterator<OperatorContext>() {
            @Override
            public boolean hasNext() {
                return false;
            }

            @Override
            public OperatorContext next() {
                return null;
            }
        };
    }
}
