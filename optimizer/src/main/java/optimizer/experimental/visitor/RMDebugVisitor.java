package optimizer.experimental.visitor;

import optimizer.experimental.context.RMOperatorImplementation;
import optimizer.experimental.graph.Vertex;
import optimizer.experimental.plan.RMOptimizationPlan;
import optimizer.parser.workflow.Operator;

import java.util.Iterator;

public class RMDebugVisitor implements GraphVisitor<RMOptimizationPlan> {

    @Override
    public void visit(Vertex<RMOptimizationPlan> vertex) {
        RMOptimizationPlan curPlan = vertex.getData();
        StringBuilder result = new StringBuilder("Plan [" + curPlan.signature() + "] with operators ");
        for (Vertex<Operator> operatorVtx : curPlan.getOperatorGraph().getVertices()) {
            Operator op = operatorVtx.getData();
            RMOperatorImplementation impl = (RMOperatorImplementation) curPlan.getImplMap().get(op);
            result.append(" {")
                    .append("Operator[").append(op.getName()).append("] ")
                    .append("Site[").append(impl.getSite().getSiteName()).append("] ")
                    .append("Platform[").append(impl.getPlatform().getPlatformName()).append("]")
                    .append("} ");
        }
        System.out.println(result);
    }

    @Override
    public Iterable<RMOptimizationPlan> result() {
        return () -> new Iterator<RMOptimizationPlan>() {
            @Override
            public boolean hasNext() {
                return false;
            }

            @Override
            public RMOptimizationPlan next() {
                return null;
            }
        };
    }
}
