package optimizer.experimental.visitor;

import optimizer.experimental.context.VertexContext;
import optimizer.experimental.graph.Vertex;

/**
 * Traverses a {@link optimizer.experimental.graph.DirectedAcyclicGraph} and performs an action on each visited vertex.
 *
 * @param <T> The vertex data type.
 */
public interface GraphVisitor<T extends VertexContext> {
    void visit(Vertex<T> vertex);

    Iterable<T> result();
}
