package optimizer.experimental.visitor;

import optimizer.experimental.context.OperatorContext;
import optimizer.experimental.context.OperatorImplementationContext;
import optimizer.experimental.context.RMOperatorImplementation;
import optimizer.experimental.graph.Edge;
import optimizer.experimental.graph.ThreadSafeDAG;
import optimizer.experimental.graph.Vertex;
import optimizer.experimental.iterable.DAGIterable;
import optimizer.experimental.plan.RMOptimizationPlan;
import optimizer.parser.network.AvailablePlatform;
import optimizer.parser.network.Site;
import optimizer.parser.workflow.Operator;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

//Site cost matrix (Site->Site) UTM
//Site1->Site1 = 0 cost
public class MinCostPlanVisitor implements GraphVisitor<RMOptimizationPlan> {
    private final int DEFAULT_COST = 1;
    private final int DEFAULT_PLATFORM_COST = 1000;
    private final int DEFAULT_INPUT_DATA_SIZE = 100000;
    private final Map<String, Integer> cost;
    private final Random RNG;
    private RMOptimizationPlan bestPlan;
    private int counter;

    public MinCostPlanVisitor() {
        this.counter = 0;
        this.cost = new HashMap<>();
        this.cost.put("flink", 10);
        this.cost.put("spark", DEFAULT_PLATFORM_COST + 1);
        this.cost.put("spark_structured_streaming", DEFAULT_PLATFORM_COST + 1);
        this.RNG = new Random(0);
    }

    private void updatePlanCost(RMOptimizationPlan curPlan) {
        ThreadSafeDAG<Operator> graph = curPlan.getOperatorGraph();
        Map<OperatorContext, OperatorImplementationContext> implMap = curPlan.getImplMap();
        int cost = 0;

        for (Vertex<Operator> vtx : new DAGIterable<>(graph)) {
            Operator op1 = vtx.getData();
            RMOperatorImplementation impl1 = (RMOperatorImplementation) implMap.get(op1);
            cost += estimateOperatorCost(op1);
            cost += estimatePlatformCost(impl1.getPlatform());
            cost += estimateSiteCost(impl1.getSite());
            int migrationCost = 0;
            for (Edge<Operator> edge : graph.edgesOf(vtx)) {
                Operator op2 = edge.getSecond().getData();
                RMOperatorImplementation impl2 = (RMOperatorImplementation) implMap.get(op2);
                migrationCost = Math.max(migrationCost, estimateMigrationCost(impl1, impl2));
            }
            cost += migrationCost;
        }

        //Update final cost
        curPlan.setCost(cost);
    }

    public int estimatePlatformCost(AvailablePlatform platform) {
        return this.cost.getOrDefault(platform.getPlatformName(), DEFAULT_PLATFORM_COST + 2 + RNG.nextInt(DEFAULT_PLATFORM_COST));
    }

    //TODO expand with the site matrix
    public int estimateSiteCost(Site site) {
        return DEFAULT_COST;
    }

    public int estimateMigrationCost(RMOperatorImplementation op1, RMOperatorImplementation op2) {
        int outputDataSize = (int) (0.1 * DEFAULT_INPUT_DATA_SIZE);
        return op1.getPlatform().equals(op2.getPlatform())
                ? outputDataSize / 10000
                : outputDataSize / 1000;
    }

    public int estimateOperatorCost(Operator op) {
        return DEFAULT_COST;
    }

    @Override
    public void visit(Vertex<RMOptimizationPlan> vertex) {
        RMOptimizationPlan curPlan = vertex.getData();
        updatePlanCost(curPlan);
        if (this.bestPlan == null || curPlan.compareTo(this.bestPlan) > 0) {
            this.bestPlan = curPlan;
        }
        this.counter++;
    }


    @Override
    public Iterable<RMOptimizationPlan> result() {
        return () -> new Iterator<RMOptimizationPlan>() {
            @Override
            public boolean hasNext() {
                return bestPlan != null;
            }

            @Override
            public RMOptimizationPlan next() {
                return bestPlan;
            }
        };
    }

    public RMOptimizationPlan getBestPlan() {
        return this.bestPlan;
    }

    public int getNumOfVisitedPlans() {
        return this.counter;
    }

    public Map<OperatorContext, Map<String, Integer>> getCostMap(RMOptimizationPlan plan) {
        Map<OperatorContext, OperatorImplementationContext> implementations = plan.getImplMap();
        ThreadSafeDAG<Operator> graph = plan.getOperatorGraph();
        Map<OperatorContext, Map<String, Integer>> costMap = new HashMap<>();
        for (Vertex<Operator> vtx : new DAGIterable<>(graph)) {
            Operator op1 = vtx.getData();
            RMOperatorImplementation impl1 = (RMOperatorImplementation) implementations.get(op1);
            Map<String, Integer> costs = new HashMap<>();
            costs.put("site", estimateSiteCost(impl1.getSite()));
            costs.put("platform", estimatePlatformCost(impl1.getPlatform()));
            costs.put("operator", estimateOperatorCost(op1));
            int migration_cost = 0;
            for (Edge<Operator> edge : graph.edgesOf(vtx)) {
                Operator op2 = edge.getSecond().getData();
                RMOperatorImplementation impl2 = (RMOperatorImplementation) implementations.get(op2);
                migration_cost = Math.max(migration_cost, estimateMigrationCost(impl1, impl2));
            }
            costs.put("migration", migration_cost);
            costMap.put(op1, costs);
        }
        return costMap;
    }

}
