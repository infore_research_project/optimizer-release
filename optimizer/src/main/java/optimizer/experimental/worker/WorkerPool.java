package optimizer.experimental.worker;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

import static optimizer.OptimizerApplication.masterLogger;

public class WorkerPool {
    //Fields
    private final ExecutorService executorService;
    private final List<Future<?>> queuedFutures;
    private final List<WorkerRunnable> queuedRunnables;

    public WorkerPool(int cores) {
        ThreadFactory workerThreadFactory = new WorkerThreadFactory();
        this.executorService = Executors.newFixedThreadPool(cores, workerThreadFactory);
        this.queuedFutures = new ArrayList<>();
        this.queuedRunnables = new ArrayList<>();
        masterLogger.debug("Worker pool created");
    }

    public void submit(WorkerRunnable workerRunnable) {
        if (this.executorService.isShutdown()) {
            throw new IllegalStateException("Worker pool has shutdown.");
        }
        this.queuedRunnables.add(workerRunnable);
        Future<?> future = this.executorService.submit(workerRunnable);
        this.queuedFutures.add(future);
    }

    //Waiting for existing threads to complete their execution
    public void shutdown(int timeout) {
        // Disable new tasks from being submitted
        this.executorService.shutdown();
        try {
            // Wait a while for existing tasks to terminate
            if (!this.executorService.awaitTermination(timeout, TimeUnit.MILLISECONDS)) {
                this.executorService.shutdownNow(); // Cancel currently executing tasks
                // Wait a while for tasks to respond to being cancelled
                // ...
            }
        } catch (InterruptedException ie) {
            // (Re-)Cancel if current thread also interrupted
            this.executorService.shutdownNow();
            // Preserve interrupt status
            Thread.currentThread().interrupt();
        }
    }

    public void shutdownNow() {
        this.executorService.shutdownNow();
    }

    public void getAll() throws ExecutionException, InterruptedException {
        for (Future<?> future : this.queuedFutures) {
            future.get();
        }
    }

    public void getAllNow() throws ExecutionException, InterruptedException {
        for (Future<?> future : this.queuedFutures) {
            try {
                future.get(0, TimeUnit.NANOSECONDS);
            } catch (TimeoutException ignored) {
            }
        }
    }

    public void cancelAll() {
        for (Future<?> future : this.queuedFutures) {
            future.cancel(true);
        }
    }

    public List<Future<?>> getQueuedFutures() {
        return queuedFutures;
    }

    public List<WorkerRunnable> getQueuedRunnables() {
        return queuedRunnables;
    }
}
