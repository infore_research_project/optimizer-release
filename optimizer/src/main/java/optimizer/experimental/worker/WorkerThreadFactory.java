package optimizer.experimental.worker;

import java.util.concurrent.ThreadFactory;

public class WorkerThreadFactory implements ThreadFactory {
    private ThreadGroup threadGroup;
    private int id;
    private long stackSize;

    public WorkerThreadFactory() {
        this.id = 0;
        this.stackSize = 0; //Default
        this.threadGroup = new ThreadGroup("WorkerThreadGroup");
    }

    @Override
    public Thread newThread(Runnable r) {
        return new Thread(this.threadGroup, r, "WorkerThread" + this.id++, this.stackSize);
    }
}
