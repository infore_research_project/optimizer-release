package optimizer.experimental.worker;

import optimizer.experimental.exception.GraphException;
import optimizer.experimental.graph.DirectedAcyclicGraph;
import optimizer.experimental.graph.Edge;
import optimizer.experimental.graph.Vertex;
import optimizer.experimental.plan.RMOptimizationPlan;
import optimizer.experimental.transformer.RMPlanGenerator;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentLinkedQueue;

import static optimizer.OptimizerApplication.masterLogger;

public class WorkerTask implements WorkerRunnable {
    private final DirectedAcyclicGraph<RMOptimizationPlan> graph;
    private final RMPlanGenerator planGenerator;
    private final int workerID;
    private final ConcurrentLinkedQueue<Vertex<RMOptimizationPlan>> globalQueue;
    private final long TIMER_PERIOD_MS = 3000;
    private String lastThreadName;

    private int plansExplored;
    private int collisions;
    private int plansPerWindow;

    public WorkerTask(int workerID, WorkerGlobalContext sharedContext) {
        this.workerID = workerID;
        this.collisions = 0;
        this.plansExplored = 0;
        this.plansPerWindow = 0;
        this.lastThreadName = Thread.currentThread().getName();
        this.planGenerator = (RMPlanGenerator) sharedContext.get("planGenerator");
        this.graph = (DirectedAcyclicGraph<RMOptimizationPlan>) sharedContext.get("planGraph");
        this.globalQueue = (ConcurrentLinkedQueue<Vertex<RMOptimizationPlan>>) sharedContext.get("planQueue");
    }

    public void registerTimerTask(Timer timer) {
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (shouldContinue()) {
                    masterLogger.debug(String.format("Worker [%d] running on [%s]: Explored plans=[%07d], Collisions=[%07d], Ratio=[%04f], Plans/sec=[%06d]",
                            workerID, lastThreadName, plansExplored, collisions,
                            collisions == 0 ? 0 : (plansExplored / (double) collisions),
                            Math.round(1000 * plansPerWindow / (double) TIMER_PERIOD_MS)));
                    //Reset plans per window
                    plansPerWindow = 0;
                }
            }
        }, TIMER_PERIOD_MS, TIMER_PERIOD_MS);
    }

    @Override
    public void run() {
        //Set this thread as the last one that scheduled this task
        this.lastThreadName = Thread.currentThread().getName();

        try {
            //A single thread will retrieve the root plan and process it
            Vertex<RMOptimizationPlan> rootVtx = this.globalQueue.poll();

            //Keep processing the plan graph as long as the executor service hasn't interrupted this thread (via a shutdown method)
            while (shouldContinue()) {

                //Block and wait for a plan to arrive
                Vertex<RMOptimizationPlan> curVtx = rootVtx != null ? rootVtx : this.globalQueue.poll();
                rootVtx = null;

                //Continue polling the queue until a plan arrives
                if (curVtx == null) {
                    continue;
                }

                //Try to produce as many plans as possible from the currently selected one
                RMOptimizationPlan curPlan = curVtx.getData();

                while (true) {
                    //Generate a brand new plan
                    RMOptimizationPlan newPlan = this.planGenerator.generate(curPlan);

                    //Can't expand this plan anymore, break and try a new vertex-plan
                    if (newPlan == null) {
                        this.collisions++;
                        break;
                    }
                    //Add the new plan to the graph
                    Vertex<RMOptimizationPlan> nextVtx = new Vertex<>(newPlan);
                    Edge<RMOptimizationPlan> newEdge = new Edge<>(curVtx, nextVtx);
                    this.graph.addVertex(nextVtx);
                    this.graph.addEdge(newEdge);
                    this.globalQueue.offer(nextVtx);

                    //Stats update
                    this.plansExplored++;
                    this.plansPerWindow++;
                }
            }
        } catch (GraphException e) {
            masterLogger.error(e);
            return;
        }

        //Reached upon the search space is exhausted
        masterLogger.info("Thread" + this.lastThreadName + " finished its run gracefully.");
    }

    private boolean shouldContinue() {
        return !Thread.currentThread().isInterrupted();
    }

    @Override
    public int getExploredPlans() {
        return this.plansExplored;
    }

    @Override
    public int getCollisions() {
        return this.collisions;
    }

    @Override
    public String toString() {
        return "WorkerTask{" +
                "plan graph size='" + graph.size() + '\'' +
                ", workerId='" + workerID + '\'' +
                ", plansExplored=" + plansExplored +
                ", collisions=" + collisions +
                ", last scheduled on=" + lastThreadName +
                '}';
    }
}
