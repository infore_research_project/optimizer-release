package optimizer.experimental.worker;

public interface WorkerRunnable extends Runnable {

    int getExploredPlans();

    int getCollisions();
}
