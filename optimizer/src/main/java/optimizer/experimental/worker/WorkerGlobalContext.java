package optimizer.experimental.worker;

import java.util.HashMap;
import java.util.Map;

public class WorkerGlobalContext {
    Map<String, Object> sharedMap;

    public WorkerGlobalContext() {
        this.sharedMap = new HashMap<>();
    }

    public void put(String key, Object obj) {
        this.sharedMap.put(key, obj);
    }

    public Object get(String key) {
        return this.sharedMap.get(key);
    }
}
