package optimizer.experimental.plan;

import optimizer.experimental.action.Action;
import optimizer.experimental.context.OperatorContext;
import optimizer.experimental.context.OperatorImplementationContext;
import optimizer.experimental.context.RMOperatorImplementation;
import optimizer.experimental.exception.GraphException;
import optimizer.experimental.graph.ThreadSafeDAG;
import optimizer.experimental.graph.Vertex;
import optimizer.experimental.utils.HashUtil;
import optimizer.graph.implementation.OperatorImplementationVertex;
import optimizer.graph.operator.OperatorVertex;
import optimizer.parser.network.AvailablePlatform;
import optimizer.parser.network.INFORENetwork;
import optimizer.parser.network.Site;
import optimizer.parser.workflow.Operator;
import optimizer.plan.AbstractOptimizerPlan;
import optimizer.plan.OptimizerPlan;
import optimizer.utils.JSONSingleton;

import java.util.HashMap;
import java.util.Map;

public class RMOptimizationPlan implements OptimizationPlan<Operator> {
    private final ThreadSafeDAG<Operator> graph;
    private final Map<OperatorContext, OperatorImplementationContext> implMap;
    private Action action;
    private int cost;

    public RMOptimizationPlan(ThreadSafeDAG<Operator> operatorGraph, INFORENetwork network) {
        this.graph = operatorGraph;
        this.implMap = new HashMap<>();
        for (Vertex<Operator> operatorVertex : graph.getVertices()) {
            Operator curOp = operatorVertex.getData();
            RMOperatorImplementation startingImpl = getDefaultImpl(network);
            this.implMap.put(curOp, startingImpl);
        }
        this.cost = Integer.MAX_VALUE;
        this.action = Action.CHANGE_SITE;
    }

    public RMOptimizationPlan(RMOptimizationPlan copy) throws GraphException {
        this.graph = new ThreadSafeDAG<>(copy.getOperatorGraph());
        this.action = Action.valueOf(copy.getAction().name());
        this.cost = copy.cost();
        this.implMap = new HashMap<>();
        for (OperatorContext operatorCtx : copy.getImplMap().keySet()) {
            Operator operator = (Operator) operatorCtx;
            RMOperatorImplementation copy_impl = (RMOperatorImplementation) copy.getImplMap().get(operator);
            this.implMap.put(operator, new RMOperatorImplementation(copy_impl.getSite(), copy_impl.getPlatform()));
        }
    }

    @Override
    public ThreadSafeDAG<Operator> getOperatorGraph() {
        return this.graph;
    }

    @Override
    public int cost() {
        return this.cost;
    }

    @Override
    public Map<OperatorContext, OperatorImplementationContext> getImplMap() {
        return this.implMap;
    }

    @Override
    public Action getAction() {
        return this.action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    @Override
    public String signature() {
        return HashUtil.planSignature(graph, implMap);
    }

    @Override
    public int compareTo(OptimizationPlan<Operator> o) {
        return -(this.cost() - o.cost());
    }

    private RMOperatorImplementation getDefaultImpl(INFORENetwork network) {
        Site site = network.getSites().iterator().next();
        AvailablePlatform platform = site.getAvailablePlatforms().get(0);
        return new RMOperatorImplementation(site, platform);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RMOptimizationPlan that = (RMOptimizationPlan) o;

        if (cost != that.cost) return false;
        if (!graph.equals(that.graph)) return false;
        if (!implMap.equals(that.implMap)) return false;
        return action == that.action;
    }

    @Override
    public int hashCode() {
        return HashUtil.hashPlan(graph, implMap);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("RMOptimizationPlan {").append("\n");
        for (OperatorContext ctx : this.implMap.keySet()) {
            Operator op = (Operator) ctx;
            RMOperatorImplementation oi = (RMOperatorImplementation) this.implMap.get(op);
            sb.append("\t").append(op.getName()).append(" -> ").append(oi.toString()).append("\n");
        }
        sb.append("Cost: ").append(this.cost).append("\n");
        sb.append("Signature: ").append(this.signature()).append("\n");
        sb.append("}");
        return sb.toString();
    }

    public void setCost(int cost) {
        this.cost = cost;
    }
}
