package optimizer.experimental.plan;

import optimizer.experimental.action.Action;
import optimizer.experimental.context.OperatorContext;
import optimizer.experimental.context.OperatorImplementationContext;
import optimizer.experimental.context.VertexContext;
import optimizer.experimental.graph.DirectedAcyclicGraph;

import java.util.Map;

public interface OptimizationPlan<C extends OperatorContext> extends Comparable<OptimizationPlan<C>>, VertexContext {

    DirectedAcyclicGraph<C> getOperatorGraph();

    int cost();

    Map<OperatorContext, OperatorImplementationContext> getImplMap();

    Action getAction();
}
