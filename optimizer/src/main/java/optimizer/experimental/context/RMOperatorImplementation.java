package optimizer.experimental.context;

import optimizer.parser.network.AvailablePlatform;
import optimizer.parser.network.Site;

public class RMOperatorImplementation implements OperatorImplementationContext {
    private Site site;
    private AvailablePlatform platform;

    public RMOperatorImplementation(Site site, AvailablePlatform platform) {
        this.site = site;
        this.platform = platform;
    }

    //SHALLOW COPY
    public RMOperatorImplementation(RMOperatorImplementation curImpl) {
        this.site = curImpl.site;
        this.platform = curImpl.platform;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RMOperatorImplementation that = (RMOperatorImplementation) o;

        if (site != null ? !site.equals(that.site) : that.site != null) return false;
        return platform != null ? platform.equals(that.platform) : that.platform == null;
    }

    @Override
    public int hashCode() {
        int result = site != null ? site.hashCode() : 0;
        result = 31 * result + (platform != null ? platform.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "RMOperatorImplementation{" +
                "site=" + (site != null ? site.getSiteName() : null) +
                ", platform=" + (platform != null ? platform.getPlatformName() : null) +
                '}';
    }

    @Override
    public String signature() {
        return String.valueOf(hashCode());
    }

    public Site getSite() {
        return site;
    }

    public void setSite(Site selectedSite) {
        this.site = selectedSite;
    }

    public AvailablePlatform getPlatform() {
        return platform;
    }

    public void setPlatform(AvailablePlatform platform) {
        this.platform = platform;
    }
}
