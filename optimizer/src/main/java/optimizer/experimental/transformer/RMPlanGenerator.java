package optimizer.experimental.transformer;

import optimizer.experimental.action.Action;
import optimizer.experimental.action.RMPlanActions;
import optimizer.experimental.context.RMOperatorImplementation;
import optimizer.experimental.exception.GraphException;
import optimizer.experimental.plan.RMOptimizationPlan;
import optimizer.experimental.utils.HashUtil;
import optimizer.experimental.utils.Triple;
import optimizer.parser.workflow.Operator;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class RMPlanGenerator implements PlanGenerator<RMOptimizationPlan> {
    private final RMPlanActions actions;
    private final Set<String> visitedSignatures;
    private final Map<String, Lock> pendingSignatures;

    public RMPlanGenerator(RMPlanActions planAction, Set<String> visitedSignatures, Map<String, Lock> pendingSignatures) {
        this.actions = planAction;
        this.visitedSignatures = visitedSignatures;
        this.pendingSignatures = pendingSignatures;
    }

    //Returns <plan,shouldContinue>
    @Override
    public RMOptimizationPlan generate(RMOptimizationPlan oldPlan) {
        //Pick a random action
        Triple<Operator, Action, RMOperatorImplementation> triple = actions.pickAction(oldPlan);
        if (triple == null) {
            return null;
        }
        Operator selectedOperator = triple.getFirst();
        Action selectedAction = triple.getSecond();
        RMOperatorImplementation selectedImplementation = triple.getThird();

        // Create a copy of the old plan
        RMOptimizationPlan newPlan;
        try {
            newPlan = new RMOptimizationPlan(oldPlan);
        } catch (GraphException e) {
            return null;
        }

        // Change the plan by performing that action and check if we have produced this plan before
        actions.performAction(newPlan, selectedAction, selectedOperator, selectedImplementation);

        // Check for collisions
        String candidate_signature = HashUtil.planSignature(newPlan.getOperatorGraph(), newPlan.getImplMap());

        //The ConcurrentHashMap provides the putIfAbsent() operation which makes sure this is done in an atomic way
        this.pendingSignatures.putIfAbsent(candidate_signature, new ReentrantLock());

        //Mark this signature as pending
        if (this.pendingSignatures.get(candidate_signature).tryLock()) {
            //Check if plan has already been visited
            boolean planAlreadyExists = this.visitedSignatures.contains(newPlan.signature());

            // Add the plan to visited and release it from the pending set
            this.visitedSignatures.add(newPlan.signature());
            this.pendingSignatures.get(candidate_signature).unlock();

            //Return the plan iff its unique
            return planAlreadyExists ? null : newPlan;
        } else {
            //Collision, someone else is processing this plan
            return null;
        }
    }
}
