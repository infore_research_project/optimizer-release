package optimizer.experimental.transformer;

import optimizer.experimental.plan.RMOptimizationPlan;

public interface PlanGenerator<T> {
    RMOptimizationPlan generate(T plan);
}
