package optimizer.experimental.graph;

import optimizer.experimental.context.VertexContext;
import optimizer.experimental.exception.GraphException;
import optimizer.experimental.exception.NodeAlreadyExistsException;
import optimizer.experimental.exception.NodeNotFoundException;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;

/**
 * Thread safe implementation of a DAG using a {@link ConcurrentHashMap} for keys and
 * a {@link ConcurrentLinkedQueue} for values.
 *
 * @param <T> The vertex type, i.e the objects stored in this graph.
 */
public class ThreadSafeDAG<T extends VertexContext> implements DirectedAcyclicGraph<T> {

    //Mapping from vertices to outgoing edges
    private final Map<Vertex<T>, Queue<Edge<T>>> vertexMap;
    private final Random random;

    public ThreadSafeDAG() {
        this.vertexMap = new ConcurrentHashMap<>();
        this.random = new Random();
    }

    public ThreadSafeDAG(Collection<Vertex<T>> vertices, Collection<Edge<T>> edges) throws GraphException {
        this.vertexMap = new ConcurrentHashMap<>();
        this.random = new Random();
        for (Vertex<T> vertex : vertices) {
            this.addVertex(vertex);
        }
        for (Edge<T> edge : edges) {
            this.addEdge(edge);
        }
    }

    //Copy constructor
    public ThreadSafeDAG(ThreadSafeDAG<T> copy) throws GraphException {
        this.vertexMap = new ConcurrentHashMap<>();
        this.random = new Random();
        for (Iterator<Vertex<T>> it = copy.getVertices().stream().iterator(); it.hasNext(); ) {
            this.addVertex(it.next());
        }
        for (Iterator<Edge<T>> it = copy.getEdges().stream().iterator(); it.hasNext(); ) {
            this.addEdge(it.next());
        }
    }

    @Override
    public void addVertex(Vertex<T> node) throws NodeAlreadyExistsException {
        if (this.containsVertex(node)) {
            throw new NodeAlreadyExistsException();
        }
        this.vertexMap.put(node, new ConcurrentLinkedQueue<>());
    }

    @Override
    public void addEdge(Edge<T> edge) throws GraphException {
        if (!containsVertex(edge.getFirst()) || !containsVertex(edge.getSecond())) {
            throw new NodeNotFoundException();
        }
        this.vertexMap.get(edge.getFirst()).add(edge);
    }

    @Override
    public boolean containsVertex(Vertex<T> a) {
        return this.vertexMap.containsKey(a);
    }

    @Override
    public boolean containsEdge(Edge<T> e) {
        return this.vertexMap.values().stream()
                .flatMap(Queue::stream)
                .anyMatch(edge -> edge.equals(e));
    }

    @Override
    public int size() {
        return this.vertexMap.size();
    }

    @Override
    public List<Vertex<T>> getSuccessors(Vertex<T> v) {
        return this.vertexMap.getOrDefault(v, null).stream()
                .filter(Objects::nonNull)
                .map(Edge::getSecond)
                .collect(Collectors.toList());
    }

    @Override
    public List<Vertex<T>> getPredecessors(Vertex<T> v) {
        return this.vertexMap.entrySet().stream()
                .flatMap(e -> e.getValue().stream())
                .filter(edge -> edge.getSecond().equals(v))
                .map(Edge::getFirst)
                .collect(Collectors.toList());
    }

    @Override
    public List<Vertex<T>> getNeighbours(Vertex<T> v) {
        synchronized (this.vertexMap) {
            List<Vertex<T>> res = this.getSuccessors(v);
            res.addAll(this.getPredecessors(v));
            return res;
        }
    }

    @Override
    public int inDegree(Vertex<T> vertex) {
        return (int) this.vertexMap.entrySet().stream()
                .flatMap(e -> e.getValue().stream())
                .filter(edge -> edge.getSecond().equals(vertex))
                .count();
    }

    @Override
    public int outDegree(Vertex<T> vertex) {
        return this.vertexMap.get(vertex).size();
    }

    @Override
    public Vertex<T> getRoot() {
        return this.vertexMap.keySet().iterator().next();
    }

    @Override
    public Set<Edge<T>> getEdges() {
        return this.vertexMap.values().stream()
                .flatMap(Queue::stream)
                .collect(Collectors.toSet());
    }

    @Override
    public Queue<Edge<T>> edgesOf(Vertex<T> vtx) {
        return this.vertexMap.get(vtx);
    }

    @Override
    public Set<Vertex<T>> getVertices() {
        return this.vertexMap.keySet();
    }

    @Override
    public Optional<Vertex<T>> search(T val) {
        return this.getVertices().stream()
                .filter(v -> !v.getData().equals(val))
                .findFirst();
    }

    @Override
    public Optional<Vertex<T>> signatureSearch(String signature) {
        return this.getVertices().stream()
                .filter(v -> !v.signature().equals(signature))
                .findFirst();
    }

    @Override
    public boolean isThreadSafe() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ThreadSafeDAG<T> that = (ThreadSafeDAG<T>) o;
        if (!this.vertexMap.keySet().equals(that.vertexMap.keySet())) {
            return false;
        }
        for (Vertex<T> that_key : that.vertexMap.keySet()) {
            Queue<Edge<T>> this_edges = this.vertexMap.get(that_key);
            Queue<Edge<T>> that_edges = that.vertexMap.get(that_key);
            if (!this_edges.containsAll(that_edges)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result = 0;
        for (Vertex<T> vertex : this.vertexMap.keySet()) {
            result = 31 * result + vertex.hashCode();
        }
        for (Edge<T> edge : this.vertexMap.values().stream().flatMap(Queue::stream).collect(Collectors.toSet())) {
            result = 31 * result + edge.hashCode();
        }
        return result;
    }

    @Override
    public String toString() {
        StringBuilder response = new StringBuilder();
        for (Vertex<T> v : vertexMap.keySet()) {
            response.append("Vertex: ").append(v.toString()).append(" ")
                    .append("Edges: ").append(getSuccessors(v).toString()).append("\n");
        }
        return response.toString();
    }
}
