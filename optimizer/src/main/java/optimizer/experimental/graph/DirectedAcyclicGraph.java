package optimizer.experimental.graph;

import optimizer.experimental.context.VertexContext;
import optimizer.experimental.exception.GraphException;
import optimizer.experimental.exception.NodeAlreadyExistsException;

import java.util.List;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;

public interface DirectedAcyclicGraph<T extends VertexContext> {
    void addVertex(Vertex<T> node) throws NodeAlreadyExistsException;

    void addEdge(Edge<T> edge) throws GraphException;

    boolean isThreadSafe();

    boolean containsVertex(Vertex<T> a);

    boolean containsEdge(Edge<T> e);

    int size();

    List<Vertex<T>> getSuccessors(Vertex<T> v);

    List<Vertex<T>> getPredecessors(Vertex<T> v);

    List<Vertex<T>> getNeighbours(Vertex<T> v);

    int inDegree(Vertex<T> vertex);

    int outDegree(Vertex<T> vertex);

    Vertex<T> getRoot();

    Set<Edge<T>> getEdges();

    Queue<Edge<T>> edgesOf(Vertex<T> vtx);

    Set<Vertex<T>> getVertices();

    Optional<Vertex<T>> search(T val);

    Optional<Vertex<T>> signatureSearch(String signature);
}
