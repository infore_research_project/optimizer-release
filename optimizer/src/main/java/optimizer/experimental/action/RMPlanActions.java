package optimizer.experimental.action;

import optimizer.experimental.context.OperatorContext;
import optimizer.experimental.context.OperatorImplementationContext;
import optimizer.experimental.context.RMOperatorImplementation;
import optimizer.experimental.graph.ThreadSafeDAG;
import optimizer.experimental.graph.Vertex;
import optimizer.experimental.plan.RMOptimizationPlan;
import optimizer.experimental.utils.HashUtil;
import optimizer.experimental.utils.Triple;
import optimizer.parser.dictionary.INFOREDictionary;
import optimizer.parser.network.AvailablePlatform;
import optimizer.parser.network.INFORENetwork;
import optimizer.parser.network.Site;
import optimizer.parser.workflow.Operator;

import java.util.*;
import java.util.stream.Collectors;

public class RMPlanActions implements PlanActions {
    private final INFOREDictionary dictionary;
    private final INFORENetwork network;
    private final Set<String> visitedSignatures;

    public RMPlanActions(INFOREDictionary dictionary, INFORENetwork network, Set<String> visitedSignatures) {
        this.dictionary = dictionary;
        this.network = network;
        this.visitedSignatures = visitedSignatures;
    }

    /**
     * TODO  Consider a random iteration of (some) resources, works better on multi-core envs.
     */
    @Override
    public Triple<Operator, Action, RMOperatorImplementation> pickAction(final RMOptimizationPlan plan) {
        //Resources
        final ThreadSafeDAG<Operator> operatorGraph = plan.getOperatorGraph();
        final Map<OperatorContext, OperatorImplementationContext> candidateImplMap = copyImplMap(plan.getImplMap());

        //Pick an action
        for (Action action : genCandidateActions()) {

            //Pick an operator
            for (Vertex<Operator> operator : genCandidateOperators(operatorGraph.getVertices())) {
                Operator curOperator = operator.getData();
                RMOperatorImplementation candidateImpl = (RMOperatorImplementation) candidateImplMap.get(curOperator);

                //Pick an implementation for this operator
                switch (action) {
                    case CHANGE_SITE:
                        for (Iterator<Site> it = genCandidateSites(curOperator, candidateImpl); it.hasNext(); ) {
                            Site candidateSite = it.next();
                            candidateImpl.setSite(candidateSite);
                            String newSignature = HashUtil.planSignature(operatorGraph, candidateImplMap);
                            if (!visitedSignatures.contains(newSignature)) {
                                return new Triple<>(curOperator, action, candidateImpl);
                            }
                        }
                        break;
                    case CHANGE_PLATFORM:
                        for (Iterator<AvailablePlatform> it = genCandidatePlatforms(curOperator, candidateImpl); it.hasNext(); ) {
                            AvailablePlatform candidatePlatform = it.next();
                            candidateImpl.setPlatform(candidatePlatform);
                            String newSignature = HashUtil.planSignature(operatorGraph, candidateImplMap);
                            if (!visitedSignatures.contains(newSignature)) {
                                return new Triple<>(curOperator, action, candidateImpl);
                            }
                        }
                        break;
                    default:
                        throw new IllegalStateException(String.format("Default case in performAction for action [%s]", action));
                }
            }
        }

        //A new implementation was not found, exit gracefully
        return null;
    }

    private List<Vertex<Operator>> genCandidateOperators(Set<Vertex<Operator>> vertices) {
        List<Vertex<Operator>> list = new ArrayList<>(vertices);
        Collections.shuffle(list);
        return list;
    }

    private Map<OperatorContext, OperatorImplementationContext> copyImplMap(Map<OperatorContext, OperatorImplementationContext> implMap) {
        Map<OperatorContext, OperatorImplementationContext> map = new HashMap<>();
        for (OperatorContext oc : implMap.keySet()) {
            RMOperatorImplementation oic = (RMOperatorImplementation) implMap.get(oc);
            map.put(oc, new RMOperatorImplementation(oic));
        }
        return map;
    }

    private Action[] genCandidateActions() {
        return Action.values();
    }

    @Override
    public void performAction(RMOptimizationPlan plan, Action selectedAction, Operator operator, RMOperatorImplementation implementation) {
        plan.getImplMap().put(operator, implementation);
        plan.setAction(selectedAction);
    }

    private Iterator<Site> genCandidateSites(Operator operator, RMOperatorImplementation operatorImplementation) {
        return this.dictionary.getAvailableSites(operator, this.network).stream()
                .filter(site -> !site.equals(operatorImplementation.getSite())) // Don't chose the same site
                .filter(site -> site.getAvailablePlatforms().contains(operatorImplementation.getPlatform()))    // The new site must also support the existing platform
                .iterator();
    }

    private Iterator<AvailablePlatform> genCandidatePlatforms(Operator operator, RMOperatorImplementation operatorImplementation) {
        //The current site supports a limited amount of platforms
        final List<String> supportedPlatforms = operatorImplementation.getSite().getAvailablePlatforms().stream()
                .map(AvailablePlatform::getPlatformName)
                .collect(Collectors.toList());

        //Iterator of candidate platforms
        return this.dictionary.getAvailablePlatforms(operator, this.network).stream()
                .filter(platform -> !platform.equals(operatorImplementation.getPlatform())) // Don't chose the same platform
                .filter(platform -> supportedPlatforms.contains(platform.getPlatformName()))
                .iterator();
    }
}
