package optimizer.experimental.action;

public enum Action {
    CHANGE_SITE,
    CHANGE_PLATFORM,

    //ignore these for now since INFORE network doesn't contain clusters
    //CHANGE_CLUSTER,
    //SWAP_OPERATOR
}
