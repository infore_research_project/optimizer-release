package optimizer.experimental.action;

import optimizer.experimental.context.RMOperatorImplementation;
import optimizer.experimental.plan.RMOptimizationPlan;
import optimizer.experimental.utils.Triple;
import optimizer.parser.workflow.Operator;

public interface PlanActions {
    Triple<Operator, Action, RMOperatorImplementation> pickAction(RMOptimizationPlan plan);

    void performAction(RMOptimizationPlan plan, Action selectedAction, Operator operator, RMOperatorImplementation implementation);
}
