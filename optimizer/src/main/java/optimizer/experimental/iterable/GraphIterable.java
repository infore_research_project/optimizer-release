package optimizer.experimental.iterable;

import optimizer.experimental.context.VertexContext;
import optimizer.experimental.graph.DirectedAcyclicGraph;
import optimizer.experimental.graph.Vertex;

/**
 * Algorithms that traverse a {@link DirectedAcyclicGraph} without modifying any vertices or edges.
 *
 * @param <T> The type encapsulated by the vertices.
 */
public interface GraphIterable<T extends VertexContext> extends Iterable<Vertex<T>> {
}
