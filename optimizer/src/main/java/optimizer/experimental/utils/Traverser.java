package optimizer.experimental.utils;


import optimizer.experimental.context.VertexContext;
import optimizer.experimental.exception.GraphException;
import optimizer.experimental.graph.ThreadSafeDAG;
import optimizer.experimental.graph.Vertex;
import optimizer.experimental.iterable.BFSIterable;
import optimizer.experimental.iterable.DAGIterable;
import optimizer.experimental.iterable.DFSIterable;
import optimizer.experimental.iterable.GraphIterable;
import optimizer.experimental.visitor.GraphVisitor;

import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Class used to launch graph traversal algorithms in separate threads.
 */
public final class Traverser {
    private static final ExecutorService executor = Executors.newFixedThreadPool(4);

    public static <T extends VertexContext> void dfs(ThreadSafeDAG<T> graph, GraphVisitor<T> visitor) throws GraphException {
        dispatch(new DFSIterable<>(graph), visitor);
    }

    public static <T extends VertexContext> void topologicalOrder(ThreadSafeDAG<T> graph, GraphVisitor<T> visitor) {
        dispatch(new DAGIterable<>(graph), visitor);
    }

    public static <T extends VertexContext> Future<?> bfs(ThreadSafeDAG<T> graph, Vertex<T> source, GraphVisitor<T> visitor) {
        return dispatch(new BFSIterable<>(graph, source), visitor);
    }

    /**
     * Dispatches tasks to the executor as {@link Task<T>}.
     */
    private static <T extends VertexContext> Future<?> dispatch(GraphIterable<T> iterable, GraphVisitor<T> visitor) {
        try {
            return executor.submit(new Task<>(iterable, visitor));
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    private static class Task<T extends VertexContext> implements Runnable {
        private final GraphIterable<T> iterable;
        private final GraphVisitor<T> visitor;

        public Task(GraphIterable<T> iterable, GraphVisitor<T> visitor) {
            this.iterable = iterable;
            this.visitor = visitor;
        }

        @Override
        public void run() {
            Objects.requireNonNull(iterable);
            for (Vertex<T> node : iterable) {
                node.accept(visitor);
            }
        }
    }
}
