package optimizer.experimental.utils;

import optimizer.experimental.context.OperatorContext;
import optimizer.experimental.context.OperatorImplementationContext;
import optimizer.experimental.context.RMOperatorImplementation;
import optimizer.experimental.context.VertexContext;
import optimizer.experimental.graph.DirectedAcyclicGraph;
import optimizer.experimental.graph.Vertex;
import optimizer.parser.workflow.Operator;

import java.util.Map;

public final class HashUtil {

    public static <T extends VertexContext> int hashPlan(DirectedAcyclicGraph<T> graph,
                                                         Map<OperatorContext, OperatorImplementationContext> implMap) {
        int result = 0;
        for (Vertex<T> operatorVertex : graph.getVertices()) {
            for (Vertex<T> succ : graph.getSuccessors(operatorVertex)) {
                result = 31 * result + succ.hashCode();
            }
            Operator curOp = (Operator) operatorVertex.getData();
            result = 31 * result + curOp.hashCode();
            RMOperatorImplementation curImpl = (RMOperatorImplementation) implMap.get(curOp);
            result = 31 * result + curImpl.hashCode();
        }
        return result;
    }

    public static <T extends VertexContext> String planSignature(DirectedAcyclicGraph<T> graph,
                                                                 Map<OperatorContext, OperatorImplementationContext> implMap) {
        return String.valueOf(hashPlan(graph, implMap));
    }

}
