package optimizer.web.controller;

import optimizer.parser.dictionary.DictionaryOperator;
import optimizer.parser.dictionary.INFOREDictionary;
import optimizer.parser.network.INFORENetwork;
import optimizer.parser.network.Site;
import optimizer.parser.workflow.INFOREWorkflow;
import optimizer.web.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

@RestController
@RequestMapping("/resource")
@ResponseBody
public class OptimizerResourcesController {

    @Autowired
    private DictionaryRepository dictionaryRepository;

    @Autowired
    private NetworkRepository networkRepository;

    @Autowired
    private WorkflowRepository workflowRepository;

    @Autowired
    private OptimizerRequestRepository optimizerRequestRepository;

    @Autowired
    private OptimizerResultRepository optimizerResultRepository;

    //PUT
    @PutMapping(value = "/network", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public String putNetwork(@Valid @RequestBody INFORENetwork inforeNetwork) {
        INFORENetwork previous = networkRepository.findById(inforeNetwork.getNetwork()).orElse(null);
        networkRepository.save(inforeNetwork);
        return previous == null
                ? "Created"
                : String.format("Updated (Previous submission was at %s)", previous.getModifiedAt());
    }

    @PutMapping(value = "/workflow", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public String putWorkflow(@Valid @RequestBody INFOREWorkflow inforeWorkflow) {
        INFOREWorkflow previous = workflowRepository.findById(inforeWorkflow.getWorkflowName()).orElse(null);
        workflowRepository.save(inforeWorkflow);
        return previous == null
                ? "Created"
                : String.format("Updated (Previous submission was at %s)", previous.getModifiedAt());
    }

    @PutMapping(value = "/dictionary", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public String putDictionary(@Valid @RequestBody INFOREDictionary inforeDictionary) {
        INFOREDictionary previous = dictionaryRepository.findById(inforeDictionary.getModifiedAt()).orElse(null);
        dictionaryRepository.save(inforeDictionary);
        return previous == null
                ? "Created"
                : String.format("Updated (Previous submission was at %s)", previous.getModifiedAt());
    }

    //Update individual items
    @PutMapping("/network/{network_name}")
    @ResponseStatus(HttpStatus.OK)
    public String addNetworkSite(@NotEmpty @PathVariable String network_name,
                                 @NotEmpty @RequestBody Site site) {
        INFORENetwork res = networkRepository
                .findById(network_name)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Network with ID={%s} not found.", network_name)));
        boolean didNotExist = res.addSite(site);
        networkRepository.save(res);
        return didNotExist ? "Added" : "Updated";
    }

    @PutMapping("/dictionary/{dictionary_name}")
    @ResponseStatus(HttpStatus.OK)
    public String addDictionaryOperator(@NotEmpty @PathVariable String dictionary_name,
                                        @NotEmpty @RequestBody DictionaryOperator dictionaryOperator) {
        INFOREDictionary res = dictionaryRepository
                .findById(dictionary_name)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Dictionary with ID={%s} not found.", dictionary_name)));
        boolean didNotExist = res.addOperator(dictionaryOperator);
        dictionaryRepository.save(res);
        return didNotExist ? "Added" : "Updated";
    }

    //DELETE
    @DeleteMapping("/network/{networkName}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteNetwork(@NotEmpty @PathVariable String networkName) {
        if (networkRepository.existsById(networkName)) {
            networkRepository.deleteById(networkName);
        } else {
            throw new ResourceNotFoundException(String.format("Network with networkName={%s} not found.", networkName));
        }
    }

    @DeleteMapping("/workflow/{workflowName}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public String deleteWorkflow(@NotEmpty @PathVariable String workflowName) {
        if (workflowRepository.existsById(workflowName)) {
            workflowRepository.deleteById(workflowName);
            return "Deleted";
        } else {
            throw new ResourceNotFoundException(String.format("Workflow with workflowName={%s} not found.", workflowName));
        }
    }

    @DeleteMapping("/dictionary/{dictionaryName}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteDictionary(@NotEmpty @PathVariable String dictionaryName) {
        if (dictionaryRepository.existsById(dictionaryName)) {
            dictionaryRepository.deleteById(dictionaryName);
        } else {
            throw new ResourceNotFoundException(String.format("Dictionary with dictionaryName={%s} not found.", dictionaryName));
        }
    }

    @DeleteMapping("/request/{requestID}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteRequest(@NotEmpty @PathVariable String requestID) {
        if (optimizerRequestRepository.existsById(requestID)) {
            optimizerRequestRepository.deleteById(requestID);
        } else {
            throw new ResourceNotFoundException(String.format("Optimizer request with requestID={%s} not found.", requestID));
        }
    }

    @DeleteMapping("/response/{responseID}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteResponse(@NotEmpty @PathVariable String responseID) {
        if (optimizerResultRepository.existsById(responseID)) {
            optimizerResultRepository.deleteById(responseID);
        } else {
            throw new ResourceNotFoundException(String.format("Optimizer Response with responseID={%s} not found.", responseID));
        }
    }

    //Update individual items
    @DeleteMapping("/network/{network_name}/{site_name}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delNetworkSite(@NotEmpty @PathVariable String network_name,
                               @NotEmpty @PathVariable String site_name) {
        INFORENetwork res = networkRepository
                .findById(network_name)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Network with ID={%s} not found.", network_name)));
        if (!res.deleteSite(site_name)) {
            throw new ResourceNotFoundException(String.format("Site with name={%s} on network={%s} not found.", site_name, network_name));
        }
        networkRepository.save(res);
    }

    @DeleteMapping("/network/{dictionary_name}/{class_key}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delDictionaryOperator(@NotEmpty @PathVariable String dictionary_name,
                                      @NotEmpty @PathVariable String class_key) {
        INFOREDictionary res = dictionaryRepository
                .findById(dictionary_name)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Dictionary with name={%s} not found.", dictionary_name)));
        if (!res.deleteOp(class_key)) {
            throw new ResourceNotFoundException(String.format("Class key with name={%s} on dictionary={%s} not found.", class_key, dictionary_name));
        }
        dictionaryRepository.save(res);
    }
}
