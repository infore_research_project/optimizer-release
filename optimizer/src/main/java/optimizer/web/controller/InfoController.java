package optimizer.web.controller;


import optimizer.parser.dictionary.INFOREDictionary;
import optimizer.parser.network.INFORENetwork;
import optimizer.parser.optimization.OptimizationRequest;
import optimizer.parser.optimization.OptimizerResponse;
import optimizer.parser.workflow.INFOREWorkflow;
import optimizer.utils.JSONSingleton;
import optimizer.web.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotEmpty;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/info")
@ResponseStatus(HttpStatus.OK)
public class InfoController {
    private final ZoneId zoneID = ZoneId.of("Europe/Athens");
    @Autowired
    private DictionaryRepository dictionaryRepository;
    @Autowired
    private NetworkRepository networkRepository;
    @Autowired
    private WorkflowRepository workflowRepository;
    @Autowired
    private OptimizerRequestRepository optimizerRequestRepository;
    @Autowired
    private OptimizerResultRepository optimizerResultRepository;

    @GetMapping("/echo/{value}")
    public String echo(@PathVariable String value) {
        return value;
    }

    @GetMapping("/logs")
    public ResponseEntity<InputStreamResource> getLogs() throws OptimizerException {
        File file = new File("/logs/optimizer.log");
        try {
            return ResponseEntity
                    .ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=optimizer.log")
                    .contentLength(file.length())
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .body(new InputStreamResource(new FileInputStream(file)));
        } catch (FileNotFoundException e) {
            throw new OptimizerException("Logs are currently unavailable");
        }
    }

    @GetMapping("/time")
    public String time() {
        return ZonedDateTime.ofInstant(Instant.now(), zoneID).toString();
    }

    //GET all items
    @GetMapping(value = "/networks", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getAllNetwork() {
        List<INFORENetwork> res = new ArrayList<>();
        networkRepository.findAll().forEach(res::add);
        return JSONSingleton.toJson(res);
    }

    @GetMapping(value = "/dictionaries", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getAllDictionaries() {
        List<INFOREDictionary> res = new ArrayList<>();
        dictionaryRepository.findAll().forEach(res::add);
        return JSONSingleton.toJson(res);
    }

    @GetMapping(value = "/workflows", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getAllWorkflows() {
        List<INFOREWorkflow> res = new ArrayList<>();
        workflowRepository.findAll().forEach(res::add);
        return JSONSingleton.toJson(res);
    }

    @GetMapping(value = "/requests", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getAllRequests() {
        List<OptimizationRequest> res = new ArrayList<>();
        optimizerRequestRepository.findAll().forEach(res::add);
        return JSONSingleton.toJson(res);
    }

    @GetMapping(value = "/results", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getAllResponses() {
        List<OptimizerResponse> res = new ArrayList<>();
        optimizerResultRepository.findAll().forEach(res::add);
        return JSONSingleton.toJson(res);
    }

    //GET with IDs
    @GetMapping(value = "/network/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getNetwork(@NotEmpty @PathVariable String id) {
        INFORENetwork res = networkRepository
                .findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Network with ID={%s} not found.", id)));
        return JSONSingleton.toJson(res);
    }

    @GetMapping(value = "/workflow/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getWorkflow(@NotEmpty @PathVariable String id) {
        INFOREWorkflow res = workflowRepository
                .findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Workflow with ID={%s} not found.", id)));
        return JSONSingleton.toJson(res);
    }

    @GetMapping(value = "/dictionary/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getDictionary(@NotEmpty @PathVariable String id) {
        INFOREDictionary res = dictionaryRepository
                .findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Dictionary with ID={%s} not found.", id)));
        return JSONSingleton.toJson(res);
    }

    @GetMapping(value = "/request/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getRequest(@NotEmpty @PathVariable String id) {
        OptimizationRequest res = optimizerRequestRepository
                .findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Optimizer Request with ID={%s} not found.", id)));
        return JSONSingleton.toJson(res);
    }

    @GetMapping(value = "/response/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getResponse(@NotEmpty @PathVariable String id) {
        OptimizerResponse res = optimizerResultRepository
                .findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format("Optimizer Response with ID={%s} not found.", id)));
        return JSONSingleton.toJson(res);
    }
}
