package optimizer.web.controller;

import optimizer.parser.optimization.OptimizationRequest;
import optimizer.parser.optimization.OptimizerResponse;
import optimizer.utils.JSONSingleton;
import optimizer.web.repository.OptimizerRequestRepository;
import optimizer.web.repository.OptimizerResultRepository;
import optimizer.web.service.OptimizerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.concurrent.TimeUnit;

import static optimizer.OptimizerApplication.masterLogger;

@RestController
@RequestMapping("/request")
public class OptimizerRequestController {

    @Value("${spring.application.request_timeout_ms}")
    private int timeout;

    @Autowired
    private OptimizerService optimizerService;

    @Autowired
    private OptimizerRequestRepository optimizerRequestRepository;

    @Autowired
    private OptimizerResultRepository optimizerResultRepository;

    /**
     * Calls the optimizer service and requests an optimization process be performed based on a
     * set of rules and parameters.
     *
     * @param optimizationRequest An optimization request details.
     * @return The optimization result.
     */
    @PostMapping(value = "/optimize", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public String processOptimizationRequest(@Valid @RequestBody OptimizationRequest optimizationRequest) throws OptimizerException {
        optimizerRequestRepository.save(optimizationRequest);
        try {
            OptimizerResponse output = optimizerService.optimize(optimizationRequest).get(timeout, TimeUnit.MILLISECONDS);
            optimizerResultRepository.save(output);
            String responseJSON = JSONSingleton.toJson(output);
            masterLogger.info("Response ID: " + output.getId());
            masterLogger.debug("Response: " + responseJSON);
            return responseJSON;
        } catch (Exception e) {
            throw new OptimizerException(e);
        }
    }
}
