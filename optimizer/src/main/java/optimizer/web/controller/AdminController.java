package optimizer.web.controller;

import optimizer.utils.JSONSingleton;
import optimizer.web.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static optimizer.OptimizerApplication.masterLogger;

@RestController
@RequestMapping("/admin")
@ResponseStatus(HttpStatus.OK)
@ResponseBody
public class AdminController {

    @Autowired
    private DictionaryRepository dictionaryRepository;

    @Autowired
    private NetworkRepository networkRepository;

    @Autowired
    private WorkflowRepository workflowRepository;

    @Autowired
    private OptimizerRequestRepository optimizerRequestRepository;

    @Autowired
    private OptimizerResultRepository optimizerResultRepository;


    //TEST methods
    @GetMapping("/test")
    public String echo() {
        masterLogger.debug("/test {}", () -> "test");
        return "test";
    }


    //DELETE ALL
    @GetMapping("/drop_networks")
    public String deleteAllNetwork() {
        networkRepository.deleteAll();
        return "All networks were deleted.";
    }

    @GetMapping("/drop_dictionaries")
    public String deleteAllDictionaries() {
        dictionaryRepository.deleteAll();
        return "All dictionaries were deleted.";
    }

    @GetMapping("/drop_workflows")
    public String deleteAllWorkflows() {
        workflowRepository.deleteAll();
        return "All workflows were deleted.";
    }

    @GetMapping("/drop_requests")
    public String deleteAllRequests() {
        optimizerRequestRepository.deleteAll();
        return "All optimizer requests were deleted.";
    }

    @GetMapping("/drop_results")
    public String deleteAllResults() {
        optimizerResultRepository.deleteAll();
        return "All optimizer results were deleted.";
    }

    @GetMapping(value = "/drop_all", produces = MediaType.APPLICATION_JSON_VALUE)
    public String dropAll() {
        masterLogger.warn("A /drop_all request has been issued.");
        List<String> list = new ArrayList<>();
        list.add(deleteAllNetwork());
        list.add(deleteAllDictionaries());
        list.add(deleteAllWorkflows());
        list.add(deleteAllRequests());
        list.add(deleteAllResults());
        return JSONSingleton.toJson(list);
    }
}
