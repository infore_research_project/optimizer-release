package optimizer.web;

import optimizer.graph.implementation.OperatorImplementationGraph;
import optimizer.graph.operator.OperatorGraph;
import optimizer.parser.dictionary.INFOREDictionary;
import optimizer.parser.network.INFORENetwork;
import optimizer.parser.optimization.OptimizationRequest;
import optimizer.parser.workflow.INFOREWorkflow;
import optimizer.plan.AbstractOptimizerPlan;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicInteger;

public class OptimizationResourcesBundle {
    //Original request
    private OptimizationRequest request;

    //Resources
    private INFOREDictionary dictionary;
    private INFORENetwork network;
    private INFOREWorkflow workflow;

    //Algorithm used to traverser.traverse and manipulate the graph
    private String traversalAlgorithmName;

    //Optional stats, mostly for debugging
    private OptimizationRequestStatisticsBundle statisticsBundle;

    //The actual optimizer graph
    private OperatorImplementationGraph implementationGraph;

    //Plans produces by the optimizer
    private Queue<AbstractOptimizerPlan> planQueue;
    private AtomicInteger numOfPlans;

    //Multi-thread options
    private int threads;

    //misc
    private String version;
    private int timeout;

    //Private constructor, for internal instantiation only
    private OptimizationResourcesBundle() {
    }

    //Stats
    public int incrCreatedPlans() {
        return this.statisticsBundle.incrementCreatedPlans();
    }

    public int incrExploredPlans() {
        return this.statisticsBundle.incrementExploredPlans();
    }

    public int incrExploredDimensions() {
        return this.statisticsBundle.incrementExploredDimensions();
    }

    public int addStat(String k, int v) {
        return this.statisticsBundle.addStat(k, v);
    }


    //Getters and Setters
    public INFOREDictionary getDictionary() {
        return dictionary;
    }

    public void setDictionary(INFOREDictionary dictionary) {
        this.dictionary = dictionary;
    }

    public INFORENetwork getNetwork() {
        return network;
    }

    public void setNetwork(INFORENetwork network) {
        this.network = network;
    }

    public INFOREWorkflow getWorkflow() {
        return workflow;
    }

    public void setWorkflow(INFOREWorkflow workflow) {
        this.workflow = workflow;
    }

    public String getTraversalAlgorithmName() {
        return traversalAlgorithmName;
    }

    public void setTraversalAlgorithmName(String traversalAlgorithmName) {
        this.traversalAlgorithmName = traversalAlgorithmName.toLowerCase().trim();
    }

    public OptimizationRequestStatisticsBundle getStatisticsBundle() {
        return statisticsBundle;
    }

    public void setStatisticsBundle(OptimizationRequestStatisticsBundle statisticsBundle) {
        this.statisticsBundle = statisticsBundle;
    }

    public OptimizationRequest getRequest() {
        return request;
    }

    public void setRequest(OptimizationRequest request) {
        this.request = request;
    }

    public OperatorImplementationGraph getImplementationGraph() {
        return implementationGraph;
    }

    public void setImplementationGraph(OperatorImplementationGraph implementationGraph) {
        this.implementationGraph = implementationGraph;
    }

    public Queue<AbstractOptimizerPlan> getPlanQueue() {
        return planQueue;
    }

    public void setPlanQueue(Queue<AbstractOptimizerPlan> planQueue) {
        this.planQueue = planQueue;
    }

    public AbstractOptimizerPlan getBestPlan() {
        return this.planQueue.peek();
    }

    public AtomicInteger getNumOfPlans() {
        return numOfPlans;
    }

    public void setNumOfPlans(AtomicInteger numOfPlans) {
        this.numOfPlans = numOfPlans;
    }

    public int getThreads() {
        return threads;
    }

    private void setThreads(int threads) {
        this.threads = threads;
    }

    public String getVersion() {
        return version;
    }

    private void setVersion(String version) {
        this.version = version;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public String toSmallString() {
        return "OptimizationResourcesBundle{" +
                "request=" + request.getId() +
                ", dictionary=" + dictionary.getDictionaryName() +
                ", network=" + network.getNetwork() +
                ", workflowName=" + workflow.getWorkflowName() +
                ", traversalAlgorithmName='" + traversalAlgorithmName + '\'' +
                ", statisticsBundle=" + statisticsBundle +
                '}';
    }
    //Returns true if the number of requested plans is met.

    public void offerPlans(Collection<? extends AbstractOptimizerPlan> plans) {
        if (plans == null) {
            return;
        }
        Iterator<? extends AbstractOptimizerPlan> planIterator = plans.iterator();
        while (planIterator.hasNext() && this.numOfPlans.getAndDecrement() > 0) {
            this.planQueue.add(planIterator.next());
        }
        this.numOfPlans.get();
    }

    public void incrExploredPlans(int numOfVisitedPlans) {
        this.statisticsBundle.incrementExploredPlans(numOfVisitedPlans);
    }

    //Builder pattern to ease the object instantiation
    public static class Builder {
        private final OptimizationResourcesBundle bundle;

        public Builder(OptimizationRequest request) {
            this.bundle = new OptimizationResourcesBundle();
            this.bundle.setRequest(request);
            this.bundle.setPlanQueue(new ArrayDeque<>());
            this.bundle.setNumOfPlans(new AtomicInteger(request.getNumOfPlans()));
        }

        public Builder withDictionary(INFOREDictionary dictionary) {
            this.bundle.setDictionary(dictionary);
            return this;
        }

        public Builder withNetwork(INFORENetwork network) {
            this.bundle.setNetwork(network);
            return this;
        }

        public Builder withWorkflow(INFOREWorkflow workflow) {
            this.bundle.setWorkflow(workflow);
            return this;
        }

        public Builder withAlgorithm(String algorithm) {
            this.bundle.setTraversalAlgorithmName(algorithm);
            return this;
        }

        public Builder withThreads(int threads) {
            this.bundle.setThreads(threads);
            return this;
        }

        public Builder withVersion(String version) {
            this.bundle.setVersion(version);
            return this;
        }

        public Builder withTimeout(int timeout) {
            this.bundle.setTimeout(timeout);
            return this;
        }

        public OptimizationResourcesBundle get() {
            //Instantiate remaining fields
            this.bundle.setStatisticsBundle(new OptimizationRequestStatisticsBundle());
            this.bundle.setImplementationGraph(new OperatorImplementationGraph(new OperatorGraph(this.bundle.getWorkflow())));

            //Return the built object
            return this.bundle;
        }
    }
}
