package optimizer.web;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class OptimizationRequestStatisticsBundle {
    private final Map<String, AtomicInteger> stats;

    private final String CREATED_PLANS = "Created Plans";
    private final String EXPLORED_PLANS = "Explored Plans";
    private final String EXPLORED_DIMS = "Explored Dimensions";

    public OptimizationRequestStatisticsBundle() {
        this.stats = new HashMap<>();
        this.stats.put(CREATED_PLANS, new AtomicInteger(0));
        this.stats.put(EXPLORED_PLANS, new AtomicInteger(0));
        this.stats.put(EXPLORED_DIMS, new AtomicInteger(0));
    }

    public int addStat(String k, int v) {
        if (!this.stats.containsKey(k)) {
            this.stats.put(k, new AtomicInteger(0));
        }
        return this.stats.get(k).addAndGet(v);
    }

    public int incrementCreatedPlans() {
        return this.stats.get(CREATED_PLANS).incrementAndGet();
    }

    public int incrementExploredPlans() {
        return this.stats.get(EXPLORED_PLANS).incrementAndGet();
    }

    public int incrementExploredPlans(int numOfVisitedPlans) {
        return this.stats.get(EXPLORED_PLANS).addAndGet(numOfVisitedPlans);
    }

    public int incrementExploredDimensions() {
        return this.stats.get(EXPLORED_DIMS).incrementAndGet();
    }

    public Map<String, String> toStringMap() {
        return this.stats.entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, kv -> String.valueOf(kv.getValue())));
    }

    @Override
    public String toString() {
        return "OptimizationRequestStatisticsBundle{" +
                "stats=" + stats +
                ", CREATED_PLANS='" + CREATED_PLANS + '\'' +
                ", EXPLORED_PLANS='" + EXPLORED_PLANS + '\'' +
                ", EXPLORED_DIMS='" + EXPLORED_DIMS + '\'' +
                '}';
    }

    public Map<String, AtomicInteger> getStats() {
        return stats;
    }
}
