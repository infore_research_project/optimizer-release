package optimizer.web.repository;

import optimizer.parser.dictionary.INFOREDictionary;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface DictionaryRepository extends ElasticsearchRepository<INFOREDictionary, String> {
}
