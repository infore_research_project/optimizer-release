package optimizer.web.repository;

import optimizer.parser.optimization.OptimizationRequest;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface OptimizerRequestRepository extends ElasticsearchRepository<OptimizationRequest, String> {
}
