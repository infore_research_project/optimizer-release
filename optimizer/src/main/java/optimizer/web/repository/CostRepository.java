package optimizer.web.repository;

import optimizer.web.entity.CostEntity;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface CostRepository extends ElasticsearchRepository<CostEntity, String> {
}
