package optimizer.web.repository;

import optimizer.parser.optimization.OptimizerResponse;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface OptimizerResultRepository extends ElasticsearchRepository<OptimizerResponse, String> {
}
