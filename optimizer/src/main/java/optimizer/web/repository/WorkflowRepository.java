package optimizer.web.repository;

import optimizer.parser.workflow.INFOREWorkflow;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface WorkflowRepository extends ElasticsearchRepository<INFOREWorkflow, String> {
}
