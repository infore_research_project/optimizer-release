package optimizer.web.repository;

import optimizer.parser.network.INFORENetwork;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface NetworkRepository extends ElasticsearchRepository<INFORENetwork, String> {
}
