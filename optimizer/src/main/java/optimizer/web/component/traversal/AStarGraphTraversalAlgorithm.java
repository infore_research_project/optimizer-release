package optimizer.web.component.traversal;

import optimizer.graph.implementation.OperatorImplementationGraph;
import optimizer.graph.implementation.OperatorImplementationVertex;
import optimizer.graph.iterable.ReverseDAGIterable;
import optimizer.graph.operator.OperatorEdge;
import optimizer.graph.operator.OperatorGraph;
import optimizer.graph.operator.OperatorVertex;
import optimizer.model.OperatorImplementation;
import optimizer.model.OperatorVertexCostModel;
import optimizer.model.OptimizationCost;
import optimizer.plan.AStarPlan;
import optimizer.utils.Tuple;
import optimizer.web.OptimizationResourcesBundle;
import optimizer.web.controller.OptimizerException;
import optimizer.web.service.CostEstimatorService;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class AStarGraphTraversalAlgorithm extends AbstractGraphTraversalAlgorithm {

    public AStarGraphTraversalAlgorithm(CostEstimatorService costEstimator) {
        super(costEstimator, Arrays.asList("A*", "AStar", "a*", "astar", "A Star", "A *"));
    }

    @Override
    public void explorePaths(OptimizationResourcesBundle bundle) throws OptimizerException {
        //Create a graph ref
        OperatorImplementationGraph opImplGraph = bundle.getImplementationGraph();

        //The graph of operator vertices
        OperatorGraph operatorGraph = opImplGraph.getOperatorGraph();

        //Number of plans this algorithm needs to produce
        int requestedPlans = bundle.getRequest().getNumOfPlans();

        //Return if the graph is empty
        if (operatorGraph.getEffectiveOperators().isEmpty()) {
            throw new OptimizerException("Empty graph provided.");
        }

        //Add missing operators to dictionary
        bundle.getDictionary().fillOperators(operatorGraph);

        //Assign initial heuristic values to graph operator implementations
        assignStartingCosts(operatorGraph);

        //Optimizer solution min-heap using the heuristic value of each operator as a comparator
        PriorityQueue<AStarPlan> solutionPQ = new PriorityQueue<>();
        PriorityQueue<AStarPlan> outputPQ = new PriorityQueue<>();
        solutionPQ.add(new AStarPlan(opImplGraph));
        bundle.incrCreatedPlans();

        //Process all graph vertices
        while (!solutionPQ.isEmpty() && outputPQ.size() < requestedPlans) {
            AStarPlan polledSOL = solutionPQ.remove();
            if (polledSOL.isComplete()) {
                outputPQ.add(polledSOL);
                continue;
            }

            //Popped plans are counted as explored
            bundle.incrExploredPlans();

            //Get a new operator and its available implementations
            Tuple<OperatorVertex, Set<OperatorImplementationVertex>> newImplementationsTpl = selectOpToAdd(polledSOL, bundle);
            OperatorVertex selectedOperator = newImplementationsTpl._1;
            Set<OperatorImplementationVertex> newImplementations = newImplementationsTpl._2;

            //START_OR/END_OR operators don't have implementations
            if (newImplementations.isEmpty()) {
                OperatorImplementationVertex newImplVtx = new OperatorImplementationVertex(selectedOperator, new OperatorImplementation());
                AStarPlan newSol = new AStarPlan(polledSOL, newImplVtx, getCostEstimator());
                solutionPQ.add(newSol);
                bundle.incrCreatedPlans();
            } else {
                //The new solution that includes the newOp with a new implementation and update ending nodes
                for (OperatorImplementationVertex vertex : newImplementations) {
                    AStarPlan newPlan = new AStarPlan(polledSOL, vertex, getCostEstimator());
                    solutionPQ.add(newPlan);
                    bundle.incrCreatedPlans();
                }
            }
        }

        bundle.offerPlans(outputPQ);
    }

    /**
     * Επιλέγει έναν operator newOP που ΔΕΝ είναι ήδη στη SOL,
     * αλλά έχει όλους τους incoming operators στη SOL.
     * Αυτό μπορεί να στο δώσει και το topological sort έτοιμο.
     * Αν υπάρχουν πολλοί, επέλεξε εναν τυχαία.
     *
     * @param sol    The current solution.
     * @param bundle The graph of optimizer resources.
     * @return A Set of all the operator implementations.
     */
    private Tuple<OperatorVertex, Set<OperatorImplementationVertex>> selectOpToAdd(AStarPlan sol, OptimizationResourcesBundle bundle) {
        OperatorGraph operatorsGraph = bundle.getImplementationGraph().getOperatorGraph();
        OperatorImplementationGraph opImplGraph = bundle.getImplementationGraph();
        for (OperatorVertex endingNode : sol.getEndingNodes()) {
            for (OperatorEdge edge : operatorsGraph.getGraph().outgoingEdgesOf(endingNode)) {
                //Check if this operator can be selected
                OperatorVertex tgtVtx = edge.getTo();
                if (!sol.getImplMap().containsKey(tgtVtx) && sol.getImplMap().keySet().containsAll(operatorsGraph.getPredecessors(tgtVtx))) {
                    //This operator is now selected.
                    //Check if we need to populate the operator implementation tree
                    //with implementations of this selected operator or just retrieve the already present nodes.
                    if (opImplGraph.getOperatorMap().containsKey(tgtVtx)) {
                        return new Tuple<>(tgtVtx, opImplGraph.getOperatorMap().get(tgtVtx));
                    }

                    //Operator predecessors of the expanded operator
                    Set<OperatorVertex> tgtVtxPreds = new HashSet<>();
                    for (OperatorEdge incEdge : operatorsGraph.getGraph().incomingEdgesOf(tgtVtx)) {
                        tgtVtxPreds.add(incEdge.getFrom());
                    }

                    //For each combination of cost dimensions add a vertex and save them in a set
                    Set<OperatorImplementationVertex> newImplementations = new HashSet<>();
                    for (List<OptimizationCost> costDims : bundle.getDictionary().getCostDims(tgtVtx.getOperator(), bundle.getNetwork())) {
                        OperatorImplementation newOpImpl = createNewOperatorImplCostModel(opImplGraph, costDims);
                        OperatorImplementationVertex newOpImplVtx = new OperatorImplementationVertex(tgtVtx, newOpImpl);
                        newImplementations.add(newOpImplVtx);
                        opImplGraph.addVertex(newOpImplVtx);

                        for (OperatorVertex predecessor : tgtVtxPreds) {
                            for (OperatorImplementationVertex implPred : opImplGraph.getOperatorMap().get(predecessor)) {
                                if (!opImplGraph.containsVertex(implPred)) {
                                    opImplGraph.addVertex(implPred);
                                }
                                opImplGraph.addEdge(implPred, newOpImplVtx);
                            }
                        }

                        //Increment performance counters
                        bundle.incrExploredDimensions();
                    }

                    //New operator implementation with the min cost
                    int minNewImplCost = newImplementations.stream()
                            .mapToInt(OperatorImplementationVertex::cost)
                            .min()
                            .orElse(0);

                    //Increment the heuristic cost of the operator (NOT THE IMPLEMENTATION)
                    tgtVtx.getOperatorVertexCostModel().incHeuristicCost(minNewImplCost);

                    LOGGER.info(String.format("Added new operator [%s] with implementations %s and heuristic cost %d",
                            tgtVtx.getOperator().getName(), newImplementations, minNewImplCost));

                    return new Tuple<>(tgtVtx, newImplementations);
                }
            }
        }
        return new Tuple<>(null, Collections.emptySet());
    }

    /**
     * Assign initial heuristic costs to all operators (not implementations).
     *
     * @param graph The input OptimizerGraph.
     */
    private void assignStartingCosts(OperatorGraph graph) {
        //Perform a reverse topological sort graph traversal and calculate the costs per operator (NOT OP IMPL)
        for (OperatorVertex vertex : new ReverseDAGIterable(graph)) {
            vertex.setOperatorVertexCostModel(new OperatorVertexCostModel(0, 0));
            LOGGER.info("Assigning starting cost to vertex " + vertex.getOperator().getName() + " " + vertex.getOperatorVertexCostModel());
        }
    }
}
