package optimizer.web.component.traversal;

import optimizer.experimental.action.RMPlanActions;
import optimizer.experimental.context.OperatorContext;
import optimizer.experimental.exception.GraphException;
import optimizer.experimental.graph.Edge;
import optimizer.experimental.graph.ThreadSafeDAG;
import optimizer.experimental.graph.Vertex;
import optimizer.experimental.plan.RMOptimizationPlan;
import optimizer.experimental.transformer.RMPlanGenerator;
import optimizer.experimental.visitor.MinCostPlanVisitor;
import optimizer.experimental.worker.WorkerGlobalContext;
import optimizer.experimental.worker.WorkerPool;
import optimizer.experimental.worker.WorkerRunnable;
import optimizer.experimental.worker.WorkerTask;
import optimizer.parser.dictionary.INFOREDictionary;
import optimizer.parser.network.INFORENetwork;
import optimizer.parser.workflow.INFOREWorkflow;
import optimizer.parser.workflow.Operator;
import optimizer.plan.MultiThreadExhaustivePlan;
import optimizer.web.OptimizationResourcesBundle;
import optimizer.web.controller.OptimizerException;
import optimizer.web.service.CostEstimatorService;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.locks.Lock;
import java.util.stream.Collectors;

@SuppressWarnings("ArraysAsListWithZeroOrOneArgument")
@Component
public class IncrementalGraphTraversalAlgorithm extends AbstractGraphTraversalAlgorithm {

    public IncrementalGraphTraversalAlgorithm(CostEstimatorService costEstimator) {
        super(costEstimator, Arrays.asList("exhaustive_beta"));
    }

    @Override
    public void explorePaths(OptimizationResourcesBundle bundle) throws OptimizerException {
        //Resources
        final INFOREWorkflow inforeWorkflow = bundle.getWorkflow();
        final INFORENetwork inforeNetwork = bundle.getNetwork();
        final INFOREDictionary inforeDictionary = bundle.getDictionary();
        int workers = bundle.getThreads();
        int timeoutMS = bundle.getTimeout();

        //Signatures of visited plans
        final Set<String> visitedSignatures = ConcurrentHashMap.newKeySet();

        //True values imply that the signature is being processed by another thread
        final Map<String, Lock> pendingSignatures = new ConcurrentHashMap<>();

        //Global resources
        final WorkerPool workerPool = new WorkerPool(workers);
        final WorkerGlobalContext wgc = new WorkerGlobalContext();

        //Translate graph
        ThreadSafeDAG<RMOptimizationPlan> planGraph;
        try {
            planGraph = translateToDAG(inforeWorkflow, inforeNetwork);
        } catch (GraphException e) {
            throw new OptimizerException(e);
        }
        LOGGER.info("Operator graph size: " + inforeWorkflow.getOperators().size());


        //Populate the shared context (shared among all threads)
        wgc.put("planGraph", planGraph);
        final ConcurrentLinkedQueue<Vertex<RMOptimizationPlan>> globalQueue = new ConcurrentLinkedQueue<>();
        globalQueue.add(planGraph.getRoot());
        wgc.put("planQueue", globalQueue);
        final RMPlanActions planActions = new RMPlanActions(inforeDictionary, inforeNetwork, visitedSignatures);
        final RMPlanGenerator planGenerator = new RMPlanGenerator(planActions, visitedSignatures, pendingSignatures);
        wgc.put("planGenerator", planGenerator);

        //Spawn the workers and wait for the results
        for (int i = 0; i < workers; i++) {
            WorkerTask workerTask = new WorkerTask(i, wgc);
            workerPool.submit(workerTask);
        }
        LOGGER.info("Finished enqueuing all tasks.");

        //Block until everything is done or a timeout has been reached
        Instant start = Instant.now();
        workerPool.shutdown(timeoutMS);
        Duration workersDuration = Duration.between(start, Instant.now());
        LOGGER.info("Worker pool shutdown after " + workersDuration.toMillis() + " ms");

        //Worker results
        LOGGER.info("**Worker Results**");
        workerPool.getQueuedRunnables().forEach(wr -> LOGGER.info(wr.toString()));

        //Stats
        int totalCollisions = workerPool.getQueuedRunnables().stream().mapToInt(WorkerRunnable::getCollisions).sum();
        int totalPlans = workerPool.getQueuedRunnables().stream().mapToInt(WorkerRunnable::getExploredPlans).sum(); //Driver produced the 1st plan
        LOGGER.info("**Stats**");
        LOGGER.info("Total collisions: " + totalCollisions);
        LOGGER.info("Total plans explored: " + totalPlans);
        LOGGER.info("Thread Avg. Plans/sec: " + totalPlans / workersDuration.getSeconds());
        LOGGER.info(String.format("Ratio of plans to collisions: %04f", totalPlans / (double) totalCollisions));

        //After the graph is explored spawn a visitor and find the best plan (with the min cost)
        MinCostPlanVisitor planVisitor = new MinCostPlanVisitor();
        planGraph.getVertices().forEach(planVisitor::visit);
        RMOptimizationPlan bestPlan = planVisitor.getBestPlan();
        Map<OperatorContext, Map<String, Integer>> costMap = planVisitor.getCostMap(bestPlan);
        LOGGER.info("Visited " + planVisitor.getNumOfVisitedPlans() + ", best plan: " + bestPlan.toString());

        //Save the best plan and some stats
        bundle.addStat("Total collisions", totalCollisions);
        bundle.addStat("Thread Avg. Plans/sec", (int) (totalPlans / workersDuration.getSeconds()));
        bundle.incrExploredPlans(planVisitor.getNumOfVisitedPlans());
        bundle.incrCreatedPlans();
        bundle.offerPlans(Collections.singletonList(new MultiThreadExhaustivePlan(bestPlan, costMap)));
    }

    /**
     * Migrates the existing graph to the new graph format.
     * TODO migrate all algorithm graphs to the new one
     */
    private ThreadSafeDAG<RMOptimizationPlan> translateToDAG(INFOREWorkflow inforeWorkflow, INFORENetwork network) throws GraphException {
        Map<String, Vertex<Operator>> operatorMap = new HashMap<>();
        inforeWorkflow.getOperators().forEach(op -> operatorMap.put(op.getName(), new Vertex<>(op)));

        List<Edge<Operator>> edges = inforeWorkflow.getOperatorConnections().stream()
                .map(conn -> new Edge<>(new Vertex<>(operatorMap.get(conn.getFromOperator())), new Vertex<>(operatorMap.get(conn.getToOperator()))))
                .collect(Collectors.toList());

        ThreadSafeDAG<Operator> operatorGraph = new ThreadSafeDAG<>(operatorMap.values(), edges);

        ThreadSafeDAG<RMOptimizationPlan> planGraph = new ThreadSafeDAG<>();
        planGraph.addVertex(new Vertex<>(new RMOptimizationPlan(operatorGraph, network)));
        return planGraph;
    }
}
