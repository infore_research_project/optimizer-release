package optimizer.web.component.traversal;

import optimizer.graph.implementation.OperatorImplementationGraph;
import optimizer.graph.implementation.OperatorImplementationVertex;
import optimizer.graph.iterable.DAGIterable;
import optimizer.graph.operator.OperatorGraph;
import optimizer.graph.operator.OperatorVertex;
import optimizer.model.OperatorImplementation;
import optimizer.model.OperatorVertexCostModel;
import optimizer.model.OptimizationCost;
import optimizer.plan.GreedyPlan;
import optimizer.web.OptimizationResourcesBundle;
import optimizer.web.controller.OptimizerException;
import optimizer.web.service.CostEstimatorService;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class GreedyGraphTraversalAlgorithm extends AbstractGraphTraversalAlgorithm {

    public GreedyGraphTraversalAlgorithm(CostEstimatorService costEstimator) {
        super(costEstimator, Arrays.asList("greedy", "GreedyAlgorithm"));
    }

    @Override
    public void explorePaths(OptimizationResourcesBundle bundle) throws OptimizerException {
        //Ref the graph
        OperatorImplementationGraph implementationGraph = bundle.getImplementationGraph();

        //The graph of operator vertices
        OperatorGraph operatorGraph = implementationGraph.getOperatorGraph();

        //Return if the graph is empty
        if (operatorGraph.getEffectiveOperators().isEmpty()) {
            throw new OptimizerException("Empty graph provided.");
        }

        //Add missing operators to dictionary
        bundle.getDictionary().fillOperators(operatorGraph);

        //Perform a reverse topological sort graph traversal and calculate the costs per operator (NOT OP IMPL)
        for (OperatorVertex vertex : operatorGraph.getGraph().vertexSet()) {
            int opCost = 0;
            int opHeuristicCost = 0;
            vertex.setOperatorVertexCostModel(new OperatorVertexCostModel(opCost, opHeuristicCost));
            LOGGER.info("Assigning starting cost to vertex " + vertex.getOperator().getName() + " " + vertex.getOperatorVertexCostModel());
        }

        //Select the best plan
        PriorityQueue<GreedyPlan> outputPQ = new PriorityQueue<>();

        //Map of all visited operators and their chosen implementations
        Map<OperatorVertex, OperatorImplementationVertex> implMap = new HashMap<>();
        implMap.put(implementationGraph.getRootVertex().getOpVertex(), implementationGraph.getRootVertex());

        //Costs of operator vertices
        Map<OperatorVertex, Integer> costMap = new HashMap<>();
        costMap.put(implementationGraph.getRootVertex().getOpVertex(), implementationGraph.getRootVertex().cost());

        //Traverse the graph in topological order
        for (OperatorVertex vertex : new DAGIterable(operatorGraph)) {

            //Cost of predecessors
            int predsCost = operatorGraph.getGraph().incomingEdgesOf(vertex).stream()
                    .mapToInt(value -> costMap.get(value.getFrom()))
                    .max()
                    .orElse(0);

            //Enumerate implementations of this vertex. Contains tuples of site-platform
            int minImplCostSoFar = Integer.MAX_VALUE;
            OperatorImplementationVertex bestVertexSoFar = null;
            for (List<OptimizationCost> costDims : bundle.getDictionary().getCostDims(vertex.getOperator(), bundle.getNetwork())) {
                OperatorImplementation newOpImpl = createNewOperatorImplCostModel(implementationGraph, costDims);
                OperatorImplementationVertex newOpImplVtx = new OperatorImplementationVertex(vertex, newOpImpl);
                implementationGraph.addVertex(newOpImplVtx);
                bundle.incrExploredDimensions();
                if (minImplCostSoFar > newOpImplVtx.cost()) {
                    minImplCostSoFar = newOpImplVtx.cost();
                    bestVertexSoFar = newOpImplVtx;
                }
            }
            if (bestVertexSoFar == null) {
                OperatorImplementation newOpImpl = new OperatorImplementation();
                OperatorImplementationVertex newVtx = new OperatorImplementationVertex(vertex, newOpImpl);
                minImplCostSoFar = 0;
                bestVertexSoFar = newVtx;
            }

            // Get the implementation with the min cost and save them
            implMap.put(bestVertexSoFar.getOpVertex(), bestVertexSoFar);
            costMap.put(bestVertexSoFar.getOpVertex(), predsCost + minImplCostSoFar);
        }

        //Verify that we have found a solution
        GreedyPlan plan = new GreedyPlan(implMap, costMap);
        bundle.incrCreatedPlans();
        if (!plan.isComplete()) {
            throw new OptimizerException("Failed to find a valid plan for the input workflow.");
        }
        outputPQ.add(plan);
        bundle.offerPlans(outputPQ);
    }
}
