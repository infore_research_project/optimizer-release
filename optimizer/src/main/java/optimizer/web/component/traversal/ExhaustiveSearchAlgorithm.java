package optimizer.web.component.traversal;

import optimizer.graph.implementation.OperatorImplementationGraph;
import optimizer.graph.implementation.OperatorImplementationVertex;
import optimizer.graph.iterable.CartesianIterable;
import optimizer.graph.operator.OperatorGraph;
import optimizer.graph.operator.OperatorVertex;
import optimizer.model.OperatorImplementation;
import optimizer.model.OperatorVertexCostModel;
import optimizer.model.OptimizationCost;
import optimizer.plan.ExhaustivePlan;
import optimizer.web.OptimizationResourcesBundle;
import optimizer.web.controller.OptimizerException;
import optimizer.web.service.CostEstimatorService;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Traverses the operator graph and selects the best {@link OperatorImplementation} for each operator.
 * More specifically, each operator implementation is selected by recursively (re)computing all available
 * paths in the {@link OperatorImplementationGraph}.
 */
@Component
public class ExhaustiveSearchAlgorithm extends AbstractGraphTraversalAlgorithm {

    public ExhaustiveSearchAlgorithm(CostEstimatorService costEstimator) {
        super(costEstimator, Arrays.asList("exhaustive", "ExhaustiveAlgorithm"));
    }

    @Override
    public void explorePaths(OptimizationResourcesBundle bundle) throws OptimizerException {
        OperatorImplementationGraph implementationGraph = bundle.getImplementationGraph();

        //The graph of operator vertices
        OperatorGraph operatorGraph = implementationGraph.getOperatorGraph();

        //Add missing operators to dictionary
        bundle.getDictionary().fillOperators(operatorGraph);

        //Return if the graph is empty
        if (operatorGraph.getEffectiveOperators().isEmpty()) {
            throw new OptimizerException("Empty graph provided.");
        }

        //Perform a reverse topological sort graph traversal and calculate the costs per operator (NOT OP IMPL)
        for (OperatorVertex vertex : operatorGraph.getGraph().vertexSet()) {
            int opCost = 0;
            int opHeuristicCost = 0;
            vertex.setOperatorVertexCostModel(new OperatorVertexCostModel(opCost, opHeuristicCost));
            LOGGER.info("Assigning starting cost to vertex " + vertex.getOperator().getName() + " " + vertex.getOperatorVertexCostModel());
        }

        //1st dimensions is operator, 2nd dimensions is operator implementation
        List<List<OperatorImplementationVertex>> costDimensionsPerVertex = new ArrayList<>();
        for (OperatorVertex vertex : operatorGraph.getGraph().vertexSet()) {
            List<OperatorImplementationVertex> vtxImplementations = new ArrayList<>();
            for (List<OptimizationCost> costDims : bundle.getDictionary().getCostDims(vertex.getOperator(), bundle.getNetwork())) {
                bundle.incrExploredDimensions();
                OperatorImplementation newOpImpl = createNewOperatorImplCostModel(implementationGraph, costDims);
                OperatorImplementationVertex newOpImplVtx = new OperatorImplementationVertex(vertex, newOpImpl);
                vtxImplementations.add(newOpImplVtx);
            }
            if (vtxImplementations.isEmpty()) {
                OperatorImplementation newOpImpl = new OperatorImplementation();
                vtxImplementations.add(new OperatorImplementationVertex(vertex, newOpImpl));
            }
            costDimensionsPerVertex.add(vtxImplementations);
        }

        //Iterate through all plans and select the best
        ExhaustivePlan bestPlan = null;
        int bestPlanCost = Integer.MAX_VALUE;
        for (List<OperatorImplementationVertex> implementationVertexList : new CartesianIterable<>(costDimensionsPerVertex)) {
            bundle.incrCreatedPlans();
            bundle.incrExploredPlans();
            ExhaustivePlan plan = new ExhaustivePlan(implementationVertexList, implementationGraph, getCostEstimator());
            if (plan.cost() < bestPlanCost) {
                bestPlan = plan;
                bestPlanCost = plan.cost();
            }
        }

        //Verify that we have found a solution
        if (bestPlan == null) {
            throw new OptimizerException("Failed to find a valid plan for the input workflow.");
        }

        //Verify solution is OK
        patchSolution(bestPlan, implementationGraph);

        //Send to output
        LOGGER.info(bundle.getStatisticsBundle().toString());

        //TODO Consider offering more plans
        bundle.offerPlans(Collections.singletonList(bestPlan));
    }

}
