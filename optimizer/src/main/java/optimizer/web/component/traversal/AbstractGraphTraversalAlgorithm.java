package optimizer.web.component.traversal;

import optimizer.graph.implementation.OperatorImplementationGraph;
import optimizer.graph.implementation.OperatorImplementationVertex;
import optimizer.graph.iterable.DAGIterable;
import optimizer.graph.operator.OperatorGraph;
import optimizer.graph.operator.OperatorVertex;
import optimizer.model.OperatorImplementation;
import optimizer.model.OptimizationCost;
import optimizer.parser.network.AvailablePlatform;
import optimizer.parser.network.Site;
import optimizer.plan.AbstractOptimizerPlan;
import optimizer.web.service.CostEstimatorService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public abstract class AbstractGraphTraversalAlgorithm implements GraphTraversalAlgorithm {
    protected static final Logger LOGGER = LogManager.getLogger(AbstractGraphTraversalAlgorithm.class.getName());

    //Stats
    private final List<String> names;
    private final CostEstimatorService costEstimator;

    public AbstractGraphTraversalAlgorithm(CostEstimatorService costEstimator, List<String> name) {
        this.costEstimator = costEstimator;
        this.names = name;
    }

    @Override
    public List<String> aliases() {
        return this.names;
    }

    @Override
    public String toString() {
        return "AbstractGraphTraversalAlgorithm{" +
                "costEstimator=" + costEstimator +
                ", id='" + this.aliases() + '\'' +
                '}';
    }

    /**
     * Unpack and create a new operator implementation cost object.
     *
     * @return A new operator implementation object.
     */
    protected OperatorImplementation createNewOperatorImplCostModel(OperatorImplementationGraph graph,
                                                                    List<OptimizationCost> costDims) {
        Site site = (Site) costDims.get(0);
        int siteCost = costEstimator.estimateCost(site, graph);
        AvailablePlatform platform = (AvailablePlatform) costDims.get(1);
        int platformCost = costEstimator.estimateCost(platform, graph);
        return new OperatorImplementation(site, platform, siteCost, platformCost);
    }

    /**
     * Traverse the solution operators and patch operators with no implementations by assigning them
     * valid platforms (inherited from their predecessors).
     */
    public void patchSolution(AbstractOptimizerPlan plan, OperatorImplementationGraph implementationGraph) {
        OperatorGraph opGraph = implementationGraph.getOperatorGraph();
        for (OperatorVertex vertex : new DAGIterable(opGraph)) {
            if (vertex.equals(opGraph.getGraphSource()) || vertex.equals(opGraph.getGraphSink())) {
                continue;
            }
            OperatorImplementationVertex curImpl = plan.getImplementation(vertex);
            if (!curImpl.isValid()) {
                List<OperatorVertex> predecessors = opGraph.getPredecessors(vertex);
                if (predecessors.isEmpty()) {
                    LOGGER.error(String.format("Operator %s is not in dictionary and with no predecessors, SKIPPING!", vertex.getOperator().getName()));
                } else {
                    OperatorVertex selPred = predecessors.get(0);
                    OperatorImplementationVertex selPredImplVtx = plan.getImplementation(selPred);
                    curImpl.setImplementation(selPredImplVtx.getImplementation());
                    LOGGER.warn(String.format("Operator %s is not in dictionary and will inherit the implementation of %s."
                            , vertex.getOperator().getName(), selPred.getOperator().getName()));
                }
            }
        }
    }

    //Getters and Setters

    public CostEstimatorService getCostEstimator() {
        return costEstimator;
    }

    public List<String> getNames() {
        return this.names;
    }
}
