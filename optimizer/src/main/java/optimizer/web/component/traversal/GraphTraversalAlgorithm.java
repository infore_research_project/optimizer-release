package optimizer.web.component.traversal;

import optimizer.graph.operator.OperatorGraph;
import optimizer.web.OptimizationResourcesBundle;
import optimizer.web.controller.OptimizerException;

import java.util.List;

/**
 * Algorithms that can traverser.traverse an {@link OperatorGraph} should implement this interface.
 */
public interface GraphTraversalAlgorithm {

    /**
     * Applies the logic of this algorithm to the input graph.
     * Should be able to be called periodically with a reasonable runtime complexity.
     */
    void explorePaths(OptimizationResourcesBundle bundle) throws OptimizerException;

    /**
     * Unique identified of each algorithm.
     *
     * @return A List of Strings that uniquely identify an implementation (eg. ExhaustiveAlgorithm).
     */
    List<String> aliases();

}
