package optimizer.web.component;

import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import java.util.concurrent.Executor;

/**
 * Task executor for the optimizer service requests.
 */
@Component
public class OptimizerExecutorProvider {

    @Bean("optimizerExecutor")
    public Executor optimizerExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(3);
        executor.setMaxPoolSize(3);
        executor.setQueueCapacity(10);
        executor.setThreadNamePrefix("AsynchThread-");
        executor.setAllowCoreThreadTimeOut(true);
        //The setKeepAliveSeconds() should consider the execution time for the requests chain.
        //executor.setKeepAliveSeconds(10);
        executor.initialize();
        return executor;
    }
}
