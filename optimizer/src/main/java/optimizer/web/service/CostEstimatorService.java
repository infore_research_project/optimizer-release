package optimizer.web.service;

import optimizer.graph.implementation.OperatorImplementationEdge;
import optimizer.graph.implementation.OperatorImplementationGraph;
import optimizer.graph.operator.OperatorEdge;
import optimizer.graph.operator.OperatorVertex;
import optimizer.model.OptimizationCost;
import optimizer.parser.network.AvailablePlatform;
import optimizer.parser.network.Site;
import optimizer.web.repository.CostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import static optimizer.OptimizerApplication.masterLogger;

@Service
public class CostEstimatorService {

    private final int DEFAULT_COST = 1;
    private final int DEFAULT_PLATFORM_COST = 1000;
    private final int DEFAULT_INPUT_DATA_SIZE = 1000000;
    private final Map<String, Integer> cost;
    private final Random RNG;
    private final AtomicInteger callCounter;

    @Autowired
    private CostRepository costRepository;

    public CostEstimatorService() {
        this.callCounter = new AtomicInteger();
        this.cost = new HashMap<>();
        this.cost.put("flink", 10);
        this.cost.put("spark", DEFAULT_PLATFORM_COST + 1);
        this.cost.put("spark_structured_streaming", DEFAULT_PLATFORM_COST + 1);
        this.RNG = new Random(0);
    }

    @Scheduled(fixedDelay = 10000)
    private void scheduleFixedDelayTask() {
        masterLogger.debug("Polling DB for statistics...");
        //Iterable<CostEntity> f = costRepository.findAll();
    }

    public int estimateCost(OptimizationCost item, OperatorImplementationGraph graph) {
        //Keep track of estimator calls
        callCounter.incrementAndGet();

        if (item instanceof AvailablePlatform) {
            return estimatePlatformCost((AvailablePlatform) item, graph);
        } else if (item instanceof Site) {
            return estimateSiteCost((Site) item, graph);
        } else if (item instanceof OperatorVertex) {
            return estimateOperatorCost((OperatorVertex) item, graph);
        } else if (item instanceof OperatorImplementationEdge) {
            return estimateOpImplEdgeCost((OperatorImplementationEdge) item, graph);
        } else {
            throw new IllegalStateException("Unknown OptimizationCost item: " + item);
        }
    }


    //Subclasses should  override these methods instead of the estimateCost()
    public int estimatePlatformCost(AvailablePlatform item, OperatorImplementationGraph graph) {
        return this.cost.getOrDefault(item.getPlatformName(), DEFAULT_PLATFORM_COST + 2 + RNG.nextInt(DEFAULT_PLATFORM_COST));
    }

    //Migration cost
    public int estimateOpImplEdgeCost(OperatorImplementationEdge item, OperatorImplementationGraph graph) {
        int outputDataSize = (int) (0.1 * DEFAULT_INPUT_DATA_SIZE);
        return item.hasOpsInSamePlatform()
                ? outputDataSize / 10000
                : outputDataSize / 1000;
    }

    public int estimateSiteCost(Site item, OperatorImplementationGraph graph) {
        return DEFAULT_COST;
    }

    public int estimateOperatorCost(OperatorVertex item, OperatorImplementationGraph graph) {
        return DEFAULT_COST;
    }

    //Operator chaining cost
    public int getEstimatorCalls() {
        return this.callCounter.get();
    }

    public void clearEstimationCalls() {
        this.callCounter.set(0);
    }

}
