package optimizer.web.service;

import optimizer.parser.optimization.OptimizationRequest;
import optimizer.parser.optimization.OptimizerResponse;
import optimizer.web.OptimizationResourcesBundle;
import optimizer.web.controller.OptimizerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.CompletableFuture;

@Service
public class OptimizerService {

    @Autowired
    private OptimizationResourcesBundleProvider resourcesProviderService;

    @Autowired
    private PlanRetrievalService planRetrievalService;

    @Async("optimizerExecutor")
    public CompletableFuture<OptimizerResponse> optimize(OptimizationRequest optimizationRequest) throws ResourceNotFoundException, OptimizerException {
        //Get all the necessary resources
        OptimizationResourcesBundle optBundle = resourcesProviderService.provide(optimizationRequest);

        //Perform the optimization process
        //Traverse the graph, produce an optimal plan under constraints and update performance measures
        Instant startInstant = Instant.now();
        planRetrievalService.explorePlans(optBundle);
        int elapsedTime = Math.toIntExact(Duration.between(startInstant, Instant.now()).toMillis());
        optBundle.addStat("Path exploration time (ms)", elapsedTime);

        //Return the result as a future since this is an async task
        return CompletableFuture.completedFuture(new OptimizerResponse(optBundle));
    }
}
