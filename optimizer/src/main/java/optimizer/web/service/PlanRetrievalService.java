package optimizer.web.service;

import optimizer.web.OptimizationResourcesBundle;
import optimizer.web.component.traversal.GraphTraversalAlgorithm;
import optimizer.web.controller.OptimizerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlanRetrievalService {

    //Will gather all @Components implementing this interface type
    @Autowired
    private List<GraphTraversalAlgorithm> traversalAlgorithms;

    public void explorePlans(OptimizationResourcesBundle optBundle) throws OptimizerException {
        //Handle auto as exhaustive algorithm requests
        String algorithmName = optBundle.getTraversalAlgorithmName().toLowerCase().trim()
                .replace("auto", "exhaustive");
        optBundle.setTraversalAlgorithmName(algorithmName);

        //Find an algorithm matching the given name
        GraphTraversalAlgorithm algorithm = traversalAlgorithms.stream()
                .filter(gta -> gta.aliases().stream().anyMatch(a -> a.toLowerCase().trim().equals(algorithmName)))
                .findFirst()
                .orElseThrow(() -> new OptimizerException("Unknown optimizer algorithm: " + algorithmName));

        //Compute the plan
        algorithm.explorePaths(optBundle);
    }
}
