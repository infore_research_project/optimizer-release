package optimizer.web.service;

import optimizer.parser.dictionary.INFOREDictionary;
import optimizer.parser.network.INFORENetwork;
import optimizer.parser.optimization.OptimizationRequest;
import optimizer.parser.workflow.INFOREWorkflow;
import optimizer.web.OptimizationResourcesBundle;
import optimizer.web.repository.DictionaryRepository;
import optimizer.web.repository.NetworkRepository;
import optimizer.web.repository.WorkflowRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Class responsible for pulling and instantiating all necessary resources from repositories and databases.
 */
@Service
public class OptimizationResourcesBundleProvider {

    @Autowired
    private DictionaryRepository dictionaryRepository;

    @Autowired
    private NetworkRepository networkRepository;

    @Autowired
    private WorkflowRepository workflowRepository;


    public OptimizationResourcesBundle provide(OptimizationRequest optimizationRequest) {
        //Gather the optimizer resources
        INFORENetwork network = networkRepository.findById(optimizationRequest.getNetworkName())
                .orElseThrow(() -> new ResourceNotFoundException("Network not found"));

        INFOREDictionary dictionary = dictionaryRepository.findById(optimizationRequest.getDictionaryName())
                .orElseThrow(() -> new ResourceNotFoundException("Dictionary not found"));

        INFOREWorkflow workflow = workflowRepository.findById(optimizationRequest.getWorkflowName())
                .orElseThrow(() -> new ResourceNotFoundException("Workflow not found"));

        String algorithm = optimizationRequest.getAlgorithm();

        String version = optimizationRequest.getVersion();

        int threads = Math.toIntExact(optimizationRequest.getParallelism());

        int timeout = (int) optimizationRequest.getTimeout_ms();

        return new OptimizationResourcesBundle.Builder(optimizationRequest)
                .withNetwork(network)
                .withDictionary(dictionary)
                .withWorkflow(workflow)
                .withAlgorithm(algorithm)
                .withVersion(version)
                .withThreads(threads)
                .withTimeout(timeout)
                .get();
    }
}
