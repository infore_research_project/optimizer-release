package optimizer.model;

import optimizer.graph.operator.OperatorEdge;
import optimizer.graph.operator.OperatorVertex;

/**
 * Objects that exhibit cost should implement this interface in order for the optimizer to create a correct plan.
 * eg. {@link OperatorVertex}, {@link OperatorEdge}.
 */
public interface OptimizationCost {
}
