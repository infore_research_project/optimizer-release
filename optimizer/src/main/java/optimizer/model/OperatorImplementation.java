package optimizer.model;


import optimizer.parser.network.AvailablePlatform;
import optimizer.parser.network.Site;

/**
 * Cost model of an operator implementation.
 */
public class OperatorImplementation implements CostModel {
    //Optimizer needs to decide about these
    private final Site selectedSite;
    private final AvailablePlatform selectedPlatform;

    //Cost estimator needs to assign costs for these
    private final int siteCost;
    private final int platformCost;

    public OperatorImplementation() {
        this.selectedSite = null;
        this.selectedPlatform = null;
        this.siteCost = 0;
        this.platformCost = 0;
    }

    public OperatorImplementation(Site selectedSite, AvailablePlatform platform, int siteCost, int platformCost) {
        this.selectedSite = selectedSite;
        this.selectedPlatform = platform;
        this.siteCost = siteCost;
        this.platformCost = platformCost;
    }

    public boolean isValid() {
        return this.selectedPlatform != null && this.selectedSite != null;
    }

    @Override
    public int cost() {
        try {
            return Math.addExact(this.siteCost, this.platformCost);
        } catch (ArithmeticException e) {
            return Integer.MAX_VALUE;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OperatorImplementation that = (OperatorImplementation) o;

        if (siteCost != that.siteCost) return false;
        if (platformCost != that.platformCost) return false;
        if (selectedSite != null ? !selectedSite.equals(that.selectedSite) : that.selectedSite != null) return false;
        return selectedPlatform != null ? selectedPlatform.equals(that.selectedPlatform) : that.selectedPlatform == null;
    }

    @Override
    public int hashCode() {
        int result = selectedSite != null ? selectedSite.hashCode() : 0;
        result = 31 * result + (selectedPlatform != null ? selectedPlatform.hashCode() : 0);
        result = 31 * result + siteCost;
        result = 31 * result + platformCost;
        return result;
    }

    @Override
    public String toString() {
        return "OperatorImplCostModel{" +
                "selectedSite=" + (selectedSite == null ? null : selectedSite.getSiteName()) +
                ", selectedPlatform=" + (selectedPlatform == null ? null : selectedPlatform.getPlatformName()) +
                '}';
    }
    //Getters and Setters

    public Site getSelectedSite() {
        return selectedSite;
    }

    public AvailablePlatform getSelectedPlatform() {
        return selectedPlatform;
    }

    public int getSiteCost() {
        return siteCost;
    }

    public int getPlatformCost() {
        return platformCost;
    }
}
