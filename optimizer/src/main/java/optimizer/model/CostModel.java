package optimizer.model;

public interface CostModel {
    /**
     * Cost estimation of this object.
     *
     * @return The cost of the object.
     */
    int cost();

    /**
     * Checks weather the cost model can be used for an implementation.
     *
     * @return True if the cost model is valid, false otherwise.
     */
    boolean isValid();
}
