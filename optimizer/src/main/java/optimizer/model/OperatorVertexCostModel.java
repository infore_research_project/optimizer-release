package optimizer.model;

/**
 * Cost model of an operator
 */
public class OperatorVertexCostModel implements CostModel {
    private int operatorCost;
    private int heuristicCost;

    public OperatorVertexCostModel(int operatorCost, int heuristicCost) {
        this.operatorCost = operatorCost;
        this.heuristicCost = heuristicCost;
    }

    //Default operator cost is 0
    public OperatorVertexCostModel() {
        this(0, 0);
    }

    @Override
    public int cost() {
        try {
            return Math.addExact(this.operatorCost, this.heuristicCost);
        } catch (ArithmeticException e) {
            return Integer.MAX_VALUE;
        }
    }

    @Override
    public boolean isValid() {
        int thisCost = this.cost();
        return 0 <= thisCost && thisCost < Integer.MAX_VALUE;
    }

    @Override
    public String toString() {
        return "OperatorCostModel{" +
                "operatorCost=" + operatorCost +
                ", heuristicCost=" + heuristicCost +
                '}';
    }

    //Getters and setters
    public int getOperatorCost() {
        return operatorCost;
    }

    public void setOperatorCost(int operatorCost) {
        this.operatorCost = operatorCost;
    }

    public int getHeuristicCost() {
        return heuristicCost;
    }

    public void setHeuristicCost(int heuristicCost) {
        this.heuristicCost = heuristicCost;
    }

    public void incHeuristicCost(int extraHeuristicCost) {
        this.heuristicCost += extraHeuristicCost;
    }
}
