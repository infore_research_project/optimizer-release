package optimizer.graph.iterable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CartesianIterable<T> implements Iterable<List<T>> {
    protected static final Logger LOGGER = LogManager.getLogger(CartesianIterable.class.getName());

    private final int[] pointers;
    private final int[] bounds;
    private final List<List<T>> lists;
    private final int level;
    private long numOfItems;

    public CartesianIterable(List<List<T>> lists) {
        this.lists = lists;
        this.pointers = new int[lists.size()];
        this.bounds = new int[lists.size()];
        this.level = lists.size() - 1;
        this.numOfItems = 1;
        for (int i = 0; i < lists.size(); i++) {
            this.bounds[i] = lists.get(i).size() - 1;
            this.pointers[i] = 0;
            this.numOfItems *= lists.get(i).size();
        }
    }

    /**
     * Get a sequence of Ts from the pointers array.
     *
     * @return A List<T> .
     */
    private List<T> listFromPointers() {
        List<T> result = new ArrayList<>();
        for (int i = 0; i < lists.size(); i++) {
            T item = lists.get(i).get(pointers[i]);
            result.add(item);
        }
        return result;
    }

    @Override
    public Iterator<List<T>> iterator() {
        return new Iterator<List<T>>() {
            @Override
            public boolean hasNext() {
                return numOfItems > 0;
            }

            @Override
            public List<T> next() {
                //Get this iteration
                List<T> result = listFromPointers();

                //Add one and wrap around if necessary
                pointers[level]++;
                for (int i = lists.size() - 1; i >= 0; i--) {
                    if (pointers[i] > bounds[i]) {
                        pointers[i] = 0;
                        if (i > 0) {
                            pointers[i - 1]++;
                        }
                    }
                }

                //LOGGER.info(Arrays.toString(pointers));
                numOfItems--;
                return result;
            }
        };
    }
}
