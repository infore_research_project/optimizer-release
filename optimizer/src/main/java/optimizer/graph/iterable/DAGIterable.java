package optimizer.graph.iterable;

import optimizer.graph.operator.OperatorEdge;
import optimizer.graph.operator.OperatorGraph;
import optimizer.graph.operator.OperatorVertex;
import org.jgrapht.Graph;
import org.jgrapht.traverse.TopologicalOrderIterator;

import java.util.Iterator;
import java.util.Objects;
import java.util.function.Consumer;

public class DAGIterable implements Iterable<OperatorVertex> {
    private final Graph<OperatorVertex, OperatorEdge> graph;

    public DAGIterable(OperatorGraph graph) {
        this.graph = graph.getGraph();
    }

    public DAGIterable(Graph<OperatorVertex, OperatorEdge> graph) {
        this.graph = graph;
    }

    @Override
    public Iterator<OperatorVertex> iterator() {
        return new TopologicalOrderIterator<>(this.graph);
    }

    @Override
    public void forEach(Consumer<? super OperatorVertex> action) {
        Objects.requireNonNull(action);
        for (OperatorVertex operatorVertex : this) {
            action.accept(operatorVertex);
        }
    }
}
