package optimizer.graph.iterable;

import optimizer.graph.operator.OperatorGraph;
import optimizer.graph.operator.OperatorVertex;
import org.jgrapht.Graphs;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Stack;

/**
 * Iterator that traverses an {@link OperatorGraph} starting from the ending nodes and ending on the starting nodes.
 */
public class ReverseDAGIterable implements Iterable<OperatorVertex> {
    private final OperatorGraph graph;
    private final Set<OperatorVertex> visited;

    public ReverseDAGIterable(final OperatorGraph operatorGraph) {
        this.graph = operatorGraph;
        this.visited = new HashSet<>();
    }

    @Override
    public Iterator<OperatorVertex> iterator() {
        Stack<OperatorVertex> stack = new Stack<>();
        stack.add(this.graph.getGraphSink());
        return new Iterator<OperatorVertex>() {
            @Override
            public boolean hasNext() {
                return !stack.isEmpty();
            }

            @Override
            public OperatorVertex next() {
                OperatorVertex next = stack.pop();
                visited.add(next);
                for (OperatorVertex vertex : Graphs.predecessorListOf(graph.getGraph(), next)) {
                    if (!visited.contains(vertex)) {
                        stack.add(vertex);
                    }
                }
                return next;
            }
        };
    }
}
