package optimizer.graph.operator;


import optimizer.parser.dictionary.INFOREDictionary;
import optimizer.parser.dictionary.Platforms;
import optimizer.parser.network.INFORENetwork;
import optimizer.parser.network.Site;
import optimizer.parser.workflow.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jgrapht.Graph;
import org.jgrapht.Graphs;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DirectedAcyclicGraph;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Graph class containing {@link OperatorVertex} objects representing objects inside
 * {@link Platforms} and {@link Site} objects. Each Graph is assigned to exactly
 * one {@link INFORENetwork}.
 * <p>
 * Both operator vertices and operator connections have a cost that changes periodically and is used to decide
 * the optimal placement of an operator.
 * <p>
 * Each operator vertex will be placed in every available platform by checking the {@link INFOREDictionary} and every
 * available {@link Site}. Connections between operators will always form a path from the global source to the
 * global sink (see below).
 * <p>
 * Note: Some graph traversal algorithms require a single source/sink vertex in order to produce valid results
 * which is why a global source and sink vertices will always be present. These vertices can be identified by their
 * unique names, 'source' and 'sink' respectively.
 */
public class OperatorGraph {
    private static final Logger LOGGER = LogManager.getLogger(OperatorGraph.class.getName());

    /**
     * The Graph of the operator and operator connections which represent operator implementations.
     * Each outgoing edge represents an operator implementation.
     * Operator specific cost details can be found inside the vertices and operator implementation specific cost
     * details can be found in the edges.
     */
    private final DirectedAcyclicGraph<OperatorVertex, OperatorEdge> operatorGraph;

    //Keep track of source and sink ORs.
    private final OperatorVertex graphSource;
    private final OperatorVertex graphSink;
    private final Set<Operator> uniqueOperators;
    private final Map<String, OperatorVertex> operatorVertexMap;
    private final Map<String, OperatorVertex> realOperators;

    public OperatorGraph(INFOREWorkflow workflow) {
        //Operator graph
        this.operatorGraph = new DirectedAcyclicGraph<>(OperatorEdge.class);
        this.uniqueOperators = new HashSet<>();
        this.operatorVertexMap = new HashMap<>();
        this.realOperators = new HashMap<>();

        //Add source and sink vertices
        this.graphSource = new OperatorVertex(new Operator("START_OR"));
        this.operatorGraph.addVertex(this.graphSource);
        this.uniqueOperators.add(this.graphSource.getOperator());
        this.operatorVertexMap.put(this.graphSource.getOperator().getName(), this.graphSource);
        this.realOperators.put(this.graphSource.getOperator().getName(), this.graphSource);

        this.graphSink = new OperatorVertex(new Operator("END_OR"));
        this.operatorGraph.addVertex(this.graphSink);
        this.uniqueOperators.add(this.graphSink.getOperator());
        this.operatorVertexMap.put(this.graphSink.getOperator().getName(), this.graphSink);
        this.realOperators.put(this.graphSink.getOperator().getName(), this.graphSink);

        //Each workflow contains a sub-DAG
        DirectedAcyclicGraph<INFOREWorkflow, DefaultEdge> workflowGraph = new DirectedAcyclicGraph<>(DefaultEdge.class);

        //Discover all workflows
        workflowDFS(workflow, null, workflowGraph);

        //Connect all workflow operators
        for (INFOREWorkflow wf : workflowGraph.vertexSet()) {
            connectWorkflow(wf, workflowGraph);
        }

        //Remove "intermediate" operators
        Set<OperatorVertex> toRemove = new HashSet<>();
        Set<OperatorEdge> toAdd = new HashSet<>();
        for (OperatorEdge edge : this.operatorGraph.edgeSet()) {
            OperatorVertex from = edge.getFrom();
            OperatorVertex to = edge.getTo();
            if (!this.realOperators.containsValue(to)) {
                for (OperatorVertex succ : Graphs.successorListOf(operatorGraph, to)) {
                    toAdd.add(new OperatorEdge(from, succ));
                }
                toRemove.add(to);
            }
        }
        for (OperatorEdge edge : toAdd) {
            this.operatorGraph.addEdge(edge.getFrom(), edge.getTo(), edge);
        }
        this.operatorGraph.removeAllVertices(toRemove);

        //Locate input and output operators and connect them to the OR source/sink
        this.operatorGraph.vertexSet().stream()
                .filter(vertex -> !vertex.equals(this.graphSource))
                .filter(vertex -> !vertex.equals(this.graphSink))
                .forEach(v -> {
                    if (operatorGraph.inDegreeOf(v) == 0) {
                        this.operatorGraph.addEdge(this.graphSource, v, new OperatorEdge(this.graphSource, v));
                    } else if (operatorGraph.outDegreeOf(v) == 0) {
                        this.operatorGraph.addEdge(v, this.graphSink, new OperatorEdge(v, this.graphSink));
                    }
                });

        LOGGER.info(String.format("Operator graph vertices: %d, edges: %d", this.operatorGraph.vertexSet().size(), this.operatorGraph.edgeSet().size()));
    }

    private Operator findOperatorFromInnerSink(INFOREWorkflow workflow, InnerSinksPortsAndSchema ispas) {
        for (Operator operator : workflow.getOperators()) {
            for (OutputPortsAndSchema opas : operator.getOutputPortsAndSchemas().stream().filter(OutputPortsAndSchema::isIsConnected).collect(Collectors.toList())) {
                if (opas.getName().equals(ispas.getName())) {
                    return operator;
                }
            }
        }
        return null;
    }

    private void connectWorkflow(INFOREWorkflow workflow,
                                 DirectedAcyclicGraph<INFOREWorkflow, DefaultEdge> workflowGraph) {

        for (InnerSinksPortsAndSchema ispas : workflow.getInnerSinksPortsAndSchemas().stream().filter(InnerSinksPortsAndSchema::isIsConnected).collect(Collectors.toList())) {
            Operator thisOperator = findOperatorFromInnerSink(workflow, ispas);
            if (thisOperator == null) {
                continue;
            }

            String portClass = ispas.getObjectClass();
            INFOREWorkflow parentWF = Graphs.predecessorListOf(workflowGraph, workflow).get(0);

            //Need the parent operator of the parentWF
            OperatorVertex parentOPVtx = this.operatorVertexMap.get(workflow.getEnclosingOperatorName());
            if (parentOPVtx == null) {
                continue;
            }
            Operator parentOP = parentOPVtx.getOperator();

            for (INFOREWorkflow otherWF : parentOP.getInnerWorkflows().stream()
                    .filter(wf -> !wf.getWorkflowName().equals(workflow.getWorkflowName()))
                    .collect(Collectors.toList())) {

                for (Operator otherOP : otherWF.getOperators()) {
                    for (InputPortsAndSchema ipas : otherOP.getInputPortsAndSchemas().stream().filter(InputPortsAndSchema::isIsConnected).collect(Collectors.toList())) {
                        if (ipas.getObjectClass().equals(portClass)) {
                            OperatorVertex src = this.operatorVertexMap.get(thisOperator.getName());
                            OperatorVertex dst = this.operatorVertexMap.get(otherOP.getName());
                            this.operatorGraph.addEdge(src, dst, new OperatorEdge(src, dst));
                        }
                    }
                }
            }

        }

        for (OperatorConnection conn : workflow.getOperatorConnections()) {
            OperatorVertex from = this.operatorVertexMap.get(conn.getFromOperator());
            OperatorVertex to = this.operatorVertexMap.get(conn.getToOperator());
            if (!to.getOperator().getName().equals(workflow.getEnclosingOperatorName())) {
                this.operatorGraph.addEdge(from, to, new OperatorEdge(from, to));
            }
        }
    }

    public void workflowDFS(INFOREWorkflow workflow,
                            INFOREWorkflow parentWorkflow,
                            DirectedAcyclicGraph<INFOREWorkflow, DefaultEdge> graph) {
        graph.addVertex(workflow);
        if (parentWorkflow != null) {
            graph.addEdge(parentWorkflow, workflow);
        }
        for (Operator operator : workflow.getOperators()) {
            OperatorVertex vtx = new OperatorVertex(operator);
            this.uniqueOperators.add(operator);
            this.operatorVertexMap.put(operator.getName(), vtx);
            this.operatorGraph.addVertex(vtx);

            if (operator.isHasSubprocesses()) {
                for (INFOREWorkflow innerWF : operator.getInnerWorkflows()) {
                    workflowDFS(innerWF, workflow, graph);
                }
            } else {
                this.realOperators.put(operator.getName(), vtx);
            }
        }
    }

    public List<OperatorVertex> getEffectiveOperators() {
        List<OperatorVertex> list = new ArrayList<>(this.operatorGraph.vertexSet());
        list.remove(this.graphSource);
        list.remove(this.graphSink);
        return list;
    }

    public List<OperatorVertex> getPredecessors(OperatorVertex vertex) {
        return Graphs.predecessorListOf(this.operatorGraph, vertex);
    }

    @Override
    public String toString() {
        return "OptimizerGraph{" +
                "operatorGraph=" + operatorGraph.vertexSet().toString() +
                ", operatorGraphSource=" + graphSource +
                ", operatorGraphSink=" + graphSink +
                '}';
    }

    //Getters and setters
    public Graph<OperatorVertex, OperatorEdge> getGraph() {
        return operatorGraph;
    }

    public OperatorVertex getGraphSource() {
        return graphSource;
    }

    public OperatorVertex getGraphSink() {
        return graphSink;
    }
}
