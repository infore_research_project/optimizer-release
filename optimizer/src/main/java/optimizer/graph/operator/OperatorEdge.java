package optimizer.graph.operator;

import optimizer.model.OptimizationCost;

import java.util.Objects;

public class OperatorEdge implements OptimizationCost {
    //Refs to the operator vertices (mostly for equality checks)
    private OperatorVertex from, to;

    //Optimizer graph constructor
    public OperatorEdge(OperatorVertex from, OperatorVertex to) {
        this.from = from;
        this.to = to;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OperatorEdge that = (OperatorEdge) o;
        return from.equals(that.from) &&
                to.equals(that.to);
    }

    @Override
    public int hashCode() {
        return Objects.hash(from, to);
    }

    @Override
    public String toString() {
        return "OperatorEdge{" +
                "from=" + from.getOperator().getName() +
                ", to=" + to.getOperator().getName() +
                '}';
    }

    //Getters and Setters
    public OperatorVertex getFrom() {
        return from;
    }

    public void setFrom(OperatorVertex from) {
        this.from = from;
    }

    public OperatorVertex getTo() {
        return to;
    }

    public void setTo(OperatorVertex to) {
        this.to = to;
    }
}
