package optimizer.graph.operator;


import optimizer.model.OperatorVertexCostModel;
import optimizer.model.OptimizationCost;
import optimizer.parser.workflow.Operator;

import java.util.Objects;

/**
 * Vertex class of {@link OperatorGraph}.
 * Represents an operator and its attributes.
 */
public class OperatorVertex implements OptimizationCost, Comparable<OperatorVertex> {

    //The RM operator
    private Operator operator;

    //Vertex cost related info
    private OperatorVertexCostModel operatorVertexCostModel;


    public OperatorVertex(Operator operator, OperatorVertexCostModel operatorVertexCostModel) {
        this.operator = operator;
        this.operatorVertexCostModel = operatorVertexCostModel;
    }

    //Optimizer graph constructor
    public OperatorVertex(Operator operator) {
        this(operator, new OperatorVertexCostModel());
    }

    @Override
    public String toString() {
        return "OperatorVertex{" +
                "operator=" + operator.getName() +
                ", operatorCostModel=" + operatorVertexCostModel +
                '}';
    }

    @Override
    public int compareTo(OperatorVertex o) {
        return this.operatorVertexCostModel.cost() - o.getOperatorVertexCostModel().cost();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OperatorVertex vertex = (OperatorVertex) o;
        return Objects.equals(operator, vertex.operator);
    }


    @Override
    public int hashCode() {
        return Objects.hash(operator);
    }

    //Getters and Setters
    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public OperatorVertexCostModel getOperatorVertexCostModel() {
        return operatorVertexCostModel;
    }

    public void setOperatorVertexCostModel(OperatorVertexCostModel operatorVertexCostModel) {
        this.operatorVertexCostModel = operatorVertexCostModel;
    }
}
