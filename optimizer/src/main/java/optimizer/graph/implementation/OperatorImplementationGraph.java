package optimizer.graph.implementation;

import optimizer.graph.operator.OperatorEdge;
import optimizer.graph.operator.OperatorGraph;
import optimizer.graph.operator.OperatorVertex;
import optimizer.model.OperatorImplementation;
import optimizer.parser.workflow.Operator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jgrapht.Graphs;
import org.jgrapht.graph.DirectedAcyclicGraph;

import java.util.*;

public class OperatorImplementationGraph {
    private static final Logger LOGGER = LogManager.getLogger(OperatorImplementationGraph.class.getName());

    private final OperatorGraph operatorGraph;
    private final DirectedAcyclicGraph<OperatorImplementationVertex, OperatorImplementationEdge> implementationGraph;
    private final Map<OperatorVertex, Set<OperatorImplementationVertex>> operatorMap;
    private final Map<OperatorImplementationVertex, Integer> opImplMap;
    private OperatorImplementationVertex rootVertex;
    private Operator goalOperator;

    public OperatorImplementationGraph(OperatorGraph graph) {
        this.operatorGraph = graph;
        this.rootVertex = new OperatorImplementationVertex(graph.getGraphSource(), new OperatorImplementation());
        this.implementationGraph = new DirectedAcyclicGraph<>(OperatorImplementationEdge.class);
        this.implementationGraph.addVertex(this.rootVertex);
        this.goalOperator = graph.getGraphSink().getOperator();
        this.operatorMap = new HashMap<>();
        this.opImplMap = new HashMap<>();
    }

    public void addVertex(OperatorImplementationVertex impl) {
        this.implementationGraph.addVertex(impl);
        this.operatorMap.putIfAbsent(impl.getOpVertex(), new HashSet<>());
        this.operatorMap.get(impl.getOpVertex()).add(impl);
    }

    //from and to should already be present in the graph
    public void addEdge(OperatorImplementationVertex from, OperatorImplementationVertex to) {
        this.implementationGraph.addEdge(from, to, new OperatorImplementationEdge(from, to));
    }

    public List<OperatorImplementationVertex> getPredecessors(OperatorImplementationVertex vertex) {
        return Graphs.predecessorListOf(this.implementationGraph, vertex);
    }

    public List<OperatorVertex> getPredecessors(OperatorVertex vertex) {
        return Graphs.predecessorListOf(this.operatorGraph.getGraph(), vertex);
    }

    public List<OperatorImplementationVertex> getSuccessors(OperatorImplementationVertex vertex) {
        return Graphs.successorListOf(this.implementationGraph, vertex);
    }

    public List<OperatorVertex> getSuccessors(OperatorVertex vertex) {
        return Graphs.successorListOf(this.operatorGraph.getGraph(), vertex);
    }

    public OperatorImplementationEdge getEdgeBetween(OperatorImplementationVertex src, OperatorImplementationVertex tgt) {
        return this.implementationGraph.getEdge(src, tgt);
    }

    public OperatorEdge getEdgeBetween(OperatorVertex src, OperatorVertex tgt) {
        return this.operatorGraph.getGraph().getEdge(src, tgt);
    }

    public boolean containsVertex(OperatorImplementationVertex vertex) {
        return this.implementationGraph.containsVertex(vertex);
    }

    public boolean containsVertex(OperatorVertex vertex) {
        return this.operatorGraph.getGraph().containsVertex(vertex);
    }

    @Override
    public String toString() {
        return "OperatorImplementationGraph{" +
                "implementationGraph=" + implementationGraph +
                ", rootVertex=" + rootVertex +
                ", goalOperator=" + goalOperator.getName() +
                '}';
    }


    //Getters and setters
    public OperatorImplementationVertex getRootVertex() {
        return rootVertex;
    }

    public void setRootVertex(OperatorImplementationVertex rootVertex) {
        this.rootVertex = rootVertex;
    }

    public Operator getGoalOperator() {
        return goalOperator;
    }

    public void setGoalOperator(Operator goalOperator) {
        this.goalOperator = goalOperator;
    }

    public Map<OperatorVertex, Set<OperatorImplementationVertex>> getOperatorMap() {
        return operatorMap;
    }

    public Map<OperatorImplementationVertex, Integer> getOpImplMap() {
        return opImplMap;
    }

    public OperatorGraph getOperatorGraph() {
        return operatorGraph;
    }
}
