package optimizer.graph.implementation;

import optimizer.model.OptimizationCost;

public class OperatorImplementationEdge implements OptimizationCost {
    private OperatorImplementationVertex from, to;
    private int cost;

    public OperatorImplementationEdge(OperatorImplementationVertex from, OperatorImplementationVertex to) {
        this.from = from;
        this.to = to;
    }

    public OperatorImplementationEdge(OperatorImplementationVertex from, OperatorImplementationVertex to, int cost) {
        this.from = from;
        this.to = to;
        this.cost = cost;
    }

    public boolean hasOpsInSamePlatform() {
        if (from.getImplementation().getSelectedPlatform() == null) {
            return to.getImplementation().getSelectedPlatform() == null;
        }
        if (to.getImplementation().getSelectedPlatform() == null) {
            return from.getImplementation().getSelectedPlatform() == null;
        }
        return from.getImplementation().getSelectedPlatform().equals(to.getImplementation().getSelectedPlatform());
    }

    @Override
    public String toString() {
        return "OperatorImplementationEdge{" +
                "from=" + from +
                ", to=" + to +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OperatorImplementationEdge that = (OperatorImplementationEdge) o;

        if (from != null ? !from.equals(that.from) : that.from != null) return false;
        return to != null ? to.equals(that.to) : that.to == null;
    }

    @Override
    public int hashCode() {
        int result = from != null ? from.hashCode() : 0;
        result = 31 * result + (to != null ? to.hashCode() : 0);
        return result;
    }

    public OperatorImplementationVertex getFrom() {
        return from;
    }

    public void setFrom(OperatorImplementationVertex from) {
        this.from = from;
    }

    public OperatorImplementationVertex getTo() {
        return to;
    }

    public void setTo(OperatorImplementationVertex to) {
        this.to = to;
    }

}
