package optimizer.graph.implementation;

import optimizer.graph.operator.OperatorVertex;
import optimizer.model.CostModel;
import optimizer.model.OperatorImplementation;

public class OperatorImplementationVertex implements CostModel, Comparable<OperatorImplementationVertex> {
    //Ref to the operator vertex in the operator graph
    private OperatorVertex vertex;

    //Operator implementation object with all cost dimensions and their costs
    private OperatorImplementation implementation;

    public OperatorImplementationVertex(OperatorVertex operatorVertex, OperatorImplementation implementation) {
        this.vertex = operatorVertex;
        this.implementation = implementation;
    }

    @Override
    public int cost() { //TODO check
        return this.implementation.cost() + vertex.getOperatorVertexCostModel().cost();
    }

    @Override
    public int compareTo(OperatorImplementationVertex o) {
        return this.cost() - o.cost();
    }

    @Override
    public boolean isValid() {
        return this.implementation.isValid();
    }

    @Override
    public String toString() {
        return "OperatorImplementationVertex{" +
                "vertex=" + vertex.getOperator().getName() +
                ", implementation=" + implementation +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OperatorImplementationVertex that = (OperatorImplementationVertex) o;

        if (vertex != null ? !vertex.equals(that.vertex) : that.vertex != null) return false;
        return implementation != null ? implementation.equals(that.implementation) : that.implementation == null;
    }

    @Override
    public int hashCode() {
        int result = vertex != null ? vertex.hashCode() : 0;
        result = 31 * result + (implementation != null ? implementation.hashCode() : 0);
        return result;
    }

    public OperatorVertex getOpVertex() {
        return vertex;
    }

    public void setVertex(OperatorVertex vertex) {
        this.vertex = vertex;
    }

    public OperatorImplementation getImplementation() {
        return implementation;
    }

    public void setImplementation(OperatorImplementation implementation) {
        this.implementation = implementation;
    }
}
