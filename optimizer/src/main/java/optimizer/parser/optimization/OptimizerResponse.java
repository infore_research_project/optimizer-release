package optimizer.parser.optimization;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import optimizer.graph.implementation.OperatorImplementationVertex;
import optimizer.parser.network.INFORENetwork;
import optimizer.parser.network.Site;
import optimizer.parser.workflow.INFOREWorkflow;
import optimizer.parser.workflow.PlacementPlatform;
import optimizer.parser.workflow.PlacementSite;
import optimizer.plan.AbstractOptimizerPlan;
import optimizer.web.OptimizationResourcesBundle;
import optimizer.web.controller.OptimizerException;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import javax.persistence.ElementCollection;
import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * The output of an optimization process.
 */
@Document(indexName = "infore_result")
@Data
@EqualsAndHashCode(exclude = "modifiedAt")
public class OptimizerResponse {
    //Don't expose to JSON
    @Field(name = "@timestamp", type = FieldType.Date, format = DateFormat.date_optional_time)
    private final Date modifiedAt = new Date();
    @SerializedName("id")
    @Expose
    @org.springframework.data.annotation.Id
    @javax.persistence.Id
    @Field(name = "id", type = FieldType.Text, store = true)
    private String id;
    @SerializedName("networkName")
    @Expose
    @Field(name = "networkName", type = FieldType.Text, store = true)
    private String networkName;
    @SerializedName("dictionaryName")
    @Expose
    @Field(name = "dictionaryName", type = FieldType.Text, store = true)
    private String dictionaryName;
    @SerializedName("requestID")
    @Expose
    @Field(name = "requestID", type = FieldType.Text, store = true)
    private String requestID;
    @SerializedName("algorithmUsed")
    @Expose
    @Field(name = "algorithmUsed", type = FieldType.Text, store = true)
    private String algorithmUsed;
    @SerializedName("workflow")
    @Expose
    @Field(name = "workflow", type = FieldType.Nested, store = true)
    private INFOREWorkflow workflow;
    @SerializedName("operatorsPretty")
    @Expose
    @Field(name = "operatorsPretty", type = FieldType.Text, store = true)
    private String operatorsPretty;
    @SerializedName("performance")
    @Expose
    @Field(name = "performance", type = FieldType.Text, store = true)
    private Map<String, String> performance;
    @SerializedName("alternativeWorkflows")
    @Expose
    @Field(name = "alternativeWorkflows", type = FieldType.Text, store = true)
    @ElementCollection
    private List<@Valid INFOREWorkflow> alternativeWorkflows;

    private OptimizerResponse() {
    }

    public OptimizerResponse(OptimizationResourcesBundle optBundle) throws OptimizerException {
        if (optBundle.getBestPlan() == null) {
            throw new OptimizerException("No suitable plans were found.");
        }
        this.id = String.format("%s_%d", optBundle.getWorkflow().getWorkflowName(), System.nanoTime());
        this.workflow = fillOutput(optBundle.getWorkflow(), optBundle.getBestPlan(), optBundle.getNetwork());
        this.operatorsPretty = operatorsToString(optBundle.getBestPlan());
        this.performance = optBundle.getStatisticsBundle().toStringMap();
        this.dictionaryName = optBundle.getDictionary().getDictionaryName();
        this.networkName = optBundle.getNetwork().getNetwork();
        this.requestID = optBundle.getRequest().getId();
        this.algorithmUsed = optBundle.getTraversalAlgorithmName();

        //For alternative plans
        this.alternativeWorkflows = new ArrayList<>();
        optBundle.getPlanQueue().poll();
        for (AbstractOptimizerPlan altPlan : optBundle.getPlanQueue()) {
            this.alternativeWorkflows.add(fillOutput(optBundle.getWorkflow(), altPlan, optBundle.getNetwork()));
        }
    }

    /**
     * A pretty operator summary of operator implementations sorted by cost.
     *
     * @param solution
     * @return A pretty summary string of the solution.
     */
    public String operatorsToString(AbstractOptimizerPlan solution) {
        StringBuilder stringBuilder = new StringBuilder("Operator placements\n");
        stringBuilder.append(String.format("[%s , %s , %s]\n", "operator", "cost", "implementation"));

        solution.getPathAndCosts().entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .forEach(entry -> stringBuilder
                        .append(entry.getKey().getOpVertex().getOperator().getName()).append(" , ")
                        .append(entry.getValue()).append(" , ")
                        .append(entry.getKey().getImplementation()).append("\n"));

        return stringBuilder.toString();
    }

    private INFOREWorkflow fillOutput(INFOREWorkflow workflow, AbstractOptimizerPlan optimizerPlan, INFORENetwork network) {
        workflow.setPlacementSites(new ArrayList<>());
        Map<String, PlacementPlatform> platformMap = new HashMap<>();

        for (Site site : network.getSites()) {
            PlacementSite newSite = new PlacementSite(site);
            workflow.getPlacementSites().add(newSite);
            platformMap.putAll(newSite.getPlatformMap());
        }

        if (optimizerPlan == null) {
            return workflow;
        }

        Map<OperatorImplementationVertex, Integer> implementations = optimizerPlan.getPathAndCosts();

        //Iterate over the solution entries
        for (OperatorImplementationVertex implVtx : implementations.keySet()) {
            String platformName = implVtx.getImplementation().getSelectedPlatform().getPlatformName();
            platformMap.get(platformName).addImpl(implVtx);
        }

        return workflow;
    }

    //Getters and Setters
    public String getId() {
        return id;
    }

    public INFOREWorkflow getWorkflow() {
        return workflow;
    }

    public String getOperatorsPretty() {
        return operatorsPretty;
    }

    public String getNetworkName() {
        return networkName;
    }

    public String getDictionaryName() {
        return dictionaryName;
    }

    public String getRequestID() {
        return requestID;
    }

    public Map<String, String> getPerformance() {
        return performance;
    }

    public String getAlgorithmUsed() {
        return algorithmUsed;
    }

    public String getModifiedAt() {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        return sdf.format(this.modifiedAt);
    }
}
