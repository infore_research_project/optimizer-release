package optimizer.parser.optimization;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

@Data
@Document(indexName = "infore_request")
@EqualsAndHashCode(exclude = "modifiedAt")
public class OptimizationRequest implements Serializable {

    private final static long serialVersionUID = 5126625308399007566L;
    //Don't expose to JSON
    @Field(name = "@timestamp", type = FieldType.Date, format = DateFormat.date_optional_time)
    private final Date modifiedAt = new Date();
    @SerializedName("id")
    @Expose(deserialize = false)    //Ignore user input timestamp, save the current timestamp instead.
    @org.springframework.data.annotation.Id
    @javax.persistence.Id
    private String id;
    @SerializedName("networkName")
    @Expose
    @NotEmpty(message = "Field networkName must not be empty")
    private String networkName;
    @SerializedName("dictionaryName")
    @Expose
    @NotEmpty(message = "Field dictionaryName must not be empty")
    private String dictionaryName;
    @SerializedName("workflowName")
    @Expose
    @NotEmpty(message = "Field workflowName must not be empty")
    private String workflowName;
    @SerializedName("version")
    @Expose
    private String version;
    @SerializedName("algorithm")
    @Expose
    @NotEmpty(message = "Field algorithm must not be empty")
    private String algorithm;
    @SerializedName("parallelism")
    @Expose
    @Min(1)
    private Long parallelism = 1L;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("timeout_ms")
    @Expose
    @Min(value = 1L, message = "Field timeout_ms must be greater than zero")
    private long timeout_ms = 10000;
    @SerializedName("numOfPlans")
    @Expose
    @Min(value = 1L, message = "Field numOfPlans should be positive.")
    private int numOfPlans = 1;

    /**
     * No args constructor for use in serialization
     */
    public OptimizationRequest() {
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public OptimizationRequest withTimestamp(String timestamp) {
        this.id = timestamp;
        return this;
    }

    public String getNetworkName() {
        return networkName;
    }

    public void setNetworkName(String networkName) {
        this.networkName = networkName;
    }

    public OptimizationRequest withNetworkName(String networkName) {
        this.networkName = networkName;
        return this;
    }

    public String getDictionaryName() {
        return dictionaryName;
    }

    public void setDictionaryName(String dictionaryName) {
        this.dictionaryName = dictionaryName;
    }

    public OptimizationRequest withDictionaryName(String dictionaryName) {
        this.dictionaryName = dictionaryName;
        return this;
    }

    public String getWorkflowName() {
        return workflowName;
    }

    public void setWorkflowName(String workflowName) {
        this.workflowName = workflowName;
    }

    public OptimizationRequest withWorkflowName(String workflowName) {
        this.workflowName = workflowName;
        return this;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public OptimizationRequest withVersion(String version) {
        this.version = version;
        return this;
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    public OptimizationRequest withAlgorithm(String algorithm) {
        this.algorithm = algorithm;
        return this;
    }

    public Long getParallelism() {
        return parallelism;
    }

    public void setParallelism(Long parallelism) {
        this.parallelism = parallelism;
    }

    public OptimizationRequest withParallelism(Long parallelism) {
        this.parallelism = parallelism;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getTimeout_ms() {
        return timeout_ms;
    }

    public void setTimeout_ms(long timeout_ms) {
        this.timeout_ms = timeout_ms;
    }

    public int getNumOfPlans() {
        return numOfPlans;
    }

    public void setNumOfPlans(int numOfPlans) {
        this.numOfPlans = numOfPlans;
    }

    public String getModifiedAt() {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        return sdf.format(this.modifiedAt);
    }
}