package optimizer.parser.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import optimizer.model.OptimizationCost;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import javax.persistence.ElementCollection;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

@Document(indexName = "infore_network")
@Data
@EqualsAndHashCode(exclude = "modifiedAt")
public class INFORENetwork implements Serializable, OptimizationCost {

    private final static long serialVersionUID = 5116414984211561420L;

    //Don't expose to JSON
    @Field(name = "@timestamp", type = FieldType.Date, format = DateFormat.date_optional_time)
    private final Date modifiedAt = new Date();

    @SerializedName("network")
    @Expose
    @javax.persistence.Id
    @org.springframework.data.annotation.Id
    @NotEmpty(message = "Network name must not be empty.")
    private String network;

    @SerializedName("sites")
    @Expose
    @NotEmpty(message = "sites must not be empty")
    @ElementCollection
    private Set<@Valid Site> sites = null;

    /**
     * No args constructor for use in serialization
     */
    public INFORENetwork() {
    }

    public INFORENetwork(String network, Set<Site> sites) {
        super();
        this.network = network;
        this.sites = sites;
    }

    public String getNetwork() {
        return network;
    }

    public void setNetwork(String network) {
        this.network = network;
    }

    public Set<Site> getSites() {
        return sites;
    }

    public boolean addSite(Site site) {
        return this.sites.add(site);
    }

    public boolean deleteSite(String site_name) {
        return sites.removeIf(site -> site.getSiteName().equals(site_name));
    }

    public String getModifiedAt() {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        return sdf.format(modifiedAt);
    }
}
