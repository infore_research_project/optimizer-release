package optimizer.parser.workflow;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import javax.persistence.ElementCollection;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Document(indexName = "infore_workflow")
@Data
@EqualsAndHashCode(exclude = "modifiedAt")
public class INFOREWorkflow implements Serializable {

    private final static long serialVersionUID = -1871235623182336168L;

    //Don't expose to JSON
    @Field(name = "@timestamp", type = FieldType.Date, format = DateFormat.date_optional_time)
    private final Date modifiedAt = new Date();

    @SerializedName("workflowName")
    @Expose
    @org.springframework.data.annotation.Id
    @javax.persistence.Id
    @NotEmpty(message = "workflowName must be empty")
    @Field(name = "workflowName", type = FieldType.Text, store = true)
    private String workflowName;

    @SerializedName("enclosingOperatorName")
    @Expose
    @NotEmpty(message = "enclosingOperatorName must not be empty")
    @Field(name = "enclosingOperatorName", type = FieldType.Text, store = true)
    private String enclosingOperatorName;

    @SerializedName("innerSourcesPortsAndSchemas")
    @Expose
    @NotNull(message = "InnerSourcesPortsAndSchema field must be present (can be empty)")
    @ElementCollection
    @Field(name = "innerSourcesPortsAndSchemas", type = FieldType.Text, store = true)
    private List<@Valid InnerSourcesPortsAndSchema> innerSourcesPortsAndSchemas = null;

    @SerializedName("innerSinksPortsAndSchemas")
    @Expose
    @NotNull(message = "innerSinksPortsAndSchemas field must be present (can be empty)")
    @ElementCollection
    @Field(name = "innerSinksPortsAndSchemas", type = FieldType.Text, store = true)
    private List<@Valid InnerSinksPortsAndSchema> innerSinksPortsAndSchemas = null;

    @SerializedName("operatorConnections")
    @Expose
    @NotNull(message = "operatorConnections field must be present (can be empty)")
    @ElementCollection
    @Field(name = "operatorConnections", type = FieldType.Text, store = true)
    private List<@Valid OperatorConnection> operatorConnections = null;

    @SerializedName("operators")
    @Expose
    @NotEmpty(message = "operators must not be empty")
    @ElementCollection
    @Field(name = "operators", type = FieldType.Text, store = true)
    private List<@Valid Operator> operators = null;

    @SerializedName("placementSites")
    @Expose
    @NotNull(message = "placementSites field must be present (can be empty)")
    @ElementCollection
    @Field(name = "placementSites", type = FieldType.Text, store = true)
    private List<@Valid PlacementSite> placementSites = null;

    /**
     * No args constructor for use in serialization
     */
    public INFOREWorkflow() {
    }

    public INFOREWorkflow(String workflowName, String enclosingOperatorName, List<InnerSourcesPortsAndSchema> innerSourcesPortsAndSchemas, List<InnerSinksPortsAndSchema> innerSinksPortsAndSchemas, List<OperatorConnection> operatorConnections, List<Operator> operators, List<PlacementSite> placementSites) {
        super();
        this.workflowName = workflowName;
        this.enclosingOperatorName = enclosingOperatorName;
        this.innerSourcesPortsAndSchemas = innerSourcesPortsAndSchemas;
        this.innerSinksPortsAndSchemas = innerSinksPortsAndSchemas;
        this.operatorConnections = operatorConnections;
        this.operators = operators;
        this.placementSites = placementSites;
    }

    public String getWorkflowName() {
        return workflowName;
    }

    public void setWorkflowName(String workflowName) {
        this.workflowName = workflowName;
    }

    public INFOREWorkflow withWorkflowName(String workflowName) {
        this.workflowName = workflowName;
        return this;
    }

    public String getEnclosingOperatorName() {
        return enclosingOperatorName;
    }

    public void setEnclosingOperatorName(String enclosingOperatorName) {
        this.enclosingOperatorName = enclosingOperatorName;
    }

    public INFOREWorkflow withEnclosingOperatorName(String enclosingOperatorName) {
        this.enclosingOperatorName = enclosingOperatorName;
        return this;
    }

    public List<InnerSourcesPortsAndSchema> getInnerSourcesPortsAndSchemas() {
        return innerSourcesPortsAndSchemas;
    }

    public void setInnerSourcesPortsAndSchemas(List<InnerSourcesPortsAndSchema> innerSourcesPortsAndSchemas) {
        this.innerSourcesPortsAndSchemas = innerSourcesPortsAndSchemas;
    }

    public INFOREWorkflow withInnerSourcesPortsAndSchemas(List<InnerSourcesPortsAndSchema> innerSourcesPortsAndSchemas) {
        this.innerSourcesPortsAndSchemas = innerSourcesPortsAndSchemas;
        return this;
    }

    public List<InnerSinksPortsAndSchema> getInnerSinksPortsAndSchemas() {
        return innerSinksPortsAndSchemas;
    }

    public void setInnerSinksPortsAndSchemas(List<InnerSinksPortsAndSchema> innerSinksPortsAndSchemas) {
        this.innerSinksPortsAndSchemas = innerSinksPortsAndSchemas;
    }

    public INFOREWorkflow withInnerSinksPortsAndSchemas(List<InnerSinksPortsAndSchema> innerSinksPortsAndSchemas) {
        this.innerSinksPortsAndSchemas = innerSinksPortsAndSchemas;
        return this;
    }

    public List<OperatorConnection> getOperatorConnections() {
        return operatorConnections;
    }

    public void setOperatorConnections(List<OperatorConnection> operatorConnections) {
        this.operatorConnections = operatorConnections;
    }

    public INFOREWorkflow withOperatorConnections(List<OperatorConnection> operatorConnections) {
        this.operatorConnections = operatorConnections;
        return this;
    }

    public List<Operator> getOperators() {
        return operators;
    }

    public void setOperators(List<Operator> operators) {
        this.operators = operators;
    }

    public INFOREWorkflow withOperators(List<Operator> operators) {
        this.operators = operators;
        return this;
    }

    public List<PlacementSite> getPlacementSites() {
        return placementSites;
    }

    public void setPlacementSites(List<PlacementSite> placementSites) {
        this.placementSites = placementSites;
    }

    public INFOREWorkflow withPlacementSites(List<PlacementSite> placementSites) {
        this.placementSites = placementSites;
        return this;
    }

    public String getModifiedAt() {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        return sdf.format(this.modifiedAt);
    }
}
