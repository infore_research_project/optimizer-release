package optimizer.parser.dictionary;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import optimizer.graph.operator.OperatorGraph;
import optimizer.graph.operator.OperatorVertex;
import optimizer.model.OptimizationCost;
import optimizer.parser.network.AvailablePlatform;
import optimizer.parser.network.INFORENetwork;
import optimizer.parser.network.Site;
import optimizer.parser.workflow.Operator;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import javax.persistence.ElementCollection;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.*;

import static optimizer.OptimizerApplication.masterLogger;

@Document(indexName = "infore_dictionary")
@Data
@EqualsAndHashCode(exclude = "modifiedAt")
public class INFOREDictionary implements Serializable {
    private final static long serialVersionUID = -9058417690405646489L;

    //Don't expose to JSON
    @Field(name = "@timestamp", type = FieldType.Date, format = DateFormat.date_optional_time)
    private final Date modifiedAt = new Date();

    @SerializedName("dictionaryName")
    @Expose
    @javax.persistence.Id
    @org.springframework.data.annotation.Id
    @NotEmpty(message = "Dictionary name must not be empty.")
    private String dictionaryName;

    @SerializedName("operators")
    @Expose
    @NotEmpty(message = "Dictionary Operators name must not be empty.")
    @ElementCollection
    private Set<@Valid DictionaryOperator> operators = null;

    /**
     * No args constructor for use in serialization
     */
    public INFOREDictionary() {
    }

    public INFOREDictionary(String dictionaryName, Set<DictionaryOperator> operators) {
        super();
        this.dictionaryName = dictionaryName;
        this.operators = operators;
    }

    public String getDictionaryName() {
        return dictionaryName;
    }

    public boolean addOperator(DictionaryOperator dictionaryOperator) {
        return operators.add(dictionaryOperator);
    }

    public List<Site> getAvailableSites(Operator operator, INFORENetwork network) {
        DictionaryOperator dictionaryOperator = this.operators.stream()
                .filter(op -> op.getClassKey().equals(operator.getClassKey()))
                .findFirst()
                .orElse(null);
        if (dictionaryOperator == null) {
            return new ArrayList<>();
        }
        List<Site> sites = new ArrayList<>();
        for (Site site : network.getSites()) {
            for (AvailablePlatform platform : site.getAvailablePlatforms(dictionaryOperator.getPlatforms())) {
                sites.add(site);
            }
        }
        return sites;
    }

    public List<AvailablePlatform> getAvailablePlatforms(Operator operator, INFORENetwork network) {
        DictionaryOperator dictionaryOperator = this.operators.stream()
                .filter(op -> op.getClassKey().equals(operator.getClassKey()))
                .findFirst()
                .orElse(null);
        if (dictionaryOperator == null) {
            return new ArrayList<>();
        }
        List<AvailablePlatform> platforms = new ArrayList<>();
        for (Site site : network.getSites()) {
            platforms.addAll(site.getAvailablePlatforms(dictionaryOperator.getPlatforms()));
        }
        return platforms;
    }

    /**
     * Cartesian product of cost dimensions.
     * Essentially the available combinations of each cost dimension.
     *
     * @param operator
     * @param network
     * @return A list of cost-dimension tuples.
     */
    public List<List<OptimizationCost>> getCostDims(Operator operator, INFORENetwork network) {
        //Each list-entry consists of the unique values of each cost dim
        List<List<OptimizationCost>> listOfLists = new ArrayList<>();

        //Locate the operator
        Optional<DictionaryOperator> inforeOperator = Optional.empty();
        for (DictionaryOperator o : this.operators) {
            if (o.getClassKey().equals(operator.getClassKey())) {
                inforeOperator = Optional.of(o);
                break;
            }
        }

        //Iterate through all sites and locate the supported platforms iff present
        inforeOperator.ifPresent(op -> {
            for (Site site : network.getSites()) {
                for (AvailablePlatform platform : site.getAvailablePlatforms(op.getPlatforms())) {
                    List<OptimizationCost> dims = new ArrayList<>();
                    dims.add(site);
                    dims.add(platform);
                    listOfLists.add(dims);
                }
            }
        });

        //Return their cartesian product
        return listOfLists;
    }

    /**
     * Add missing operators to the dictionary.
     */
    public void fillOperators(OperatorGraph operatorGraph) {
        //Collect all platforms and class keys
        Set<String> existingClassKeys = new HashSet<>();
        for (DictionaryOperator op : this.operators) {
            existingClassKeys.add(op.getClassKey());
        }

        //New operators with implementations
        Set<DictionaryOperator> modifiedOperators = new HashSet<>();

        //Iterate over non source/sink operators
        for (OperatorVertex operatorVertex : operatorGraph.getGraph().vertexSet()) {
            if (!operatorVertex.equals(operatorGraph.getGraphSource()) && !operatorVertex.equals(operatorGraph.getGraphSink())) {
                String classKey = operatorVertex.getOperator().getClassKey();
                if (!existingClassKeys.contains(classKey)) {
                    //Assign this new operator all available platforms
                    Platforms dummyPlatforms = new Platforms(
                            new Spark("spark_" + classKey),
                            new Flink("flink_" + classKey),
                            new Akka("akka_" + classKey)
                    );

                    //Add the new entry
                    modifiedOperators.add(new DictionaryOperator(classKey, dummyPlatforms));
                    existingClassKeys.add(classKey);
                }
            }
        }

        //Add/Overwrite the new operators to the existing ones
        this.operators.addAll(modifiedOperators);

        if (modifiedOperators.size() > 0) {
            masterLogger.info(String.format("Filled operators [%s] for dictionary [%s]",
                    modifiedOperators.toString().replace("\n", ""), this.dictionaryName));
        }
    }

    public Set<DictionaryOperator> getOperators() {
        return operators;
    }

    public boolean deleteOp(String class_key) {
        return this.operators.removeIf(f -> f.getClassKey().equals(class_key));
    }

    public String getModifiedAt() {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        return sdf.format(modifiedAt);
    }
}
