package optimizer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@SpringBootApplication
@EnableWebSecurity
public class OptimizerApplication implements CommandLineRunner {
    public static final Logger masterLogger = LogManager.getLogger(OptimizerApplication.class);

    public static void main(java.lang.String[] args) {
        SpringApplication.run(OptimizerApplication.class, args);
    }

    @Override
    public void run(String... args) {
        masterLogger.info("Optimizer READY!");
    }
}
