package optimizer.plan;

import optimizer.graph.implementation.OperatorImplementationEdge;
import optimizer.graph.implementation.OperatorImplementationGraph;
import optimizer.graph.implementation.OperatorImplementationVertex;
import optimizer.graph.operator.OperatorVertex;
import optimizer.web.service.CostEstimatorService;

import java.util.List;

public class ExhaustivePlan extends AbstractOptimizerPlan {
    private final int cost;

    public ExhaustivePlan(List<OperatorImplementationVertex> implementationVertexList, OperatorImplementationGraph implGraph, CostEstimatorService costEstimator) {
        super();

        //Map from operator vertices to their implementation
        for (OperatorImplementationVertex implementationVertex : implementationVertexList) {
            addImplementation(implementationVertex, 0);
        }

        //Iterate through all implementation vertices and calculate their cost
        //Calculate the transition cost as well between each implementation vertex and its successors
        for (OperatorImplementationVertex implementationVtx : implementationVertexList) {
            int opCost = this.costMap.get(implementationVtx.getOpVertex());
            for (OperatorVertex succ : implGraph.getSuccessors(implementationVtx.getOpVertex())) {
                if (this.implMap.containsKey(succ)) {
                    int transitionCost = costEstimator.estimateCost(new OperatorImplementationEdge(implementationVtx, this.implMap.get(succ)), implGraph);
                    int succCost = this.costMap.get(succ);
                    if (opCost + transitionCost > succCost) {
                        int newSuccCost = opCost + transitionCost + this.implMap.get(succ).cost();
                        this.costMap.put(succ, newSuccCost);
                    }
                }
            }
        }

        //Plan cost is equal to the cost of the END_OR operator
        this.cost = this.costMap.get(implGraph.getOperatorGraph().getGraphSink());
    }

    @Override
    public int cost() {
        return this.cost;
    }

    public void addImplementation(OperatorImplementationVertex vertex, int cost) {
        this.implMap.put(vertex.getOpVertex(), vertex);
        this.costMap.put(vertex.getOpVertex(), cost);
    }

    @Override
    public boolean isComplete() {
        return true;
    }
}
