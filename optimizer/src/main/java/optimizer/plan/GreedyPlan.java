package optimizer.plan;

import optimizer.graph.implementation.OperatorImplementationVertex;
import optimizer.graph.operator.OperatorVertex;

import java.util.Map;

public class GreedyPlan extends AbstractOptimizerPlan {

    public GreedyPlan(Map<OperatorVertex, OperatorImplementationVertex> implMap, Map<OperatorVertex, Integer> costMap) {
        this.implMap = implMap;
        this.costMap = costMap;
    }

    @Override
    public int cost() {
        return this.costMap.values().stream().reduce(0, Integer::sum);
    }

    @Override
    public boolean isComplete() {
        return this.implMap.size() > 0 && this.costMap.size() > 0;
    }
}
