package optimizer.plan;

import optimizer.graph.implementation.OperatorImplementationVertex;

import java.util.Map;

public interface OptimizerPlan {
    int cost();

    boolean isComplete();

    Map<OperatorImplementationVertex, Integer> getPathAndCosts();

    String toJSON();
}
