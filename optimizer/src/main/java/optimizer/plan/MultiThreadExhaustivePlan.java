package optimizer.plan;

import optimizer.experimental.context.OperatorContext;
import optimizer.experimental.context.OperatorImplementationContext;
import optimizer.experimental.context.RMOperatorImplementation;
import optimizer.experimental.plan.RMOptimizationPlan;
import optimizer.graph.implementation.OperatorImplementationVertex;
import optimizer.graph.operator.OperatorVertex;
import optimizer.model.OperatorImplementation;
import optimizer.parser.workflow.Operator;

import java.util.HashMap;
import java.util.Map;

public class MultiThreadExhaustivePlan extends AbstractOptimizerPlan {
    private final int cost;

    public MultiThreadExhaustivePlan(RMOptimizationPlan bestPlan, Map<OperatorContext, Map<String, Integer>> costs) {
        //Populate the implementation map
        Map<OperatorVertex, OperatorImplementationVertex> implMap = getImplMap();
        Map<OperatorVertex, Integer> costMap = new HashMap<>();
        this.cost = bestPlan.cost();

        //Iterate over the operators
        for (Map.Entry<OperatorContext, OperatorImplementationContext> entry : bestPlan.getImplMap().entrySet()) {
            OperatorVertex operatorVertex = new OperatorVertex((Operator) entry.getKey());
            RMOperatorImplementation implementation = (RMOperatorImplementation) entry.getValue();
            Map<String, Integer> opCosts = costs.get(entry.getKey());
            int siteCost = opCosts.get("site");
            int platformCost = opCosts.get("platform");
            int operatorCost = opCosts.get("operator");
            int migrationCost = opCosts.get("migration");
            OperatorImplementation oi = new OperatorImplementation(implementation.getSite(), implementation.getPlatform(), siteCost, platformCost);
            OperatorImplementationVertex oiv = new OperatorImplementationVertex(operatorVertex, oi);
            implMap.put(operatorVertex, oiv);
            costMap.put(operatorVertex, siteCost + platformCost + operatorCost + migrationCost);
        }

        //Update the parent maps
        this.setImplMap(implMap);
        this.setCostMap(costMap);
    }

    @Override
    public int cost() {
        return this.cost;
    }

    //All plans produced by this algorithm are complete
    @Override
    public boolean isComplete() {
        return true;
    }
}
