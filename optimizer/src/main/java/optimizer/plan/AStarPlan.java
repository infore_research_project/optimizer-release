package optimizer.plan;

import optimizer.graph.implementation.OperatorImplementationEdge;
import optimizer.graph.implementation.OperatorImplementationGraph;
import optimizer.graph.implementation.OperatorImplementationVertex;
import optimizer.graph.operator.OperatorVertex;
import optimizer.web.service.CostEstimatorService;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class AStarPlan extends AbstractOptimizerPlan {
    //Ref to the operator implementation graph
    private final OperatorImplementationGraph opImplGraph;

    //Set of operator implementations that are inside this solution and their outgoing edges
    //lead to operator implementations outside this solution.
    private final Set<OperatorVertex> endingNodes;

    //Operator implementation that was used to expand the previous solution and produce this one (i.e the new vertex).
    private final OperatorImplementationVertex newImplementationVertex;

    //Cost of this solution
    private final int cost;

    /**
     * Initial constructor, used to create the first solution that includes the root operator of the graph.
     *
     * @param opImplGraph A ref to the operator implementation graph.
     */
    public AStarPlan(OperatorImplementationGraph opImplGraph) {
        super();
        OperatorImplementationVertex newOp = opImplGraph.getRootVertex();

        this.opImplGraph = opImplGraph;
        this.opImplGraph.addVertex(newOp);

        this.newImplementationVertex = newOp;
        this.endingNodes = new HashSet<>();
        this.endingNodes.add(newOp.getOpVertex());

        this.implMap.put(newOp.getOpVertex(), newImplementationVertex);
        this.costMap.put(newOp.getOpVertex(), newOp.cost());
        this.cost = newOp.cost();
    }

    /**
     * Constructor used when expanding a previously created solution.
     *
     * @param otherSol     The solution this one builds upon.
     * @param newOpImplVtx The new operator implementation vertex.
     */
    public AStarPlan(AStarPlan otherSol, OperatorImplementationVertex newOpImplVtx, CostEstimatorService costEstimator) {
        //Copy the reference to the operator implementation graph and add the new operator implementation
        this.opImplGraph = otherSol.getOpImplGraph();
        this.opImplGraph.addVertex(newOpImplVtx);
        this.newImplementationVertex = newOpImplVtx;

        //Copy the ending nodes of the previous solution and add the new operator implementation.
        this.endingNodes = new HashSet<>(otherSol.getEndingNodes());
        this.implMap = new HashMap<>(otherSol.getImplMap());
        this.costMap = new HashMap<>(otherSol.getCostMap());

        //Add the new operator implementation in the ending nodes
        this.endingNodes.add(newOpImplVtx.getOpVertex());
        this.implMap.put(newOpImplVtx.getOpVertex(), newOpImplVtx);

        //Check if the addition of this implementation leads to the removal of other ending nodes
        //Take the graph predecessors of this implementation from the graph and remove those that
        //contain operators not in the unique visited operators.
        int newOpCostSoFar = 0;
        for (OperatorVertex predecessor : this.opImplGraph.getPredecessors(this.newImplementationVertex.getOpVertex())) {
            if (this.endingNodes.containsAll(this.opImplGraph.getSuccessors(predecessor))) {
                this.endingNodes.remove(predecessor);
            }

            //Cost of the predecessor operator implementation
            int predCost = this.costMap.get(predecessor);

            //Edge between the predecessor implementation and the new implementation
            OperatorImplementationEdge edge = opImplGraph.getEdgeBetween(this.implMap.get(predecessor), this.newImplementationVertex);
            int transmissionCost = edge != null ? costEstimator.estimateCost(edge, opImplGraph) : 0;

            //Calculate the new operator implementation cost as the max of the previous operators
            newOpCostSoFar = Math.max(newOpCostSoFar, predCost + transmissionCost);
        }

        //Add the new operator cost
        this.costMap.put(newImplementationVertex.getOpVertex(), newOpCostSoFar + this.newImplementationVertex.getImplementation().cost());

        //Calculate the solution cost as the max cost of the ending nodes + the heuristic cost
        this.cost = this.endingNodes.stream()
                .mapToInt(en -> this.costMap.get(en) + en.getOperatorVertexCostModel().getHeuristicCost())
                .max()
                .orElse(Integer.MAX_VALUE);
    }

    @Override
    public int cost() {
        return this.cost;
    }

    /**
     * Checks if the last inserted operator is the goal operator.
     *
     * @return True if the last inserted vertex equals the goal vertex.
     */
    public boolean isComplete() {
        return this.newImplementationVertex.getOpVertex().getOperator().equals(this.opImplGraph.getGoalOperator());
    }

    public String toSmallString() {
        return "OptimizerSolution{" +
                "implMap=" + implMap + "," +
                "cost=" + cost() +
                ", #uniqueOperators=" + costMap.size() +
                ", #endingNodes=" + endingNodes.size() +
                ", lastInsertedVertex=" + newImplementationVertex.getOpVertex().getOperator().getName() +
                '}';
    }

    @Override
    public String toString() {
        return "OptimizerSolution{" +
                "opImplGraph=" + opImplGraph +
                ", implMap=" + implMap +
                ", costs=" + costMap +
                ", endingNodes=" + endingNodes +
                ", newImplementationVertex=" + newImplementationVertex +
                '}';
    }

    //Getters and setters
    public OperatorImplementationGraph getOpImplGraph() {
        return opImplGraph;
    }

    public Set<OperatorVertex> getEndingNodes() {
        return endingNodes;
    }

    public OperatorImplementationVertex getNewImplementationVertex() {
        return newImplementationVertex;
    }

    public int getCost() {
        return cost;
    }
}
