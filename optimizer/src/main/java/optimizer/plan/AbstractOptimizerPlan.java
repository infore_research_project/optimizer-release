package optimizer.plan;

import optimizer.graph.implementation.OperatorImplementationVertex;
import optimizer.graph.operator.OperatorVertex;
import optimizer.utils.JSONSingleton;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractOptimizerPlan implements OptimizerPlan, Comparable<AbstractOptimizerPlan> {
    //Map of all visited operators and their chosen implementations
    protected Map<OperatorVertex, OperatorImplementationVertex> implMap;

    //Costs of operator vertices
    protected Map<OperatorVertex, Integer> costMap;

    public AbstractOptimizerPlan() {
        this.implMap = new HashMap<>();
        this.costMap = new HashMap<>();
    }

    /**
     * Iterate over the graph and find the optimal implementation per operator
     *
     * @return The operators as a list of operator vertices.
     */
    public Map<OperatorImplementationVertex, Integer> getPathAndCosts() {
        Map<OperatorImplementationVertex, Integer> optimalPath = new HashMap<>();
        for (OperatorVertex vertex : this.implMap.keySet()) {
            if (this.implMap.get(vertex).isValid()) {
                optimalPath.put(this.implMap.get(vertex), this.costMap.get(vertex));
            }
        }
        return optimalPath;
    }

    public OperatorImplementationVertex getImplementation(OperatorVertex vtx) {
        return this.implMap.get(vtx);
    }

    /**
     * The cost of this solution.
     */
    public abstract int cost();

    @Override
    public int compareTo(AbstractOptimizerPlan o) {
        return this.cost() - o.cost();
    }

    @Override
    public String toJSON() {
        return JSONSingleton.toJson(this);
    }

    //Getters and setter
    public Map<OperatorVertex, OperatorImplementationVertex> getImplMap() {
        return implMap;
    }

    public void setImplMap(Map<OperatorVertex, OperatorImplementationVertex> implMap) {
        this.implMap = implMap;
    }

    public Map<OperatorVertex, Integer> getCostMap() {
        return costMap;
    }

    public void setCostMap(Map<OperatorVertex, Integer> costMap) {
        this.costMap = costMap;
    }
}
